#ifndef FILEEXPORT_H
#define FILEEXPORT_H

#include <string>

#include "basemesh.h"

//! Base class for the file exporters
class FileExport
{
public:

    FileExport()
    {
    }

//    virtual void exportToFile(PolygonMesh<Tetrahedron3D>* mesh, std::string filename, Transform t = Transform()) = 0;
    virtual void exportToFile(BaseMesh* mesh,                   std::string filename, Transform t = Transform()) = 0;

};

#endif // FILEEXPORT_H
