#ifndef MESHGENERATIONPARAMETERS_H
#define MESHGENERATIONPARAMETERS_H

#include <vector>
#include <iostream>
#include <memory>

#include "labeldomain.h"

//! Base class for the mesh generation parameters
class MeshGenerationParameters
{
    using labelType = double;
public:
    MeshGenerationParameters();
    virtual ~MeshGenerationParameters() = 0;

    std::vector<BoundingBoxLabelDomain<> > labelDomains;
    double outputScale;
};

//! Parameters used to generate meshes with CGAL
class CGALParameters : public MeshGenerationParameters
{
public:
    double facet_size;
    double facet_angle;
    double facet_approximation;

    double cell_radius_edge_ratio;
    double cell_size;

    bool lloyd_smoothing;
    unsigned int lloyd_max_iteration;

    bool odt_smoothing;
    unsigned int odt_max_iteration;

    bool perturb;
    double perturb_max_time;

    bool exude;
    double exude_max_time;

    CGALParameters(): MeshGenerationParameters()
      , facet_size(0.15)
      , facet_angle(25)
      , facet_approximation(0.008)
      , cell_radius_edge_ratio(3)
      , cell_size(0.15)
      , lloyd_smoothing(false)
      , lloyd_max_iteration(200)
      , odt_smoothing(false)
      , odt_max_iteration(200)
      , perturb(false)
      , perturb_max_time(30)
      , exude(false)
      , exude_max_time(30)
    {}

    CGALParameters(double facet_size
        , double facet_angle
        , double facet_approximation
        , double cell_radius_edge_ratio
        , double cell_size
        , bool _lloyd_smoothing
        , unsigned int _lloyd_max_iteration
        , bool _odt_smoothing
        , unsigned int _odt_max_iteration
        , bool _perturb
        , double _perturb_max_time
        , bool _exude
        , double _exude_max_time
                   )
          : facet_size(facet_size)
          , facet_angle(facet_angle)
          , facet_approximation(facet_approximation)
          , cell_radius_edge_ratio(cell_radius_edge_ratio)
          , cell_size(cell_size)
          , lloyd_smoothing(_lloyd_smoothing)
          , lloyd_max_iteration(_lloyd_max_iteration)
          , odt_smoothing(_odt_smoothing)
          , odt_max_iteration(_odt_max_iteration)
          , perturb(_perturb)
          , perturb_max_time(_perturb_max_time)
          , exude(_exude)
          , exude_max_time(_exude_max_time)
    {}

    ~CGALParameters(){}
};


#endif // MESHGENERATIONPARAMETERS_H
