#ifndef POINT3D_H
#define POINT3D_H

#include "vector.h"
#include "transform.h"

#include <iostream>

class Transform;

//! 3D point in space
template <class T = double>
class Point3D : public Vector3d<T>
{
public:
    Point3D() : Vector3d<T>() {}
    Point3D(T x, T y, T z) : Vector3d<T>(x,y,z) {}
    Point3D(const Point3D<T>& point) : Vector3d<T>(point) {}

    T getX() const {return this->operator [](0);}
    T getY() const {return this->operator [](1);}
    T getZ() const {return this->operator [](2);}

    void setX(T value){this->operator [](0) = value;}
    void setY(T value){this->operator [](1) = value;}
    void setZ(T value){this->operator [](2) = value;}

    T distance(Point3D p);

    void scale(T x, T y, T z);

    void transform(const Transform& t);
};

template<typename T>
std::ostream& operator<<(std::ostream &os, const Point3D<T>& p)
{
    return os << "(" << p.getX() << ", " << p.getY() << ", " << p.getZ() << ")";
}

template<typename T>
Point3D<T>& operator*(Point3D<T>& v1, FixedVector<T,3>& v2)
{
    return v1*v2;
}

template<typename T>
Point3D<T>& operator*(FixedVector<T,3>& v1, Point3D<T>& v2)
{
    return v1*v2;
}

template<typename T>
Point3D<T>& operator+(Point3D<T>& v1, FixedVector<T,3>& v2)
{
    return v1+v2;
}

template<typename T>
Point3D<T>& operator+(FixedVector<T,3>& v1, Point3D<T>& v2)
{
    return v1+v2;
}

#endif // POINT3D_H
