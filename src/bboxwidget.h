#ifndef BBOXWIDGET_H
#define BBOXWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QSlider>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QColorDialog>
#include <QCheckBox>

#include "mainwindow.h"
#include "filereader.h"
#include "baseimage.h"
#include "boundingbox.h"
#include "point3d.h"


class MainWindow;

//!  Qt widget to display a bounding box
class BBoxWidget : public QWidget
{
    Q_OBJECT
public:
    explicit BBoxWidget(QWidget *parent = 0);

    void setName(QString n);
    void provideImageData(std::shared_ptr<BaseImage> image);

    Point3D<> getMin();
    Point3D<> getMax();
    BoundingBox<> getBoundingBox();

    double getValue();
    QColor getColor();

    bool getCheckPoints() const;
    bool getCheckFacets() const;
    bool getCheckCells () const;

signals:

public slots:

    void updateSliderValueMinX(int value);
    void updateSliderValueMinY(int value);
    void updateSliderValueMinZ(int value);

    void updateSliderValueMaxX(int value);
    void updateSliderValueMaxY(int value);
    void updateSliderValueMaxZ(int value);

    void updateMainWindowOutput();

    void setColor();

private:
    QVBoxLayout* labelLayout;
    QLabel* name;

    QFormLayout* labelFormLayout;

    QSlider* slider_min_x;
    QSlider* slider_min_y;
    QSlider* slider_min_z;

    QSlider* slider_max_x;
    QSlider* slider_max_y;
    QSlider* slider_max_z;

    QLineEdit* labelValue;

    QButtonGroup* group;
    QCheckBox* checkPoint;
    QCheckBox* checkFacet;
    QCheckBox* checkCell;

    QPushButton* colorPickerButton;



    MainWindow* mainWindow;

    QColor bboxColor;

};


class BBoxWidgetList : public QVBoxLayout
{
public:
    int size(){return list.size();}
    void updateLabelNames();

    void addWidget(QWidget *widget, int stretch = 0, Qt::Alignment alignment = 0);


    std::vector<BoundingBox<> > getBoundingBoxes();

    BBoxWidget* getWidget(int i);

protected:
    std::vector<BBoxWidget*> list;
};

#endif // BBOXWIDGET_H
