#ifndef FILEREADER_H
#define FILEREADER_H

#include <iostream>
#include <vector>
#include <algorithm>

#include "point3d.h"

enum class FileType
{
    NONE,
    MESH,
    IMAGE
};

//! Base class for the file readers
class FileReader
{
public:
    FileReader(){}
    FileReader(std::string filename);

    void setFilename(std::string filename);
    std::string getFilename();


    virtual FileType getFileType() const = 0;


protected:
    std::string m_filename;
};

#endif // FILEREADER_H
