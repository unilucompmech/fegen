#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "vector.h"
#include "point3d.h"
#include "quaternion.h"
#include "matrix.h"

template <class T> class Point3D;

//! Linear transformation (translation, rotation and scaling)
class Transform
{
    using float_type = double;
    typedef Vector3d<float_type> Vec;
    typedef Quaternion<float_type> Quat;
    typedef Matrix<float_type,4> Mat44;

public:
    //! Default constructor
    Transform();

    //! Get a reference to the scale component
    Vec& getScale();

    //! Set the scale component
    void setScale(const Vec &value);

    //! Set the scale component uniformly
    void setScale(const float_type& value);

    //! Cumulative
    void addScale(const float_type& value);

    //! Get a reference to the translation component
    Vec& getTranslation();

    //! Set the translation component
    void setTranslation(const Vec &value);

    //! Get a reference to the rotation component
    Quat &getRotation();

    //! Get the rotation component
    Quat getRotation() const;

    //! Set the rotation component
    void setRotation(const Quat &value);

    Vec& getRotScale();
    void setRotScale(const Vec &value);

    //! Return the transformed 3D point
    template <class T> Point3D<T> transform(Point3D<T> p) const
    {
        FixedVector<double,4> p_1;
        p_1[0] = p[0];
        p_1[1] = p[1];
        p_1[2] = p[2];
        p_1[3] = 1;

        p_1 = this->toMatrix()*p_1;
        return Point3D<T>(p_1[0], p_1[1], p_1[2]);
    }

    //! Convert this transform to a 4x4 transformation matrix
    Mat44 toMatrix() const;

    friend std::ostream& operator<<(std::ostream &os, const Transform& t)
    {
        os << "scale[";
        os << t.scale;
        os << "]";
        os << ",rotation[";
        os << t.rotation;
        os << "]";
        os << ",rotScale[";
        os << t.rotScale;
        os << "]";
        os << ",translation[";
        os << t.translation;
        os << "]";
        os << ",matrix";
        os << t.toMatrix();
        return os;
    }


protected:

    Vec  scale;
    Vec  translation;
    Quat rotation;
    Vec  rotScale;

};

#endif // TRANSFORM_H
