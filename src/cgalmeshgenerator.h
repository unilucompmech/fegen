#ifndef CGALMESHGENERATOR_H
#define CGALMESHGENERATOR_H

#include "meshgenerator.h"
#include "basemesh.h"
#include "transform.h"

#include <iostream>
#include <time.h>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Mesh_triangulation_3.h>
#include <CGAL/Mesh_complex_3_in_triangulation_3.h>
#include <CGAL/Mesh_criteria_3.h>
#include <CGAL/Polyhedral_mesh_domain_3.h>
#include <CGAL/Labeled_image_mesh_domain_3.h>
#include <CGAL/make_mesh_3.h>
#include <CGAL/refine_mesh_3.h>
// IO
#include <CGAL/IO/Polyhedron_iostream.h>


//! Mesh generator using CGAL
class CGALMeshGenerator : public MeshGenerator
{
public:

    using MeshType     = Tetrahedron3D;
    using MeshCellType = PolygonMesh<MeshType>::CellType;


    // Domain
    typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
    typedef CGAL::Polyhedron_3<K> Polyhedron;
    typedef CGAL::Polyhedral_mesh_domain_3<Polyhedron, K> Mesh_domain;
    typedef CGAL::Labeled_image_mesh_domain_3<CGAL::Image_3,K> Mesh_domain_from_image;

    // Triangulation
    #ifdef CGAL_CONCURRENT_MESH_3
      typedef CGAL::Mesh_triangulation_3<
        Mesh_domain,
        CGAL::Kernel_traits<Mesh_domain>::Kernel, // Same as sequential
        CGAL::Parallel_tag                        // Tag to activate parallelism
      >::type Tr;
    #else
      typedef CGAL::Mesh_triangulation_3<Mesh_domain>::type Tr;
    #endif
    typedef CGAL::Mesh_complex_3_in_triangulation_3<Tr> C3t3;

    // Criteria
    typedef CGAL::Mesh_criteria_3<Tr> Mesh_criteria;
    // To avoid verbose function and named parameters call
//    using namespace CGAL::parameters;

    //iterators
    typedef typename C3t3::Cell_iterator Cell_iterator;
    typedef typename Tr::Finite_vertices_iterator Finite_vertices_iterator;

public:
    CGALMeshGenerator();

    void exportMesh(std::string filename)
    {
        std::cout << "Export mesh to " << filename << std::endl;
        std::ofstream medit_file(filename.c_str());
        c3t3.output_to_medit(medit_file);
        medit_file.close();
    }

    PolygonMesh<Tetrahedron3D> getTetrahedronMesh();

    Transform getTransform() const;

protected:

    Polyhedron polyhedron;
    C3t3 c3t3;

    Transform transform;
    PolygonMesh<MeshType> outputMesh;

    void convertToInternalMesh();
    void updateLabels(MeshGenerationParameters *parameters) override;
};

#endif // CGALMESHGENERATOR_H
