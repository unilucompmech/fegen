#ifndef OBJREADER_H
#define OBJREADER_H

#include <stdlib.h>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>

#include "point3d.h"
#include "filereader.h"
#include "meshfilereader.h"

//! Read a mesh in OBJ Wavefront format
class ObjReader : public MeshFileReader
{
public:

    ObjReader(){}
    ObjReader(std::string filename)
    {
        std::cout << "Create the Wavefront .obj file reader" << std::endl;
        this->setFilename(filename);
        this->readMesh();
    }

    void readMesh();

};

#endif // OBJREADER_H
