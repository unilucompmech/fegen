#include "outputinfowidget.h"

OutputInfoWidget::OutputInfoWidget(QWidget *parent) :
    QTreeWidget(parent)
{
    setColumnCount(2);


    QStringList m_tableHeader;
    m_tableHeader<<"Property"<<"Value";
    this->setHeaderLabels(m_tableHeader);



    this->expandToDepth(0);
    this->resizeColumnToContents(0);



    rootItem = nullptr;
    labelRootItem = nullptr;
}

void OutputInfoWidget::setOutputRoot()
{
    if (rootItem == nullptr)
    {
        rootItem = new QTreeWidgetItem(this);
        rootItem->setText(0, QString("Output"));
    }
}

void OutputInfoWidget::addNbVertices(unsigned int nb)
{
    setOutputRoot();
    if (rootItem != nullptr)
    {
        QTreeWidgetItem *treeItem_nbVertices = new QTreeWidgetItem(rootItem);
        treeItem_nbVertices->setText(0, QString("Number of points"));
        treeItem_nbVertices->setText(1, QString::number(nb));
    }

    this->expandToDepth(0);
    this->resizeColumnToContents(0);
}

void OutputInfoWidget::addNbCells(unsigned int nb)
{
    setOutputRoot();
    if (rootItem != nullptr)
    {
        QTreeWidgetItem *treeItem_nbCells = new QTreeWidgetItem(rootItem);
        treeItem_nbCells->setText(0, QString("Number of cells"));
        treeItem_nbCells->setText(1, QString::number(nb));
    }

    this->expandToDepth(0);
    this->resizeColumnToContents(0);
}

void OutputInfoWidget::addNbFacets(unsigned int nb)
{
    setOutputRoot();
    if (rootItem != nullptr)
    {
        QTreeWidgetItem *treeItem_nbFacets = new QTreeWidgetItem(rootItem);
        treeItem_nbFacets->setText(0, QString("Number of facets"));
        treeItem_nbFacets->setText(1, QString::number(nb));
    }

    this->expandToDepth(0);
    this->resizeColumnToContents(0);
}

void OutputInfoWidget::addBoundingBox(BoundingBox<> bbox)
{
    setOutputRoot();

    if (rootItem != nullptr)
    {
        std::ostringstream ss;
        ss << bbox.print();

        QTreeWidgetItem *treeItem_nbCells = new QTreeWidgetItem(rootItem);
        treeItem_nbCells->setText(0, QString("Bounding box"));
        treeItem_nbCells->setText(1, QString(ss.str().c_str()));
    }
}

void OutputInfoWidget::addLabel(double labelValue, unsigned int nbVertices, unsigned int nbFacets, unsigned int nbCells)
{
    setOutputRoot();

    if (labelRootItem == nullptr && rootItem != nullptr)
    {
        labelRootItem = new QTreeWidgetItem(rootItem);
        labelRootItem->setText(0, QString("Labels"));
    }

    if (labelRootItem != nullptr)
    {
        QTreeWidgetItem* treeItem_label = new QTreeWidgetItem(labelRootItem);
        treeItem_label->setText(0, QString("Label #")+QString::number(labelRootItem->childCount()));

        QTreeWidgetItem *treeItem_value = new QTreeWidgetItem(treeItem_label);
        treeItem_value->setText(0, QString("Value"));
        treeItem_value->setText(1, QString::number(labelValue));

        QTreeWidgetItem *treeItem_nbVertices = new QTreeWidgetItem(treeItem_label);
        treeItem_nbVertices->setText(0, QString("Number of points"));
        treeItem_nbVertices->setText(1, QString::number(nbVertices));

        QTreeWidgetItem *treeItem_nbFacets = new QTreeWidgetItem(treeItem_label);
        treeItem_nbFacets->setText(0, QString("Number of facets"));
        treeItem_nbFacets->setText(1, QString::number(nbFacets));

        QTreeWidgetItem *treeItem_nbCells = new QTreeWidgetItem(treeItem_label);
        treeItem_nbCells->setText(0, QString("Number of cells"));
        treeItem_nbCells->setText(1, QString::number(nbCells));

        labelRootItem->setText(1, QString::number(labelRootItem->childCount()));
    }

    this->expandToDepth(2);
    this->resizeColumnToContents(0);
}

void OutputInfoWidget::addVertices(std::vector<MeshVertex<> > vertices)
{
    if (vertices.size() < 2000)
    {
        setOutputRoot();
        if (rootItem != nullptr)
        {
            QTreeWidgetItem *treeItem_vert = new QTreeWidgetItem(rootItem);
            treeItem_vert->setText(0, QString("Vertices"));

            unsigned int i = 0;
            for (const auto& v : vertices)
            {
                std::stringstream ss;
                ss << v;
                QTreeWidgetItem *treeItem_vert_i = new QTreeWidgetItem(treeItem_vert);
                treeItem_vert_i->setText(0, QString::number(i));
                treeItem_vert_i->setText(1, QString(ss.str().c_str()));
                i++;
            }
        }
    }
}



void OutputInfoWidget::clear()
{
    QTreeWidget::clear();


    rootItem = nullptr;
    labelRootItem = nullptr;
}

