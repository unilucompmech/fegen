#ifndef MESHFILEREADER_H
#define MESHFILEREADER_H

#include "point3d.h"
#include "filereader.h"

#include <vector>

enum class MeshType
{
    NONE,
    TRIANGLES,
    QUADS,
    QUADS_AND_TRIANGLES,
    TETRAHEDRONS,
    HEXAHEDRONS,
    COMBINAISON
};

//! Base class for the mesh file readers
class MeshFileReader : public FileReader
{
public:

    typedef std::vector<unsigned int> Face;
    typedef double value_type;

    MeshFileReader();

    std::vector<Point3D<value_type> > getVertices(){return vertices;}
    std::vector<Face > getCells(){return cells;}

    unsigned int getNbCells(){return cells.size();}
    unsigned int getNbVertices(){return vertices.size();}

    MeshType getMeshType()
    {
        if (cells.size())
        {
            std::vector<unsigned int> sizes; //different cell sizes in the mesh

            for (unsigned int i = 0; i < cells.size(); i++)
            {
                bool alreadyRegistered = false;
                for (unsigned int j = 0; j < sizes.size(); j++)
                {
                    if (cells[i].size() == sizes[j])
                    {
                        alreadyRegistered = true;
                        break;
                    }
                }

                if (!alreadyRegistered) sizes.push_back(cells[i].size());
            }

            if (sizes.size() == 0)
            {
                return MeshType::NONE;
            }
            else if (sizes.size() == 1)
            {
                unsigned int s = sizes[0];
                switch (s) {
                case 3:
                    return MeshType::TRIANGLES;
                    break;
                case 4:
                    return MeshType::TETRAHEDRONS;
                    break;
                case 8:
                    return MeshType::HEXAHEDRONS;
                    break;
                default:
                    return MeshType::NONE;
                    break;
                }
            }
            else
            {
                std::sort(sizes.begin(), sizes.end());
                if (sizes.size() == 2)
                {
                    if (sizes[0] == 3 && sizes[1] == 4)
                    {
                        return MeshType::QUADS_AND_TRIANGLES;
                    }
                }
                else
                    return MeshType::COMBINAISON;
            }

        }

        return MeshType::NONE;
    }

    FileType getFileType() const{return FileType::MESH;}

protected:

    std::vector<Point3D<value_type> > vertices;
    std::vector<Face> cells;
};

#endif // MESHFILEREADER_H
