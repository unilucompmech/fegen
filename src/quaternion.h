#ifndef QUATERNION_H
#define QUATERNION_H

#include "vector.h"

#include <math.h>

//! Quaternion and mathematical operations
template <class T>
class Quaternion : public FixedVector<T, 4>
{
public :
    Quaternion() : FixedVector<T, 4>()
    {
        this->data[0] = (T)1;
    }

    Quaternion(T a, T b, T c, T d)
    {
        this->data[0] = a;
        this->data[1] = b;
        this->data[2] = c;
        this->data[3] = d;
    }

    Quaternion(const Quaternion<T>& q)
    {
        this->data[0] = q[0];
        this->data[1] = q[1];
        this->data[2] = q[2];
        this->data[3] = q[3];
    }

    const Vector3d<T> toEuler()
    {
        T phi = atan2(
                    2*(this->data[0]*this->data[1]+this->data[2]*this->data[3]),
                    1-2*(this->data[1]*this->data[1]+this->data[2]*this->data[2])  );
        T theta = asin(2*(this->data[0]*this->data[2]-this->data[3]*this->data[1]));
        T psi = atan2(
                    2*(this->data[0]*this->data[3]+this->data[1]*this->data[2]),
                    1-2*(this->data[2]*this->data[2]+this->data[3]*this->data[3])  );

        return Vector3d<T>(phi,theta,psi);
    }
};
#endif // QUATERNION_H
