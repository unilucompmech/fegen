#ifndef VTUEXPORT_H
#define VTUEXPORT_H

#include "fileexport.h"

//! Write a mesh in VTK format
class VTUExport : public FileExport
{
public:
    VTUExport();

    void exportToFile(BaseMesh *mesh,                   std::string filename, Transform t = Transform()) override;
    void exportToFile(PolygonMesh<Tetrahedron3D> *mesh, std::string filename, Transform t = Transform());
    void exportToFile(PolygonMesh<Triangle3D> *mesh,    std::string filename, Transform t = Transform());


};

#endif // VTUEXPORT_H
