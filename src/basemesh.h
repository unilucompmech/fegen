#ifndef BASEMESH_H
#define BASEMESH_H

#include <vector>
#include <sstream>
#include <limits>
#include <set>

#include "point3d.h"
#include "polygon3d.h"
#include "filereader.h"
#include "meshfilereader.h"
#include "boundingbox.h"



#define DEFAULT_LABEL 0

//!  Mesh vertex used in the class PolygonMesh
template <class T = double>
class MeshVertex : public Point3D<T>
{
    using labelType = double;

public:
    MeshVertex(const Point3D<T>& p) : Point3D<T>(p) {}
    void addLabel(labelType labelValue){label1D.insert(labelValue);}
    void clearLabels(){label1D.clear();}
    bool hasLabel(labelType label) const
    {
        for (const auto& l : label1D)
            if (l == label) return true;
        return false;
    }

    unsigned int getNbLabels() const
    {
        return label1D.size();
    }

    unsigned int getLabel(unsigned int i) const
    {
        if (i < this->getNbLabels())
        {
            unsigned int nb = 0;
            for (const auto& l : label1D)
            {
                if (nb == i) return l;
                nb++;
            }

        }
        return DEFAULT_LABEL;
    }

protected:
    std::set<labelType> label1D;

public:
    std::set<unsigned int> cells;
    std::set<unsigned int> facets;
};

//!  Mesh cell used in the class PolygonMesh
template <size_t N>
class MeshCell : public Polygon3D<N>
{
    using labelType = double;

public:

    using Facet = typename Polygon3D<N>::Facet;

    //default constructor
    MeshCell() : Polygon3D<N>() {}
    //copy constructor
    MeshCell(const Polygon3D<N>& p) : Polygon3D<N>(p) {}

    //add a label to this cell
    void addLabel(labelType labelValue){label1D.insert(labelValue);}

    //clear all labels of this cell
    void clearLabels(){label1D.clear();}

    //check if the label has been assigned to this cell
    bool hasLabel(labelType label) const
    {
        return std::find(label1D.begin(), label1D.end(), label) != label1D.end();
    }

    //return the number of labels of this cell
    unsigned int getNbLabels() const
    {
        return label1D.size();
    }

    //get the label #i
    unsigned int getLabel(unsigned int i) const
    {
        if (i < this->getNbLabels())
        {
            unsigned int nb = 0;
            for (const auto& l : label1D)
            {
                if (nb == i) return l;
                nb++;
            }

        }
        return DEFAULT_LABEL;
    }

protected:
    std::set<labelType> label1D;
public:
    std::set<unsigned int> indicesOtherCells;
};

//!  Base class for a mesh
class BaseMesh
{
public:
    virtual void readFromFile(FileReader* reader) = 0;
    virtual BoundingBox<> getBoundingBox() const = 0;

    virtual unsigned int getNbPoints() const = 0;
    virtual unsigned int getNbPointsWithLabel(double label) const = 0;
    virtual unsigned int getNbFacetsWithLabel(double label) const = 0;
    virtual unsigned int getNbCellsWithLabel(double label) const = 0;
    virtual unsigned int getNbCells() const = 0;
    virtual unsigned int getNbLabels() const = 0;
    virtual unsigned int getNbFacets() const = 0;
};

//!  Templated class for a mesh
/*!
  The template corresponds to the type of cells in the mesh
*/
template <class T>
class PolygonMesh : public BaseMesh
{

public:

    static const unsigned int cSize = T::size;
    using CellType  = MeshCell<cSize>;

    static const unsigned int fSize = cSize-1;
    using FacetType = MeshCell<fSize>;

    using PointID = typename CellType::PointID;
    using CellID  = typename CellType::PointID;
    using FacetID = typename FacetType::PointID;

    using CellsAroundVertex  = std::set<CellID>;
    using FacetsAroundVertex = std::set<FacetID>;
    using CellsAroundFacet   = std::set<CellID>;

    PolygonMesh(){}

    //! Add a cell in the mesh
    void addCell(CellType p)
    {
        //first, we add the cell to the list of cells
        cells.push_back(p);

        //get all the facets around the cell vertices
        // (this operation allow us to reduce the size of the next search)
        std::set<FacetID> facetsAround;
        for(const auto& v : p.indices)
        {
            for (const auto& fAround : this->getFacetsAroundVertex(v))
            {
                facetsAround.insert(fAround);
            }
        }

        //generate an array of facets from the input cell (p.getFacets())
        //loop over the cell facets and compare with the existing facets (facetsAround)
        for (const auto& f : ((Polygon3D<cSize>)p).getFacets())
        {
            bool alreadyExist = false;
            for (const auto& fAroundID : facetsAround)
            {
                if (facets[fAroundID] == f )
                {
                    //the facet already exists in the mesh
                    alreadyExist=true;

                    //tell to the cell this facet id
                    cells.back().indicesOtherCells.insert(fAroundID);

                    //... and tell to the facet that it belongs to a new cell
                    facets[fAroundID].indicesOtherCells.insert(cells.size()-1);

                    break;
                }
            }

            if (!alreadyExist)
            {
                //the facet does not exist yet in the mesh, so we add it...
                facets.push_back(f);

                //... and tell to the vertices that they belong to this facet
                for (const auto& v : f.indices)
                    positions[v].facets.insert(facets.size()-1);

                //also tell to the cell its facets id
                cells.back().indicesOtherCells.insert(facets.size()-1);

                //... and tell to the facet that it belongs to a new cell
                facets.back().indicesOtherCells.insert(cells.size()-1);
            }
        }

        //tell to the vertices that they belong to the input cell
        for (const auto& v : p.indices)
            positions[v].cells.insert(cells.size()-1);

    }

    //! Add a vertex in the mesh
    void addPoint(Point3D<> p)
    {
        positions.push_back(MeshVertex<>(p));
    }
    void addVertex(Point3D<> p){this->addPoint(p);}

    //! Add a label to a vertex
    void addPointLabel(int i, double labelValue)
    {
//        assert(i < positions.size());
        labels1D.insert(labelValue);
        positions[i].addLabel(labelValue);
    }
    //! Add a label to a facet
    void addFacetLabel(int i, double labelValue)
    {
//        assert(i < facets.size());
        labels1D.insert(labelValue);
        facets[i].addLabel(labelValue);
    }
    //! Add a label to a cell
    void addCellLabel(int i, double labelValue)
    {
//        assert(i < cells.size());
        labels1D.insert(labelValue);
        cells[i].addLabel(labelValue);
    }
    //! Add a label to a vertex
    void addLabel(MeshVertex<>& v, double labelValue)
    {
        labels1D.insert(labelValue);
        v.addLabel(labelValue);
    }
    //! Add a label to a cell
    void addLabel(CellType& c, double labelValue)
    {
        labels1D.insert(labelValue);
        c.addLabel(labelValue);
    }
    //! Add a label to a facet
    void addLabel(FacetType& f, double labelValue)
    {
        labels1D.insert(labelValue);
        f.addLabel(labelValue);
    }

    //! Get the number of vertices in the mesh
    unsigned int getNbPoints() const override
    {
        return positions.size();
    }

    //! Get the number of vertices with the label label
    unsigned int getNbPointsWithLabel(double label) const override
    {
        unsigned int nb = 0;
        for (const auto& p : positions)
            if (p.hasLabel(label)) nb++;
        return nb;
    }

    //! Get the number of facets with the label label
    unsigned int getNbFacetsWithLabel(double label) const override
    {
        unsigned int nb = 0;
        for (const auto& p : facets)
            if (p.hasLabel(label)) nb++;
        return nb;
    }

    //! Get the number of cells with the label label
    unsigned int getNbCellsWithLabel(double label) const override
    {
        unsigned int nb = 0;
        for (const auto& p : cells)
            if (p.hasLabel(label)) nb++;
        return nb;
    }

    unsigned int getNbLabels() const override
    {
        return labels1D.size();
    }

    unsigned int getNbCells() const override
    {
        return cells.size();
    }

    unsigned int getNbFacets() const override
    {
        return facets.size();
    }

    MeshVertex<> getPosition(unsigned int i) const
    {
//        assert(i < positions.size());
        return positions[i];
    }

    std::vector<MeshVertex<> >& getPositions()
    {
        return positions;
    }

    FacetType getFacet(unsigned int i) const
    {
        return facets[i];
    }

    std::vector<FacetType>& getFacets()
    {
        return facets;
    }

    CellType getCell(unsigned int i) const
    {
//        assert(i < polygons.size());
        return cells[i];
    }

    std::vector<CellType>& getCells()
    {
        return cells;
    }

    CellsAroundVertex getCellsAroundVertex(unsigned int i) const
    {
        return positions[i].cells;
    }

    FacetsAroundVertex getFacetsAroundVertex(unsigned int i) const
    {
        return positions[i].facets;
    }

    CellsAroundVertex getCellsAroundFacet(FacetType facet) const
    {
        return facet.indicesOtherCells;
    }

    std::set<double> getLabels() const
    {
        return labels1D;
    }

    BoundingBox<> getBoundingBox() const override;

    void clear()
    {
        positions.clear();
        cells.clear();
        facets.clear();
        clearLabels();
    }

    void clearLabels()
    {
        labels1D.clear();
        for (auto& p : positions) p.clearLabels();
        for (auto& p : facets   ) p.clearLabels();
        for (auto& p : cells    ) p.clearLabels();
    }

    void readFromFile(FileReader* reader) override;

    std::string print(bool verbose=false) const
    {
        std::ostringstream ss;
        ss << "Mesh<nodes=" << this->getNbPoints() << ", cells=" << this->getNbCells();
        if (facets.size())
            ss << ", facets=" << this->getNbFacets();
        ss << ">";
        if (verbose)
        {
            ss << "\n";
            ss << "points=[";
            for (unsigned int i = 0; i < this->getNbPoints(); i++)
            {
                ss << this->getPosition(i);
                if (i < this->getNbPoints()-1) ss << ", ";
            }
            ss << "]\n";
            ss << "cells=[";
            for (unsigned int i = 0; i < this->getNbCells(); i++)
            {
                ss << this->getCell(i);
                if (i < this->getNbCells()-1) ss << ", ";
            }
            ss << "]";
        }
        return ss.str();
    }

    std::string getLabelsInfo() const
    {
        std::ostringstream ss;
        unsigned int i = 0;
        for (const auto& l : labels1D)
        {
            ss << "Label #" << i << ": ";
            ss << "points(" << this->getNbPointsWithLabel(l) << "), ";
            ss << "facets(" << this->getNbFacetsWithLabel(l) << "), ";
            ss << "cells(" << this->getNbCellsWithLabel(l) << "), ";
            ss << "\n";
            i++;
        }
        return ss.str();
    }

    void transformMesh(const Transform& t)
    {
        for (auto& p : positions)
            p.transform(t);
    }

    friend std::ostream& operator<<(std::ostream &os, const PolygonMesh<T>& p)
    {
        return os << p.print();
    }

protected:
    std::vector<MeshVertex<> > positions;
    std::vector<CellType> cells;
    std::vector<FacetType> facets;

    //store the different labels defined for this mesh
    std::set<double> labels1D;
};



#endif // BASEMESH_H
