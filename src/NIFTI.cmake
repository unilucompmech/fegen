message( "External project - NIFTI" )

ExternalProject_Add(NIFTI
    PREFIX ${CMAKE_CURRENT_BINARY_DIR}/nifti
    DOWNLOAD_DIR ${CMAKE_CURRENT_BINARY_DIR}/nifti/download
    URL "http://sourceforge.net/projects/niftilib/files/nifticlib/nifticlib_2_0_0/nifticlib-2.0.0.tar.gz/download"
    URL_HASH SHA1=3a6187cb09767f97cef997cf492d89ac3db211df
#    SOURCE_DIR ${CMAKE_CURRENT_BINARY_DIR}/nifti/src
    BUILD_IN_SOURCE 1
    BUILD_COMMAND cmake --build .
#    BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR}/nifti/build
    INSTALL_COMMAND make install
    INSTALL_DIR ${CMAKE_CURRENT_BINARY_DIR}/nifti/install
    UPDATE_COMMAND ""
)

ExternalProject_Get_Property(NIFTI install_dir binary_dir)
message("NIFTI bin directory: " ${binary_dir})
message("NIFTI install directory: " ${install_dir})

add_library(libnifticdf STATIC IMPORTED)
add_dependencies(libnifticdf NIFTI)
set_property(TARGET libnifticdf PROPERTY IMPORTED_LOCATION
    ${binary_dir}/nifticdf/libnifticdf${CMAKE_STATIC_LIBRARY_SUFFIX}
)

add_library(libniftiio STATIC IMPORTED)
add_dependencies(libniftiio NIFTI)
set_property(TARGET libniftiio PROPERTY IMPORTED_LOCATION
    ${binary_dir}/niftilib/libniftiio${CMAKE_STATIC_LIBRARY_SUFFIX}
)

add_library(libznz STATIC IMPORTED)
add_dependencies(libznz NIFTI)
set_property(TARGET libznz PROPERTY IMPORTED_LOCATION
    ${binary_dir}/znzlib/libznz${CMAKE_STATIC_LIBRARY_SUFFIX}
)

include_directories(${binary_dir}/nifticdf/ ${binary_dir}/niftilib/ ${binary_dir}/znzlib/)

#set( NIFTI_DIR ${binary_dir} )
#set( NIFTI_LIBRARIES ${binary_dir}/lib)

#find_package(NIFTI REQUIRED)

#set(NIFTI_INCLUDE_DIR ${binary_dir}/niftilib/)


#set( NIFTI_LIBRARIES
#  ${NIFTI_BASE_LIBRARY}
#  ${NIFTI_ZNZ_LIBRARY}
#)
