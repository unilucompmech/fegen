#ifndef CYLINDERGENERATIONWIDGET_H
#define CYLINDERGENERATIONWIDGET_H

#include <assert.h>

#include <QWidget>
#include <QFormLayout>
#include <QLineEdit>
#include <QCheckBox>

class CylinderGenerationWidget : public QWidget
{
    Q_OBJECT
public:
    explicit CylinderGenerationWidget(QWidget *parent = 0);

    double getLength();
    double getInnerRadius();
    double getOuterRadius();
    unsigned int getLengthSubdiv();
    unsigned int getRadiusSubdiv();
    unsigned int getAngleSubdiv();

    bool getAutomaticLabelingVertex();
    bool getAutomaticLabelingFacet();
    bool getAutomaticLabelingCell();

signals:

public slots:

protected:
    QLineEdit* lengthLine;
    QLineEdit* innerRadiusLine;
    QLineEdit* outerRadiusLine;
    QLineEdit* lengthSubdivLine;
    QLineEdit* radiusSubdivLine;
    QLineEdit* angleSubdivLine;
    QCheckBox* automaticLabelingVertex;
    QCheckBox* automaticLabelingFacet;
    QCheckBox* automaticLabelingCell;

    QFormLayout* formLayout;
};

#endif // CYLINDERGENERATIONWIDGET_H
