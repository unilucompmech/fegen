#include "vtuexport.h"

#include <vtkVersion.h>
#include <vtkSmartPointer.h>
#include <vtkTetra.h>
#include <vtkTriangle.h>
#include <vtkCellArray.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkDataSetMapper.h>
#include <vtkActor.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkVertexGlyphFilter.h>
#include <vtkFloatArray.h>


VTUExport::VTUExport()
{
}

void VTUExport::exportToFile(PolygonMesh<Tetrahedron3D>* mesh, std::string filename, Transform t)
{
    std::cout << "About to write " << *mesh << " to " << filename << std::endl;

    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();

    for (const auto& c : mesh->getPositions())
    {
        MeshVertex<> p = c;
        p.transform(t);
        points->InsertNextPoint(p.getX(), p.getY(), p.getZ() );
    }

    vtkSmartPointer<vtkCellArray> cellArray = vtkSmartPointer<vtkCellArray>::New();

    for (const auto& c : mesh->getCells())
    {
        vtkSmartPointer<vtkTetra> tetra = vtkSmartPointer<vtkTetra>::New();

        for (unsigned int j = 0; j < c.getSize(); j++) //0..3
            tetra->GetPointIds()->SetId(j, c.getIndex(j));

        cellArray->InsertNextCell(tetra);
    }

//    vtkSmartPointer<vtkCellArray> cellArray = vtkSmartPointer<vtkCellArray>::New();

//    for (const auto& f : mesh->getFacets())
//    {
//        vtkSmartPointer<vtkTriangle> triangle = vtkSmartPointer<vtkTriangle>::New();

//        for (unsigned int j = 0; j < f.getSize(); j++) //0..2
//            triangle->GetPointIds()->SetId(j, f.getIndex(j));

//        cellArray->InsertNextCell(triangle);
//    }

    vtkSmartPointer<vtkUnstructuredGrid> unstructuredGrid = vtkSmartPointer<vtkUnstructuredGrid>::New();
    unstructuredGrid->SetPoints(points);
    unstructuredGrid->SetCells(VTK_TETRA, cellArray);

    if (mesh->getNbLabels())
    {
        vtkSmartPointer<vtkFloatArray> pArray = vtkSmartPointer<vtkFloatArray>::New();
        pArray->SetNumberOfValues(mesh->getNbPoints());
        pArray->SetName("Point Data");
        int vertex = 0;
        for (const auto& p : mesh->getPositions())
            pArray->SetValue(vertex++, (float)p.getLabel(0));
        vtkSmartPointer<vtkPointData> pointDataArray = unstructuredGrid->GetPointData();
        pointDataArray->AddArray(pArray);

        vtkSmartPointer<vtkFloatArray> cArray = vtkSmartPointer<vtkFloatArray>::New();
        cArray->SetName("Cell Data");
        cArray->SetNumberOfValues(mesh->getNbCells());
        int cell = 0;
        for (const auto& c : mesh->getCells())
            cArray->SetValue(cell++, (float)c.getLabel(0));
        vtkSmartPointer<vtkCellData> cellDataArray = unstructuredGrid->GetCellData();
        cellDataArray->AddArray(cArray);

    }


    // Write file
    vtkSmartPointer<vtkXMLUnstructuredGridWriter> writer = vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();
    writer->SetFileName(filename.c_str());
    #if VTK_MAJOR_VERSION <= 5
    writer->SetInput(unstructuredGrid);
    #else
    writer->SetInputData(unstructuredGrid);
    #endif
    writer->Write();


    std::cerr << "Successfully write "<< *mesh << " to " << filename << std::endl;

}

void VTUExport::exportToFile(PolygonMesh<Triangle3D> *mesh, std::string filename, Transform t)
{
    std::cout << "About to write " << *mesh << " to " << filename << std::endl;

    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();

    for (const auto& c : mesh->getPositions())
    {
        MeshVertex<> p = c;
        p.transform(t);
        points->InsertNextPoint(p.getX(), p.getY(), p.getZ() );
    }

    vtkSmartPointer<vtkCellArray> cellArray = vtkSmartPointer<vtkCellArray>::New();

    for (const auto& c : mesh->getCells())
    {
        vtkSmartPointer<vtkTriangle> tri = vtkSmartPointer<vtkTriangle>::New();

        for (unsigned int j = 0; j < c.getSize(); j++) //0..2
            tri->GetPointIds()->SetId(j, c.getIndex(j));

        cellArray->InsertNextCell(tri);
    }

//    vtkSmartPointer<vtkCellArray> cellArray = vtkSmartPointer<vtkCellArray>::New();

//    for (const auto& f : mesh->getFacets())
//    {
//        vtkSmartPointer<vtkTriangle> triangle = vtkSmartPointer<vtkTriangle>::New();

//        for (unsigned int j = 0; j < f.getSize(); j++) //0..2
//            triangle->GetPointIds()->SetId(j, f.getIndex(j));

//        cellArray->InsertNextCell(triangle);
//    }

    vtkSmartPointer<vtkUnstructuredGrid> unstructuredGrid = vtkSmartPointer<vtkUnstructuredGrid>::New();
    unstructuredGrid->SetPoints(points);
    unstructuredGrid->SetCells(VTK_TRIANGLE, cellArray);

    if (mesh->getNbLabels())
    {
        vtkSmartPointer<vtkFloatArray> pArray = vtkSmartPointer<vtkFloatArray>::New();
        pArray->SetNumberOfValues(mesh->getNbPoints());
        pArray->SetName("Point Data");
        int vertex = 0;
        for (const auto& p : mesh->getPositions())
            pArray->SetValue(vertex++, (float)p.getLabel(0));
        vtkSmartPointer<vtkPointData> pointDataArray = unstructuredGrid->GetPointData();
        pointDataArray->AddArray(pArray);


        vtkSmartPointer<vtkFloatArray> cArray = vtkSmartPointer<vtkFloatArray>::New();
        cArray->SetName("Cell Data");
        cArray->SetNumberOfValues(mesh->getNbCells());
        int cell = 0;
        for (const auto& c : mesh->getCells())
            cArray->SetValue(cell++, (float)c.getLabel(0));
        vtkSmartPointer<vtkCellData> cellDataArray = unstructuredGrid->GetCellData();
        cellDataArray->AddArray(cArray);

    }


    // Write file
    vtkSmartPointer<vtkXMLUnstructuredGridWriter> writer = vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();
    writer->SetFileName(filename.c_str());
    #if VTK_MAJOR_VERSION <= 5
    writer->SetInput(unstructuredGrid);
    #else
    writer->SetInputData(unstructuredGrid);
    #endif
    writer->Write();


    std::cerr << "Successfully write "<< *mesh << " to " << filename << std::endl;
}

void VTUExport::exportToFile(BaseMesh *mesh, std::string filename, Transform t)
{
    if (PolygonMesh<Triangle3D>* m = dynamic_cast<PolygonMesh<Triangle3D>* >(mesh))
    {
        this->exportToFile(m, filename, t);
    }
    else if (PolygonMesh<Tetrahedron3D>* m = dynamic_cast<PolygonMesh<Tetrahedron3D>* >(mesh))
    {
        this->exportToFile(m, filename, t);
    }
    else
    {
        std::cout << "This type of mesh cannot be exported in VTK format" << std::endl;
    }
}
