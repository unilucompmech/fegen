#include "polygon3d.h"


template<>
std::vector<Polygon3D<2>::Facet> Polygon3D<2>::getFacets() const
{
    std::vector<Polygon3D<2>::Facet> facets;

    Edge3D::Facet f1;
    f1.setPoint(0, this->getIndex(0));
    facets.push_back(f1);

    Edge3D::Facet f2;
    f2.setPoint(0, this->getIndex(1));
    facets.push_back(f2);

    return facets;
}

template<>
std::vector<Polygon3D<3>::Facet> Polygon3D<3>::getFacets() const
{
    std::vector<Polygon3D<3>::Facet> facets;

    Triangle3D::Facet f1;
    f1.setPoint(0, std::min(this->getIndex(0), this->getIndex(1)) );
    f1.setPoint(1, std::max(this->getIndex(0), this->getIndex(1)) );
    facets.push_back(f1);

    Triangle3D::Facet f2;
    f2.setPoint(0, std::min(this->getIndex(1), this->getIndex(2)) );
    f2.setPoint(1, std::max(this->getIndex(1), this->getIndex(2)) );
    facets.push_back(f2);

    Triangle3D::Facet f3;
    f3.setPoint(0, std::min(this->getIndex(2), this->getIndex(0)) );
    f3.setPoint(1, std::max(this->getIndex(2), this->getIndex(0)) );
    facets.push_back(f3);

    return facets;
}

template<>
std::vector<Polygon3D<4>::Facet> Polygon3D<4>::getFacets() const
{
    std::vector<Polygon3D<4>::Facet> facets;

    for (unsigned int i = 0 ; i < 4; i++)
    {
        if ( this->getIndex(i) < this->getIndex((i+1)%4) && this->getIndex(i) < this->getIndex((i+2)%4))
            facets.push_back(Triangle3D(this->getIndex(i), this->getIndex((i+1)%4), this->getIndex((i+2)%4)));
        else if( this->getIndex((i+1)%4) < this->getIndex(i) && this->getIndex((i+1)%4) < this->getIndex((i+2)%4))
            facets.push_back(Triangle3D(this->getIndex((i+1)%4), this->getIndex((i+2)%4), this->getIndex(i)));
        else
            facets.push_back(Triangle3D(this->getIndex((i+2)%4), this->getIndex(i), this->getIndex((i+1)%4)));
    }

    return facets;
}




Triangle3D::Triangle3D(PointID a, PointID b, PointID c)
{
    setPoint(0,a);
    setPoint(1,b);
    setPoint(2,c);
}



Tetrahedron3D::Tetrahedron3D(PointID a, PointID b, PointID c, PointID d)
{
    setPoint(0,a);
    setPoint(1,b);
    setPoint(2,c);
    setPoint(3,d);
}




Edge3D::Edge3D(PointID a, PointID b)
{
    setPoint(0,a);
    setPoint(1,b);
}

