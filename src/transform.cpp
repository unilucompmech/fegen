#include "transform.h"

Transform::Transform()
{
    scale    = Vec(1,1,1);
    rotScale = Vec(1,1,1);
    rotation = Quat();
}

Transform::Vec& Transform::getScale()
{
    return scale;
}

void Transform::setScale(const Vec &value)
{
    scale = value;
}

void Transform::setScale(const Transform::float_type &value)
{
    scale = Vec(value,value,value);
}

void Transform::addScale(const Transform::float_type &value)
{
    scale *= value;
}

Transform::Vec& Transform::getTranslation()
{
    return translation;
}

void Transform::setTranslation(const Vec &value)
{
    translation = value;
}

Transform::Quat& Transform::getRotation()
{
    return rotation;
}

Transform::Quat Transform::getRotation() const
{
    return rotation;
}

void Transform::setRotation(const Quat &value)
{
    rotation = value;
}

Transform::Vec &Transform::getRotScale()
{
    return rotScale;
}

void Transform::setRotScale(const Vec &value)
{
    rotScale = value;
}



Transform::Mat44 Transform::toMatrix() const
{
    Mat44 m;
    m.identity();

    RotationMatrix<float_type> rotation;
    rotation.fromQuaternion(this->getRotation());

    Mat44 m1; m1.identity();
    Mat44 m2; m2.identity();
    Mat44 m3; m3.identity();

    for (unsigned int i = 0 ; i < 3; i++)
        for (unsigned int j = 0 ; j < 3; j++)
            m1(i,j) = rotation(i,j) * rotScale[j];
    for (unsigned int i = 0 ; i < 3; i++)
            m2(i,i) *= scale[i];

    m=m2*m1;

    for (unsigned int i = 0 ; i < 3; i++)
            m(i,3) = translation[i];

    return m;
}





