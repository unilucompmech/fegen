#include "objexporter.h"

#include <fstream>

OBJExporter::OBJExporter()
{

}

void OBJExporter::exportToFile(PolygonMesh<Tetrahedron3D> *mesh, std::string filename, Transform t)
{
    std::cout << "Writing " << *mesh << " to " << filename << " ..." << std::endl;

    std::ofstream* outfile = new std::ofstream(filename.c_str());

    if( !outfile->is_open() )
    {
        std::cerr << "Error creating file "<< filename << std::endl;
        delete outfile;
        outfile = NULL;
        return;
    }

    if (mesh->getNbPoints() && mesh->getNbCells())
    {
        for (int i=0 ; i < mesh->getNbPoints(); i++)
        {
            Point3D<> p = mesh->getPosition(i);
            p.transform(t);
            *outfile << "v " << p.getX() << " " << p.getY() << " " << p.getZ() << std::endl;
        }
        std::vector<PolygonMesh<Tetrahedron3D>::FacetType> triangles = mesh->getFacets();
        for (const auto& tri : triangles)
        {
            if (mesh->getCellsAroundFacet(tri).size() == 1 )
            {
                *outfile << "f ";
                for (unsigned int j = 0 ; j < tri.getSize(); j++)
                    *outfile << (tri.getIndex(j)+1) << " ";
                *outfile << std::endl;
            }
        }
    }

    outfile->close();
    std::cerr << "Successfully write "<< *mesh << " to " << filename << std::endl;
}

void OBJExporter::exportToFile(PolygonMesh<Triangle3D> *mesh, std::string filename, Transform t)
{
    std::cout << "Writing " << *mesh << " to " << filename << " ..." << std::endl;

    std::ofstream* outfile = new std::ofstream(filename.c_str());

    if( !outfile->is_open() )
    {
        std::cerr << "Error creating file "<< filename << std::endl;
        delete outfile;
        outfile = NULL;
        return;
    }

    if (mesh->getNbPoints() && mesh->getNbCells())
    {
        for (int i=0 ; i < mesh->getNbPoints(); i++)
        {
            Point3D<> p = mesh->getPosition(i);
            p.transform(t);
            *outfile << "v " << p.getX() << " " << p.getY() << " " << p.getZ() << std::endl;
        }
        std::vector<PolygonMesh<Triangle3D>::CellType> triangles = mesh->getCells();
        for (const auto& tri : triangles)
        {
            *outfile << "f ";
            for (unsigned int j = 0 ; j < tri.getSize(); j++)
                *outfile << (tri.getIndex(j)+1) << " ";
            *outfile << std::endl;
        }
    }

    outfile->close();
    std::cerr << "Successfully write "<< *mesh << " to " << filename << std::endl;
}

void OBJExporter::exportToFile(BaseMesh *mesh, std::string filename, Transform t)
{
    if (PolygonMesh<Triangle3D>* m = dynamic_cast<PolygonMesh<Triangle3D>* >(mesh))
    {
        this->exportToFile(m, filename, t);
    }
    else if (PolygonMesh<Tetrahedron3D>* m = dynamic_cast<PolygonMesh<Tetrahedron3D>* >(mesh))
    {
        this->exportToFile(m, filename, t);
    }
    else
    {
        std::cout << "This type of mesh cannot be exported in OBJ format" << std::endl;
    }
}

