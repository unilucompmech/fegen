#ifndef VTKREADER_H
#define VTKREADER_H

#include <iostream>
#include <string>

#include <vtkXMLUnstructuredGridReader.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkGenericDataObjectReader.h>
#include <vtkSmartPointer.h>
#include <vtkPoints.h>
#include <vtkCell.h>
#include <vtkTriangle.h>
#include <vtkTetra.h>
#include <vtkPolyData.h>
#include <vtkUnstructuredGrid.h>

#include "meshfilereader.h"

//! Read a mesh in VTK format
class VTKReader : public MeshFileReader
{
public:
    VTKReader();
    VTKReader(std::string filename);

    void readMesh();
};

#endif // VTKREADER_H
