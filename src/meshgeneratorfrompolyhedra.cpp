#include "meshgeneratorfrompolyhedra.h"


MeshGeneratorFromPolyhedra::MeshGeneratorFromPolyhedra(PolygonMesh<Triangle3D> *mesh)
{
    std::cout << "Create the mesh generator from " << *mesh << std::endl;

    inputMesh = mesh;
}


void MeshGeneratorFromPolyhedra::init()
{
    polyhedron_builder<Polyhedron::HalfedgeDS> builder( this->inputMesh  );
    std::cout << "CGAL Builder has been created" << std::endl;
    polyhedron.delegate( builder );

    std::cout << "[CGAL] " << polyhedron.size_of_vertices() << " vertices, " << polyhedron.size_of_facets() << " facets." << std::endl;

    if (polyhedron.size_of_vertices() == 0 || polyhedron.size_of_facets() == 0)
    {
        std::cout << "No surface to compute a mesh" << std::endl;
        return;
    }

}

void MeshGeneratorFromPolyhedra::computeMesh(MeshGenerationParameters *parameters)
{
    std::cout << "[MeshGeneratorFromPolyhedra]::computeMesh()" << std::endl;

    // Create domain
    std::cout << "Create domain" << std::endl;
    Mesh_domain domain(polyhedron);

    double facet_angle = 25;
    double facet_size = 0.15;
    double facet_distance = 0.008;
    double cell_radius_edge_ratio = 3;
    double cell_size = 0.15;

    if (CGALParameters* param = dynamic_cast<CGALParameters*>(parameters))
    {
        facet_angle = param->facet_angle;
        facet_size = param->facet_size;
        facet_distance = param->facet_approximation;
        cell_radius_edge_ratio = param->cell_radius_edge_ratio;
        cell_size = param->cell_size;
    }
    else
    {
        std::cout << "Wrong parameters type" << std::endl;
    }

    // Mesh criteria (no cell_size set)
    Mesh_criteria criteria(CGAL::parameters::facet_angle=facet_angle
            , CGAL::parameters::facet_size=facet_size
            , CGAL::parameters::facet_distance=facet_distance
            , CGAL::parameters::cell_radius_edge_ratio=cell_radius_edge_ratio
            , CGAL::parameters::cell_size=cell_size);

    // Mesh generation
    std::cout << "Make mesh" << std::endl;
    c3t3 = CGAL::make_mesh_3<C3t3>(domain, criteria, CGAL::parameters::no_perturb(), CGAL::parameters::no_exude());

    this->convertToInternalMesh();
    std::cout << "Computed mesh:  " << outputMesh << std::endl;

}

