#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QApplication>
#include <QPushButton>
#include <QVBoxLayout>
#include <QPainter>

#include "ui_mainwindow.h"
#include "bboxwidget.h"
#include "cgalparameterswidget.h"
#include "outputinfowidget.h"
#include "cylindergenerationwidget.h"

#include "meshgenerator.h"

#include "filereader.h"
#include "objreader.h"
#include "vtkreader.h"
#include "niftireader.h"

#include "imageslice.h"
//#include "meshviewer.h"

#include "basemesh.h"
#include "meshgenerationparameters.h"


#define IMAGE_VIEW_PROPORTION 0.4


class BBoxWidgetList;
class BBoxWidget;


enum class MeshingMethod
{
    NONE,
    CONVERSION,
    CYLINDER,
    CGAL
};

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


signals:

public slots:
    void openInputFile();
    void setOutputFile();
    void computeMesh();
    void exportMesh();
    void updateImagesView();
    void setMeshingMethod(QString text);

    void updateImagesViewX(int newValue);
    void updateImagesViewY(int newValue);
    void updateImagesViewZ(int newValue);

    void addLabel();

    void updateUIFromInputFile();
    void updateUIFromOutput();
    void setLabelDomainsFromUI();

protected:
    Ui::MainWindow ui;

    BaseMesh* mesh;
    MeshGenerator* meshGenerator;
    FileReader* reader;

    void openFilename(QString inputFilename);
    void setOutputFile(QString outputFilename);

    void closeEvent(QCloseEvent *event);
    void resizeEvent(QResizeEvent *event);

    void writeWindowSettings();
    void readWindowSettings();

    MeshingMethod getMeshingMethod();


    bool inputLoaded;
    bool meshComputed;

    std::string outFilename;

    std::shared_ptr<MeshGenerationParameters> meshParameters;

    QTreeWidget* inputWidget;
    QTreeWidget* outputWidget;

    QPushButton* addSubdomainButton;
//    QVBoxLayout* labelList;
    BBoxWidgetList* labelList;

    QLabel* imageXLabel;
    QLabel* imageYLabel;
    QLabel* imageZLabel;

    QSlider* sliderX;
    QSlider* sliderY;
    QSlider* sliderZ;

    QLabel* imageCaptionX;
    QLabel* imageCaptionY;
    QLabel* imageCaptionZ;

    ImageSlice* slice;
//    MeshViewer* meshViewer;

    QWidget* imageWidget;
    QGridLayout* imageLayout;

    QWidget* meshParametersWidget;
    OutputInfoWidget *outputInfoWidget;

};



#endif // MAINWINDOW_H
