#ifndef MESHGENERATORFROMIMAGE_H
#define MESHGENERATORFROMIMAGE_H

#include "cgalmeshgenerator.h"
#include "filereader.h"
#include "imagefilereader.h"
#include "meshgenerationparameters.h"


//! Generate a mesh from a 3D image
class MeshGeneratorFromImage  : public CGALMeshGenerator
{
public:
    MeshGeneratorFromImage();
    MeshGeneratorFromImage(FileReader* reader);

    void computeMesh(MeshGenerationParameters* parameters) override;
    void init() override;

protected:
    FileReader* reader;
};

#endif // MESHGENERATORFROMIMAGE_H
