#include <QApplication>
#include <QQmlApplicationEngine>

#include "FEgenConfig.h"

#include "mainwindow.h"

void printHelp();

int main(int argc, char *argv[])
{
    if (argc > 1)
    {
        for (unsigned int i = 1 ; i < argc; i++)
        {
            if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0) //equal
            {
                printHelp();
                return 0;
            }
        }

    }

    QApplication app(argc, argv);

    MainWindow *window = new MainWindow;
    window->show();

    return app.exec();
}


void printHelp()
{
    std::cout << "Usage: " << FEgen_PROJECT_NAME << " [OPTION]" << std::endl;
    std::cout << "Options: " << FEgen_PROJECT_NAME << " [OPTION]" << std::endl;
    std::cout << std::endl;

    int reducedWidth = 5;
    int expandedWidth = 10;
    int descriptionWidth = 30;
    const char separator = ' ';

    std::cout << std::left << std::setw(reducedWidth) << std::setfill(separator) << "-i";
    std::cout << std::left << std::setw(expandedWidth) << std::setfill(separator) << "--input";
    std::cout << std::left << std::setw(descriptionWidth) << std::setfill(separator) << "Input file";
    std::cout << std::endl;

    std::cout << std::left << std::setw(reducedWidth) << std::setfill(separator) << "-o";
    std::cout << std::left << std::setw(expandedWidth) << std::setfill(separator) << "--output";
    std::cout << std::left << std::setw(descriptionWidth) << std::setfill(separator) << "Output file";
    std::cout << std::endl;

    std::cout << std::left << std::setw(reducedWidth) << std::setfill(separator) << "-h";
    std::cout << std::left << std::setw(expandedWidth) << std::setfill(separator) << "--help";
    std::cout << std::left << std::setw(descriptionWidth) << std::setfill(separator) << "This help";
    std::cout << std::endl;

    std::cout << std::endl;
}
