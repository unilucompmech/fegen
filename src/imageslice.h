#ifndef IMAGE2D_H
#define IMAGE2D_H

#include <QImage>

#include "baseimage.h"

//! A slice in a 3D image
class ImageSlice
{
public:

    ImageSlice(std::shared_ptr<BaseImage> image3d);


    QImage getSliceX(int dimX, int dimY, double slice);
    QImage getSliceY(int dimX, int dimY, double slice);
    QImage getSliceZ(int dimX, int dimY, double slice);

    QImage getSliceX(int slice);
    QImage getSliceY(int slice);
    QImage getSliceZ(int slice);
protected:

    std::shared_ptr<BaseImage> image3d;
    bool expand;

//    unsigned char* dataX;
//    unsigned char* dataY;
//    unsigned char* dataZ;

//    unsigned char** dataUChar;
//    void** data;
    std::shared_ptr<unsigned char> data[3];

    template<class T> void setData(int plane, int index, T value);

};

#endif // IMAGE2D_H
