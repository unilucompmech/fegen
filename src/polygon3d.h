#ifndef POLYGON3D_H
#define POLYGON3D_H

#include <vector>
#include <climits>
#include <iostream>
#include <assert.h>
#include <algorithm>
#include <array>

//! Polygon made of indices of the different vertices
template<std::size_t N>
class Polygon3D
{
public:
    using PointID = unsigned int;

    static const long unsigned int size = N;

    using Facet = Polygon3D<N-1>;

    Polygon3D(){}
    Polygon3D(const Polygon3D<N>& p) : indices(p.indices){}
    ~Polygon3D(){}

    void setPoint(unsigned int i, PointID index)
    {
        if (i < this->getSize())
            indices[i] = index;
    }
    unsigned int getSize() const
    {
        return N;
    }

    PointID getIndex(unsigned int i) const
    {
        assert(i < this->getSize());
        return indices[i];
    }

    friend std::ostream& operator<<(std::ostream &os, const Polygon3D<N>& p)
    {
        os << "[";
        for (unsigned int i = 0 ; i < p.getSize(); ++i)
        {
            os << p.getIndex(i);
            if (i < p.getSize()-1) os << ", ";
        }
        os << "]";
        return os;
    }

    bool operator==(const Polygon3D<N>& p2 ) const
    {
        return this->equalTo_slow(p2);
    }

    bool equalTo_slow(const Polygon3D<N>& p2) const
    {
        for (const auto& p : indices)
            if (std::find(p2.indices.begin(), p2.indices.end(), p) == p2.indices.end())
                return false;

        return true;
    }

    bool equalTo_fast(const Polygon3D<N>& p2) const
    {
        return indices == p2.indices;
    }

    std::vector<Facet> getFacets() const
    {
        return std::vector<Facet>();
    }

public:
    std::array<PointID,N> indices;
};


template<> std::vector<Polygon3D<4>::Facet> Polygon3D<4>::getFacets() const;
template<> std::vector<Polygon3D<3>::Facet> Polygon3D<3>::getFacets() const;
template<> std::vector<Polygon3D<2>::Facet> Polygon3D<2>::getFacets() const;


class Edge3D : public Polygon3D<2>
{
public:
    Edge3D(){}
    ~Edge3D(){}
    Edge3D(PointID a, PointID b);
};

class Triangle3D : public Polygon3D<3>
{
public:
    Triangle3D(){}
    ~Triangle3D(){}
    Triangle3D(PointID a, PointID b, PointID c);
};

class Tetrahedron3D : public Polygon3D<4>
{
public:
    Tetrahedron3D(){}
    ~Tetrahedron3D(){}
    Tetrahedron3D(PointID a, PointID b, PointID c, PointID d);
};

#endif // POLYGON3D_H
