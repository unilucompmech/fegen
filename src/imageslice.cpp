#include "imageslice.h"

#include <limits>


ImageSlice::ImageSlice(std::shared_ptr<BaseImage> image3d): image3d(image3d), expand(true)
{
}


QImage ImageSlice::getSliceX(int dimX, int dimY, double slice)
{
    const int nbPix = dimX*dimY;
    unsigned char * data = new unsigned char[nbPix];

    unsigned char min = std::numeric_limits<unsigned char>::max();
    unsigned char max = std::numeric_limits<unsigned char>::min();

    if (std::shared_ptr<Image<unsigned char> > img = std::dynamic_pointer_cast<Image<unsigned char> >(this->image3d))
    {
        for (unsigned int i = 0 ; i < dimX; i++)
        {
            for (unsigned int j = 0 ; j < dimY; j++)
            {
                double x = slice;
                double y = (double)(i * img->getDimY()) / (double)(dimX-1);
                double z = (double)(j * img->getDimZ()) / (double)(dimY-1);
                unsigned char intensity = img->getIntensityAtPoint(x,y,z);
                data[j*dimX + i] = intensity;
                if (intensity > max) max = intensity;
                if (intensity < min) min = intensity;
            }
        }
    }
    else
    {
        std::cerr << "Cannot convert to 2d image" << std::endl;
        return QImage(dimX, dimY, QImage::Format_Indexed8);
    }

    return QImage(data, dimX, dimY, QImage::Format_Indexed8);
}

QImage ImageSlice::getSliceY(int dimX, int dimY, double slice)
{
    const int nbPix = dimX*dimY;
    unsigned char * data = new unsigned char[nbPix];

    unsigned char min = std::numeric_limits<unsigned char>::max();
    unsigned char max = std::numeric_limits<unsigned char>::min();

    if (std::shared_ptr<Image<unsigned char> > img = std::dynamic_pointer_cast<Image<unsigned char> >(image3d))
    {
        for (unsigned int i = 0 ; i < dimX; i++)
        {
            for (unsigned int j = 0 ; j < dimY; j++)
            {
                double x = (double)(i * img->getDimX()) / (double)(dimX-1);
                double y = slice;
                double z = (double)(j * img->getDimZ()) / (double)(dimY-1);
                unsigned char intensity = img->getIntensityAtPoint(x,y,z);
                data[j*dimX + i] = intensity;
                if (intensity > max) max = intensity;
                if (intensity < min) min = intensity;
            }
        }
    }
    else
    {
        std::cerr << "Cannot convert to 2d image" << std::endl;
        return QImage(dimX, dimY, QImage::Format_Indexed8);
    }

//    std::cout << "Min intensity: " << static_cast<unsigned>(min) << std::endl;
//    std::cout << "Max intensity: " << static_cast<unsigned>(max) << std::endl;

    return QImage(data, dimX, dimY, QImage::Format_Indexed8);
}

QImage ImageSlice::getSliceZ(int dimX, int dimY, double slice)
{
    const int nbPix = dimX*dimY;
    unsigned char * data = new unsigned char[nbPix];

    unsigned char min = std::numeric_limits<unsigned char>::max();
    unsigned char max = std::numeric_limits<unsigned char>::min();

    if (std::shared_ptr<Image<unsigned char> > img = std::dynamic_pointer_cast<Image<unsigned char> >(image3d))
    {
        for (unsigned int i = 0 ; i < dimX; i++)
        {
            for (unsigned int j = 0 ; j < dimY; j++)
            {
                double x = (double)(i * img->getDimX()) / (double)(dimX-1);
                double y = (double)(j * img->getDimY()) / (double)(dimY-1);
                double z = slice;
                unsigned char intensity = img->getIntensityAtPoint(x,y,z);
                data[j*dimX + i] = intensity;
                if (intensity > max) max = intensity;
                if (intensity < min) min = intensity;
            }
        }
    }
    else
    {
        std::cerr << "Cannot convert to 2d image" << std::endl;
        return QImage(dimX, dimY, QImage::Format_Indexed8);
    }

    return QImage(data, dimX, dimY, QImage::Format_Indexed8);
}

QImage ImageSlice::getSliceX(int slice)
{
    if (!image3d)
    {
        std::cerr << "No image provided" << std::endl;
        return QImage();
    }

    unsigned char min = std::numeric_limits<unsigned char>::max();
    unsigned char max = std::numeric_limits<unsigned char>::min();

    if (!data[0])
    {
        int length = this->image3d->getDimY() * this->image3d->getDimZ();
        data[0] = std::shared_ptr<unsigned char>(new unsigned char[length], std::default_delete<unsigned char[]>());
    }

    if (std::shared_ptr<Image<unsigned char> > img = std::dynamic_pointer_cast<Image<unsigned char> >(this->image3d))
    {
        for (unsigned int i = 0 ; i < this->image3d->getDimY(); i++)
        {
            for (unsigned int j = 0 ; j < this->image3d->getDimZ(); j++)
            {
                int x = slice;
                int y = i;
                int z = j;
                float range = img->getMax() - img->getMin();
                if (range)
                {
                    unsigned char intensity = 255 * (img->getVoxel(x,y,z)- img->getMin()) / range;
                    setData<unsigned char>(0, j * this->image3d->getDimY() + i, intensity);
    //                data[0][j * this->image3d->getDimY() + i] = intensity;
                    if (intensity > max) max = intensity;
                    if (intensity < min) min = intensity;
                }
            }
        }
    }
    else if (std::shared_ptr<Image<float> > img = std::dynamic_pointer_cast<Image<float> >(this->image3d))
    {
        for (unsigned int i = 0 ; i < this->image3d->getDimY(); i++)
        {
            for (unsigned int j = 0 ; j < this->image3d->getDimZ(); j++)
            {
                int x = slice;
                int y = i;
                int z = j;
                float range = img->getMax() - img->getMin();
                if (range)
                {
                    float intensity_f = (img->getVoxel(x,y,z) - img->getMin()) / range;
                    unsigned char intensity = intensity_f * 255;
    //                dataX[j * this->image3d->getDimY() + i] = intensity;
                    setData<unsigned char>(0, j * this->image3d->getDimY() + i, intensity);
                    if (intensity > max) max = intensity;
                    if (intensity < min) min = intensity;
                }
            }
        }
    }
    else
    {
        std::cerr << "Cannot convert to 2d image" << std::endl;
        return QImage(this->image3d->getDimY(), this->image3d->getDimZ(), QImage::Format_Indexed8);
    }

    return QImage(data[0].get(), this->image3d->getDimY(), this->image3d->getDimZ(), QImage::Format_Indexed8);

}

QImage ImageSlice::getSliceY(int slice)
{
    if (!image3d)
    {
        std::cerr << "No image provided" << std::endl;
        return QImage();
    }

    if (!data[1])
    {
        int length = this->image3d->getDimX() * this->image3d->getDimZ();
        data[1] = std::shared_ptr<unsigned char>(new unsigned char[length], std::default_delete<unsigned char[]>());
    }

    unsigned char min = std::numeric_limits<unsigned char>::max();
    unsigned char max = std::numeric_limits<unsigned char>::min();

    if (std::shared_ptr<Image<unsigned char> > img = std::dynamic_pointer_cast<Image<unsigned char> >(this->image3d))
    {
        for (unsigned int i = 0 ; i < this->image3d->getDimX(); i++)
        {
            for (unsigned int j = 0 ; j < this->image3d->getDimZ(); j++)
            {
                int x = i;
                int y = slice;
                int z = j;
                float range = img->getMax() - img->getMin();
                if (range)
                {
                    unsigned char intensity = 255 * (img->getVoxel(x,y,z)- img->getMin()) / range;
//                dataY[j * this->image3d->getDimX() + i] = intensity;
                    setData<unsigned char>(1, j * this->image3d->getDimX() + i, intensity);
                    if (intensity > max) max = intensity;
                    if (intensity < min) min = intensity;
                }
            }
        }
    }
    else if (std::shared_ptr<Image<float> > img = std::dynamic_pointer_cast<Image<float> >(this->image3d))
    {
        for (unsigned int i = 0 ; i < this->image3d->getDimX(); i++)
        {
            for (unsigned int j = 0 ; j < this->image3d->getDimZ(); j++)
            {
                int x = i;
                int y = slice;
                int z = j;
                float range = img->getMax() - img->getMin();
                float intensity_f = (img->getVoxel(x,y,z) - img->getMin()) / range;
                unsigned char intensity = intensity_f * 255;
//                dataX[j * this->image3d->getDimY() + i] = intensity;
                setData<unsigned char>(1, j * this->image3d->getDimX() + i, intensity);
                if (intensity > max) max = intensity;
                if (intensity < min) min = intensity;
            }
        }
    }
    else
    {
        std::cerr << "Cannot convert to 2d image" << std::endl;
        return QImage(this->image3d->getDimX(), this->image3d->getDimZ(), QImage::Format_Indexed8);
    }

    return QImage(data[1].get(), this->image3d->getDimX(), this->image3d->getDimZ(), QImage::Format_Indexed8);
}

QImage ImageSlice::getSliceZ(int slice)
{
    if (!image3d)
    {
        std::cerr << "No image provided" << std::endl;
        return QImage();
    }

    if (!data[2])
    {
        int length = this->image3d->getDimX() * this->image3d->getDimY();
        data[2] = std::shared_ptr<unsigned char>(new unsigned char[length], std::default_delete<unsigned char[]>());
    }

    unsigned char min = std::numeric_limits<unsigned char>::max();
    unsigned char max = std::numeric_limits<unsigned char>::min();

    if (std::shared_ptr<Image<unsigned char> > img = std::dynamic_pointer_cast<Image<unsigned char> >(this->image3d))
    {
        for (unsigned int i = 0 ; i < this->image3d->getDimX(); i++)
        {
            for (unsigned int j = 0 ; j < this->image3d->getDimY(); j++)
            {
                int x = i;
                int y = j;
                int z = slice;
                float range = img->getMax() - img->getMin();
                if (range)
                {
                    unsigned char intensity = 255 * (img->getVoxel(x,y,z)- img->getMin()) / range;
//                dataZ[j * this->image3d->getDimX() + i] = intensity;
                    setData<unsigned char>(2, j * this->image3d->getDimX() + i, intensity);
                    if (intensity > max) max = intensity;
                    if (intensity < min) min = intensity;
                }
            }
        }
    }
    else if (std::shared_ptr<Image<float> > img = std::dynamic_pointer_cast<Image<float> >(this->image3d))
    {
        for (unsigned int i = 0 ; i < this->image3d->getDimX(); i++)
        {
            for (unsigned int j = 0 ; j < this->image3d->getDimY(); j++)
            {
                int x = i;
                int y = j;
                int z = slice;
                float range = img->getMax() - img->getMin();
                float intensity_f = (img->getVoxel(x,y,z) - img->getMin()) / range;
                unsigned char intensity = intensity_f * 255;
//                dataX[j * this->image3d->getDimY() + i] = intensity;
                setData<unsigned char>(2, j * this->image3d->getDimX() + i, intensity);
                if (intensity > max) max = intensity;
                if (intensity < min) min = intensity;
            }
        }
    }
    else
    {
        std::cerr << "Cannot convert to 2d image" << std::endl;
        return QImage(this->image3d->getDimX(), this->image3d->getDimY(), QImage::Format_Indexed8);
    }

    return QImage(data[2].get(), this->image3d->getDimX(), this->image3d->getDimY(), QImage::Format_Indexed8);
}

template<class T>
void ImageSlice::setData(int plane, int index, T value)
{
    if (data[plane])
    {
        *(data[plane].get() + index) = value;
    }
}

