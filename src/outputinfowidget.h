#ifndef OUTPUTINFOWIDGET_H
#define OUTPUTINFOWIDGET_H

#include <sstream>

#include <QTreeWidget>

#include "basemesh.h"
#include "boundingbox.h"

//! Qt widget to display info about the computed mesh
class OutputInfoWidget : public QTreeWidget
{
    Q_OBJECT
public:
    explicit OutputInfoWidget(QWidget *parent = 0);

    void setOutputRoot();
    void addNbVertices(unsigned int nb);
    void addNbCells(unsigned int nb);
    void addNbFacets(unsigned int nb);
    void addBoundingBox(BoundingBox<> bbox);
    void addLabel(double labelValue, unsigned int nbVertices, unsigned int nbFacets, unsigned int nbCells);
    void addVertices(std::vector<MeshVertex<> > vertices);

signals:

public slots:
    void clear();

protected:
    QTreeWidgetItem * rootItem;
    QTreeWidgetItem * labelRootItem;

};

#endif // OUTPUTINFOWIDGET_H
