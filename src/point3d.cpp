#include "point3d.h"

#include <math.h>

template<class T>
T Point3D<T>::distance(Point3D p)
{
    return sqrt( (p.getX() - getX())*(p.getX() - getX()) + (p.getY() - getY())*(p.getY() - getY()) + (p.getZ() - getZ())*(p.getZ() - getZ()));
}

template<class T>
void Point3D<T>::transform(const Transform &t)
{
    Point3D<T> pT = t.transform(*this);
    for (unsigned int i = 0 ; i  < pT.size; i++)
        this->data[i] = pT[i];
}



template class Point3D<float>;
template class Point3D<double>;
