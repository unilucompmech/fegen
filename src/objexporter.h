#ifndef OBJEXPORTER_H
#define OBJEXPORTER_H

#include "fileexport.h"

//! Write a mesh in OBJ Wavefront format
class OBJExporter : public FileExport
{
public:
    OBJExporter();

    void exportToFile(PolygonMesh<Tetrahedron3D> *mesh, std::string filename, Transform t = Transform());
    void exportToFile(PolygonMesh<Triangle3D>    *mesh, std::string filename, Transform t = Transform());
    void exportToFile(BaseMesh                   *mesh, std::string filename, Transform t = Transform()) override;


};

#endif // OBJEXPORTER_H
