#ifndef NIFTIREADER_H
#define NIFTIREADER_H

#include "imagefilereader.h"
#include "baseimage.h"
#include "vector.h"
#include "matrix.h"

#include <nifti/nifti1_io.h>

#include <sstream>
#include <iomanip>

//! Read a NIfTI 3D image
class NiftiReader : public ImageFileReader
{
public:
    NiftiReader();
    NiftiReader(std::string filename)
    {
        this->setFilename(filename);

        nifti_image * nim = nifti_image_read(filename.c_str(), 1);

        if( !nim )
        {
           std::cerr << "Failed to read NIfTI image from " << filename << std::endl;
           return;
        }

        std::cout << "Sucessfully read NIfTI image from " << filename << " with "; nifti_disp_lib_version();

        // show image info
//        nifti_image_infodump(nim);

        int datatype = nim->datatype;
        std::cout << std::left << std::setw(22) << std::setfill(' ') << "Image datatype:";
        std::cout << std::left << std::setw(22) << std::setfill(' ') << datatype;
        std::cout << std::endl;

        size_t nvox = nim->nvox;
        std::cout << std::left << std::setw(22) << std::setfill(' ') << "Nb voxels:";
        std::cout << std::left << std::setw(22) << std::setfill(' ') << nvox;
        std::cout << std::endl;

        std::cout << std::left << std::setw(22) << std::setfill(' ') << "Dimension of image:";
        std::cout << std::left << std::setw(22) << std::setfill(' ') << nim->ndim;
        std::cout << std::endl;

//        std::cout << "Nb voxels in dimension 0: " << nim->nx << std::endl;
//        if( nim->ndim > 1 ) std::cout << "Nb voxels in dimension 1: " << nim->ny << std::endl;
//        if( nim->ndim > 2 ) std::cout << "Nb voxels in dimension 2: " << nim->nz << std::endl;
//        if( nim->ndim > 3 ) std::cout << "Nb voxels in dimension 3: " << nim->nt << std::endl;
//        if( nim->ndim > 4 ) std::cout << "Nb voxels in dimension 4: " << nim->nu << std::endl;
//        if( nim->ndim > 5 ) std::cout << "Nb voxels in dimension 5: " << nim->nv << std::endl;
//        if( nim->ndim > 6 ) std::cout << "Nb voxels in dimension 6: " << nim->nw << std::endl;
//        std::cout << "Voxel size in dimension 0: " << nim->dx << std::endl;
//        if( nim->ndim > 1 ) std::cout << "Voxel size in dimension 1: " << nim->dy << std::endl;
//        if( nim->ndim > 2 ) std::cout << "Voxel size in dimension 2: " << nim->dz << std::endl;
//        if( nim->ndim > 3 ) std::cout << "Voxel size in dimension 3: " << nim->dt << std::endl;
//        if( nim->ndim > 4 ) std::cout << "Voxel size in dimension 4: " << nim->du << std::endl;
//        if( nim->ndim > 5 ) std::cout << "Voxel size in dimension 5: " << nim->dv << std::endl;
//        if( nim->ndim > 6 ) std::cout << "Voxel size in dimension 6: " << nim->dw << std::endl;

        if (nim->ndim != 3)
        {
            std::cerr << "Only images with 3 dimensions are supported" << std::endl;
            return;
        }

        int dim[3];
        dim[0]=nim->nx;
        dim[1]=nim->ny;
        dim[2]=nim->nz;

        std::ostringstream converter;
        converter << dim[0] << "x" << dim[1] << "x" << dim[2];
        std::cout << std::left << std::setw(22) << std::setfill(' ') << "Image dimensions:";
        std::cout << std::left << std::setw(22) << std::setfill(' ') << converter.str();
        std::cout << std::endl;

        switch(datatype)
        {
            case 2:
            {
            #ifdef IMAGE_FILE_READER_USE_SMART_POINTER
                image = std::shared_ptr<Image<unsigned char> >(new Image<unsigned char>(dim[0], dim[1], dim[2]));
                std::shared_ptr<Image<unsigned char> > img = std::dynamic_pointer_cast<Image<unsigned char> >(image);
            #else
                image = new Image<unsigned char>(dim[0], dim[1], dim[2]);
                Image<unsigned char>* img = dynamic_cast<Image<unsigned char>* >(image);
            #endif
                unsigned char* data = (unsigned char*)nim->data;
                for (unsigned int i = 0 ; i < dim[0]; i++)
                {
                    for (unsigned int j = 0 ; j < dim[1]; j++)
                    {
                        for (unsigned int k = 0 ; k < dim[2]; k++)
                        {
                            img->setVoxel(i,j,k, data[k * dim[0] * dim[1] + j * dim[0] + i]);
                        }
                    }
                }
                break;
            }

            case 4:
            {
                #ifdef IMAGE_FILE_READER_USE_SMART_POINTER
                    image = std::shared_ptr<Image<short> >(new Image<short>(dim[0], dim[1], dim[2]));
//                    std::shared_ptr<Image<short> > img = std::dynamic_pointer_cast<Image<short> >(image);
                #else
                    image = new Image<short>(dim[0], dim[1], dim[2]);
//                    Image<short>* img = dynamic_cast<Image<short>* >(image);
                #endif
                break;
            }
            case 8:
            {
                #ifdef IMAGE_FILE_READER_USE_SMART_POINTER
                    image = std::shared_ptr<Image<int> >(new Image<int>(dim[0], dim[1], dim[2]));
                //  std::shared_ptr<Image<int> > img = std::dynamic_pointer_cast<Image<int> >(image);
                #else
                    image = new Image<int>(dim[0], dim[1], dim[2]);
                //  Image<int>* img = dynamic_cast<Image<int>* >(image);
                #endif
                break;
            }
            case 16:
            {
                #ifdef IMAGE_FILE_READER_USE_SMART_POINTER
                    image = std::shared_ptr<Image<float> >(new Image<float>(dim[0], dim[1], dim[2]));
                    std::shared_ptr<Image<float> > img = std::dynamic_pointer_cast<Image<float> >(image);
                #else
                    image = new Image<float>(dim[0], dim[1], dim[2]);
                    Image<float>* img = dynamic_cast<Image<float>* >(image);
                #endif
                float* data = (float*)nim->data;
                for (unsigned int i = 0 ; i < dim[0]; i++)
                {
                    for (unsigned int j = 0 ; j < dim[1]; j++)
                    {
                        for (unsigned int k = 0 ; k < dim[2]; k++)
                        {
                            img->setVoxel(i,j,k, data[k * dim[0] * dim[1] + j * dim[0] + i]);
                        }
                    }
                }
                break;
            }
            case 64:
            {
                #ifdef IMAGE_FILE_READER_USE_SMART_POINTER
                image = std::shared_ptr<Image<double> >(new Image<double>(dim[0], dim[1], dim[2]));
//                std::shared_ptr<Image<double> > img = std::dynamic_pointer_cast<Image<double> >(image);
                #else
                image = new Image<double>(dim[0], dim[1], dim[2]);
//              Image<double>* img = dynamic_cast<Image<double>* >(image);
                #endif
            }
            default:
            {
                std::cout << "Data type non-supported" << std::endl;
                image = NULL;
                break;
            }
        }

        if (image)
        {
            int vsize[3];
            float qfac = nim->qfac;
            if (qfac != 1 && qfac != -1) qfac = 1;

//            qfac = 1;
            vsize[0]=nim->dx;
            vsize[1]=nim->dy;
            vsize[2]=nim->dz;

            std::ostringstream converter2;
            converter2 << vsize[0] << "x" << vsize[1] << "x" << vsize[2];
            std::cout << std::left << std::setw(22) << std::setfill(' ') << "Voxel size:";
            std::cout << std::left << std::setw(22) << std::setfill(' ') << converter2.str();
            std::cout << std::endl;

            image->setVoxelSizeX(vsize[0]);
            image->setVoxelSizeY(vsize[1]);
            image->setVoxelSizeZ(vsize[2]);

            if (nim->qform_code > 0) //method 2
            {
                double b = nim->quatern_b;
                double c = nim->quatern_c;
                double d = nim->quatern_d;
                double a = sqrt(1.0 - b*b - c*c - d*d);

                image->getTransform().getRotation() = Quaternion<double>(a,b,c,d);

                image->getTransform().getTranslation()[0] = nim->qoffset_x;
                image->getTransform().getTranslation()[1] = nim->qoffset_y;
                image->getTransform().getTranslation()[2] = nim->qoffset_z;

                std::ostringstream converter3;
                converter3 << nim->qoffset_x << ", " << nim->qoffset_y << ", " << nim->qoffset_z;
                std::cout << std::left << std::setw(22) << std::setfill(' ') << "Origin:";
                std::cout << std::left << std::setw(22) << std::setfill(' ') << converter3.str();
                std::cout << std::endl;

//                image->getTransform().getRotScale()[0] = 1;
//                image->getTransform().getRotScale()[1] = 1;
                image->getTransform().getRotScale()[2] = qfac;
            }
            else
            {
                std::cerr << "Other method than 2 not implemented" << std::endl;
            }
        }

    }



};

#endif // NIFTIREADER_H
