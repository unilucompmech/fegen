#include "baseimage.h"

#include <cmath>
#include <limits>

BaseImage::BaseImage()
{
}

BaseImage::BaseImage(int dimX, int dimY, int dimZ)
{
    this->setDimX(dimX);
    this->setDimY(dimY);
    this->setDimZ(dimZ);
}

BaseImage::~BaseImage()
{
}

ImageType BaseImage::getDatatype()
{
    return datatype;
}

Transform &BaseImage::getTransform()
{
    return transform;
}

void BaseImage::setTransform(const Transform &value)
{
    transform = value;
}

template <class T>
Image<T>::Image() : BaseImage()
{
    data = NULL;
    datatype = ImageType::NONE;
}

template <>
Image<unsigned char>::Image() : BaseImage()
{
    data = NULL;
    datatype = ImageType::UNSIGNED_CHAR;
}
template <>
Image<short>::Image() : BaseImage()
{
    data = NULL;
    datatype = ImageType::SHORT;
}
template <>
Image<int>::Image() : BaseImage()
{
    data = NULL;
    datatype = ImageType::INT;
}
template <>
Image<float>::Image() : BaseImage()
{
    data = NULL;
    datatype = ImageType::FLOAT;
}
template <>
Image<double>::Image() : BaseImage()
{
    data = NULL;
    datatype = ImageType::DOUBLE;
}

template <class T>
Image<T>::Image(int dimX, int dimY, int dimZ) : BaseImage(dimX,dimY,dimZ)
{
    datatype = ImageType::NONE;
    createData(dimX, dimY, dimZ);
}

template <>
Image<unsigned char>::Image(int dimX, int dimY, int dimZ) : BaseImage(dimX,dimY,dimZ)
{
    datatype = ImageType::UNSIGNED_CHAR;
    createData(dimX, dimY, dimZ);
}

template <>
Image<short>::Image(int dimX, int dimY, int dimZ) : BaseImage(dimX,dimY,dimZ)
{
    datatype = ImageType::SHORT;
    createData(dimX, dimY, dimZ);
}

template <>
Image<int>::Image(int dimX, int dimY, int dimZ) : BaseImage(dimX,dimY,dimZ)
{
    datatype = ImageType::INT;
    createData(dimX, dimY, dimZ);
}

template <>
Image<float>::Image(int dimX, int dimY, int dimZ) : BaseImage(dimX,dimY,dimZ)
{
    datatype = ImageType::FLOAT;
    createData(dimX, dimY, dimZ);
}

template <>
Image<double>::Image(int dimX, int dimY, int dimZ) : BaseImage(dimX,dimY,dimZ)
{
    datatype = ImageType::DOUBLE;
    createData(dimX, dimY, dimZ);
}

template <class T>
T Image<T>::getVoxel(int x, int y, int z)
{
    int index = z*this->getDimX()*this->getDimY() + y*this->getDimX() + x;
    if (index >= this->voxelCount())
    {
        std::cerr << "Trying to access voxel (" << x << ", " << y << ", " << z << ")/(index = " << index << ") whereas data size is " << this->voxelCount() << std::endl;
        return T();
    }

#ifdef BASE_IMAGE_USE_SMART_POINTER
    return *(data.get() + index);
#else
    return data[index];
#endif
}

template <class T>
void Image<T>::setVoxel(int x, int y, int z, T value)
{
    int index = z*this->getDimX()*this->getDimY() + y*this->getDimX() + x;
    if (index >= this->voxelCount())
    {
        std::cerr << "Trying to access voxel (" << x << ", " << y << ", " << z << ")/(index = " << index << ") whereas data size is " << this->voxelCount() << std::endl;
    }

//    data[index] = value;
#ifdef BASE_IMAGE_USE_SMART_POINTER
    *(data.get() + index) = value;
#else
    data[index] = value;
#endif

    if (value > max) max = value;
    if (value < min) min = value;
}


template <class T>
void Image<T>::createData(int dimX, int dimY, int dimZ)
{
#ifdef BASE_IMAGE_USE_SMART_POINTER
    data = std::shared_ptr<T>(new T[this->voxelCount()], std::default_delete<T[]>());
#else
    data = new T[this->voxelCount()];
#endif

    float s = sizeof(T)*this->voxelCount() / 8;
    if (s < 1024)
        std::cout << "Image size in memory: " << s << " bytes" << std::endl;
    else if (s < 1024*1024)
        std::cout << "Image size in memory: " << s/1024 << " ko" << std::endl;
    else if (s < 1024*1024*1024)
        std::cout << "Image size in memory: " << s/(1024*1024) << " Mo" << std::endl;
    else
        std::cout << "Image size in memory: " << s/(1024*1024*1024) << " Go" << std::endl;

    min = std::numeric_limits<T>::max();
    max = std::numeric_limits<T>::min();
}

template <class T>
T Image<T>::getMax() const
{
    return max;
}

template <class T>
T Image<T>::getMin() const
{
    return min;
}



template <class T>
ImageType Image<T>::getDatatype()
{
    return datatype;
}

template <class T>
T Image<T>::getIntensityAtPoint(double x, double y, double z)
{
//    std::cout << "getIntensityAtPoint(" << x << ", " << y << ", " << z << ")" << std::endl;

    if (x < 0) x = 0;
    if (y < 0) y = 0;
    if (z < 0) z = 0;

    if (x > this->getDimX()-1 ) x = this->getDimX()-1 - 1e-10;
    if (y > this->getDimY()-1 ) y = this->getDimY()-1 - 1e-10;
    if (z > this->getDimZ()-1 ) z = this->getDimZ()-1 - 1e-10;

//    std::cout << "new getIntensityAtPoint(" << x << ", " << y << ", " << z << ")" << std::endl;

    const int x_floor = floor(x);
    const int y_floor = floor(y);
    const int z_floor = floor(z);

    const int x_up = x_floor+1;
    const int y_up = y_floor+1;
    const int z_up = z_floor+1;

    const double x_d = (x - (double)x_floor);
    const double y_d = (y - (double)y_floor);
    const double z_d = (z - (double)z_floor);

//    std::cout << "t(" << x_d << ", " << y_d << ", " << z_d << ")" << std::endl;

    const T c_00 = this->getVoxel(x_floor, y_floor, z_floor) * (1.0 - x_d)
                 + this->getVoxel(x_up   , y_floor, z_floor) * x_d;
    const T c_10 = this->getVoxel(x_floor, y_up   , z_floor) * (1.0 - x_d)
                 + this->getVoxel(x_up   , y_up   , z_floor) * x_d;
    const T c_01 = this->getVoxel(x_floor, y_floor, z_up   ) * (1.0 - x_d)
                 + this->getVoxel(x_up   , y_floor, z_up   ) * x_d;
    const T c_11 = this->getVoxel(x_floor, y_up   , z_up   ) * (1.0 - x_d)
                 + this->getVoxel(x_up   , y_up   , z_up   ) * x_d;

    const T c_0  = c_00 * (1.0 - y_d) + c_10 * y_d;
    const T c_1  = c_01 * (1.0 - y_d) + c_11 * y_d;

    const T c    = c_0 * (1.0 - z_d) + c_1 * z_d;

//    std::cout << "intensity " << static_cast<unsigned>(c) << std::endl;

    return c;
}



//template <>
//unsigned char Image<unsigned char>::getIntensityAtPoint(double x, double y, double z)
//{
////    std::cout << "getIntensityAtPoint(" << x << ", " << y << ", " << z << ")" << std::endl;

//    if (x < 0) x = 0;
//    if (y < 0) y = 0;
//    if (z < 0) z = 0;

//    if (x > this->getDimX()-1 ) x = this->getDimX()-1.1;
//    if (y > this->getDimY()-1 ) y = this->getDimY()-1.1;
//    if (z > this->getDimZ()-1 ) z = this->getDimZ()-1.1;

////    std::cout << "new getIntensityAtPoint(" << x << ", " << y << ", " << z << ")" << std::endl;

//    const int x_floor = floor(x);
//    const int y_floor = floor(y);
//    const int z_floor = floor(z);

////    std::cout << "getVoxel(" << x_floor << ", " << y_floor << ", " << z_floor << ")" << std::endl;

//    const unsigned char c = this->getVoxel(x_floor, y_floor, z_floor);

////    std::cout << "intensity " << static_cast<unsigned>(c) << std::endl;
////    std::cout << "intensity " << c << std::endl;

//    return c;
//}






template class Image<unsigned char>;
template class Image<short>;
template class Image<int>;
template class Image<float>;
template class Image<double>;



