#ifndef IMAGEFILEREADER_H
#define IMAGEFILEREADER_H

#include <iostream>

#include "filereader.h"
#include "baseimage.h"

#define IMAGE_FILE_READER_USE_SMART_POINTER

class SimpleImageFileReader : public FileReader
{
public:
    SimpleImageFileReader();
    SimpleImageFileReader(std::string filename);

    FileType getFileType() const{return FileType::IMAGE;}
};

//! Read a 3D image file
class ImageFileReader : public SimpleImageFileReader
{
public:
    ImageFileReader();
    ImageFileReader(std::string filename);

#ifdef IMAGE_FILE_READER_USE_SMART_POINTER
    std::shared_ptr<BaseImage>
#else
    BaseImage*
#endif
    getImage(){return image;}

protected:
#ifdef IMAGE_FILE_READER_USE_SMART_POINTER
    std::shared_ptr<BaseImage> image;
#else
    BaseImage* image;
#endif
};

#endif // IMAGEFILEREADER_H
