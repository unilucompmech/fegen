#ifndef VECTOR_H
#define VECTOR_H

#include <cstddef>
#include <ostream>
#include <array>

//! Vector data structure with a fixed size
template <class T, std::size_t N>
class FixedVector
{
public:

    static const std::size_t size = N;

    FixedVector()
    {
        for (unsigned int i = 0 ; i < N; i++)
            this->data[i] = (T)0;
    }

    FixedVector(const FixedVector<T,N>& v)
    {
        for (int i = 0 ; i < N; i++)
            data[i] = v[i];
    }

    void sum(const FixedVector<T,N>& v)
    {
        for (int i = 0 ; i < N; i++)
            data[i] += v[i];
    }

    //Hadamard product
    void product(const FixedVector<T,N>& v)
    {
//        return this->
    }

    T& operator[](int i){return data[i];}
    const T& operator[](int i) const {return data[i];}

    void operator=(const FixedVector<T,N> b)
    {
        for (int i = 0 ; i < N; i++)
            data[i] = b[i];
    }

    void operator*=(const double& b)
    {
        for (int i = 0 ; i < N; i++)
            data[i] *= b;
    }

//    FixedVector<T,N>& operator*(FixedVector<T,N>& v) {return this->product(v);}
//    FixedVector<T,N>& operator+(FixedVector<T,N>& v) {return this->product(v);}


    friend std::ostream& operator<<(std::ostream &os, const FixedVector<T,N>& p)
    {
        os << "(";
        for (unsigned int i = 0 ; i < N; i++)
        {
            os << p.data[i];
            if (i < N-1) os << ", ";
        }
        os << ")";
        return os;
    }

protected:
    std::array<T,N> data;
};


//operators
template <typename T, std::size_t N, typename BinaryOp>
FixedVector<T, N> base_op(const FixedVector<T, N>& a, const FixedVector<T, N>& b, BinaryOp f)
{
    FixedVector<T, N> result;
    for (unsigned int i = 0 ; i < N; i++)
    {
        result[i] = f(a[i], b[i]);
    }
    return result;
}

template <typename T, std::size_t N>
FixedVector<T, N> operator+(const FixedVector<T, N>& a, const FixedVector<T, N>& b)
{
    return base_op(a, b, std::plus<T>());
}

template <typename T, std::size_t N>
FixedVector<T, N> operator-(const FixedVector<T, N>& a, const FixedVector<T, N>& b)
{
    return base_op(a, b, std::minus<T>());
}

template <typename T, std::size_t N>
FixedVector<T, N> operator*(const FixedVector<T, N>& a, const FixedVector<T, N>& b)
{
    return base_op(a, b, std::multiplies<T>());
}

template <typename T, std::size_t N>
FixedVector<T, N> operator*(const FixedVector<T, N>& a, const double& b)
{
    FixedVector<T, N> result;
    for (unsigned int i = 0 ; i < N; i++)
    {
        result[i] *= b;
    }
    return result;
}


template <class T>
class Vector3d : public FixedVector<T, 3>
{
public:
    Vector3d() : FixedVector<T, 3>(){}
    Vector3d(T x, T y, T z)
    {
        this->data[0] = x;
        this->data[1] = y;
        this->data[2] = z;
    }
    Vector3d(const Vector3d<T>& v) : FixedVector<T, 3>(v) {}
};

template <class T>
class Vector2d : public FixedVector<T, 2>
{
public:
    Vector2d() : FixedVector<T, 2>(){}
    Vector2d(T x, T y)
    {
        this->data[0] = x;
        this->data[1] = y;
    }
    Vector2d(const Vector2d<T>& v) : FixedVector<T, 2>(v) {}
};



#endif // VECTOR_H
