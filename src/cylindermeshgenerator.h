#ifndef CYLINDERMESHGENERATOR_H
#define CYLINDERMESHGENERATOR_H

#include "meshgenerator.h"
#include "basemesh.h"
#include "transform.h"

#include <iostream>
#include <time.h>

class CylinderMeshGenerator : public MeshGenerator
{
public:
    CylinderMeshGenerator();

    void computeMesh(MeshGenerationParameters* parameters) override;
    void init() override;
    void updateLabels(MeshGenerationParameters *parameters) override;

    PolygonMesh<Tetrahedron3D> getOutputMesh() const;

private:
    PolygonMesh<Tetrahedron3D> outputMesh;

    std::vector<unsigned int> insideIndices;
    std::vector<unsigned int> outsideIndices;
    std::vector<unsigned int> topIndices;
    std::vector<unsigned int> bottomIndices;
};


class CylinderMeshGeneratorParameters : public MeshGenerationParameters
{
public:
    double length;
    double innerRadius;
    double outerRadius;
    unsigned int lengthSubdiv;
    unsigned int radiusSubdiv;
    unsigned int angleSubdiv;
    bool automaticLabelingVertices;
    bool automaticLabelingFacets;
    bool automaticLabelingCells;

    CylinderMeshGeneratorParameters(double length, double innerRadius, double outerRadius, unsigned int lengthSubdiv, unsigned int radiusSubdiv, unsigned int angleSubdiv, bool autoLVert, bool autoLFac, bool autoLCel) :
        length(length)
      , innerRadius(innerRadius)
      , outerRadius(outerRadius)
      , lengthSubdiv(lengthSubdiv)
      , radiusSubdiv(radiusSubdiv)
      , angleSubdiv(angleSubdiv)
      , automaticLabelingVertices(autoLVert)
      , automaticLabelingFacets(autoLFac)
      , automaticLabelingCells(autoLCel)
    {}

    CylinderMeshGeneratorParameters() :
        length(1.0)
      , innerRadius(0.9)
      , outerRadius(1.0)
      , lengthSubdiv(10)
      , radiusSubdiv(3)
      , angleSubdiv(8)
      , automaticLabelingVertices(false)
      , automaticLabelingFacets(false)
      , automaticLabelingCells(false)
    {}
};

#endif // CYLINDERMESHGENERATOR_H
