#include "cylindergenerationwidget.h"

CylinderGenerationWidget::CylinderGenerationWidget(QWidget *parent) :
    QWidget(parent)
{
    formLayout = new QFormLayout;
    this->setLayout(formLayout);

    lengthLine = new QLineEdit;
    lengthLine->setToolTip("Length of the cylinder");
    formLayout->addRow(tr("&Length"), lengthLine);

    innerRadiusLine = new QLineEdit;
    innerRadiusLine->setToolTip("Inner radius of the cylinder");
    formLayout->addRow(tr("&Inner radius"), innerRadiusLine);

    outerRadiusLine = new QLineEdit;
    outerRadiusLine->setToolTip("Outer radius of the cylinder");
    formLayout->addRow(tr("&Outer radius"), outerRadiusLine);

    lengthSubdivLine = new QLineEdit;
    lengthSubdivLine->setToolTip("Subdivision along the length of the cylinder");
    formLayout->addRow(tr("&Length subdivision"), lengthSubdivLine);

    radiusSubdivLine = new QLineEdit;
    radiusSubdivLine->setToolTip("Subdivision along the radius of the cylinder");
    formLayout->addRow(tr("&Radius subdivision"), radiusSubdivLine);

    angleSubdivLine = new QLineEdit;
    angleSubdivLine->setToolTip("Subdivision along the angle of the cylinder");
    formLayout->addRow(tr("&Angle subdivision"), angleSubdivLine);

    automaticLabelingVertex = new QCheckBox;
    formLayout->addRow(tr("&Automatic labeling (vertices)"), automaticLabelingVertex);

    automaticLabelingFacet = new QCheckBox;
    formLayout->addRow(tr("&Automatic labeling (facets)"), automaticLabelingFacet);

    automaticLabelingCell = new QCheckBox;
    formLayout->addRow(tr("&Automatic labeling (cells)"), automaticLabelingCell);


    lengthLine->setText(QString::number(1.0));
    innerRadiusLine->setText(QString::number(0.9));
    outerRadiusLine->setText(QString::number(1.0));
    lengthSubdivLine->setText(QString::number(10));
    radiusSubdivLine->setText(QString::number(3));
    angleSubdivLine->setText(QString::number(8));

    automaticLabelingVertex->setChecked(false);
    automaticLabelingFacet->setChecked(false);
    automaticLabelingCell->setChecked(false);
}

double CylinderGenerationWidget::getLength()
{
    assert(lengthLine != nullptr);
    return lengthLine->text().toDouble();
}

double CylinderGenerationWidget::getInnerRadius()
{
    assert(innerRadiusLine != nullptr);
    return innerRadiusLine->text().toDouble();
}

double CylinderGenerationWidget::getOuterRadius()
{
    assert(outerRadiusLine != nullptr);
    return outerRadiusLine->text().toDouble();
}

unsigned int CylinderGenerationWidget::getLengthSubdiv()
{
    assert(lengthSubdivLine != nullptr);
    return lengthSubdivLine->text().toInt();
}

unsigned int CylinderGenerationWidget::getRadiusSubdiv()
{
    assert(radiusSubdivLine != nullptr);
    return radiusSubdivLine->text().toInt();
}

unsigned int CylinderGenerationWidget::getAngleSubdiv()
{
    assert(angleSubdivLine != nullptr);
    return angleSubdivLine->text().toInt();
}

bool CylinderGenerationWidget::getAutomaticLabelingVertex()
{
    assert(automaticLabelingVertex != nullptr);
    return automaticLabelingVertex->checkState()==Qt::Checked;
}

bool CylinderGenerationWidget::getAutomaticLabelingFacet()
{
    assert(automaticLabelingFacet != nullptr);
    return automaticLabelingFacet->checkState()==Qt::Checked;
}

bool CylinderGenerationWidget::getAutomaticLabelingCell()
{
    assert(automaticLabelingCell != nullptr);
    return automaticLabelingCell->checkState()==Qt::Checked;
}

