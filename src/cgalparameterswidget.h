#ifndef CGALPARAMETERSWIDGET_H
#define CGALPARAMETERSWIDGET_H

#include <assert.h>

#include <QWidget>
#include <QFormLayout>
#include <QLineEdit>
#include <QCheckBox>

//!  Qt widget to display and input the parameters for the CGAL mesh generator
class CGALParametersWidget : public QWidget
{
    Q_OBJECT
public:
    explicit CGALParametersWidget(QWidget *parent = 0);

    double       getCellSize();
    double       getCellRadiusEdgeRatio();
    double       getFacetSize();
    double       getFacetAngle();
    double       getFacetDistance();
    bool         getLloydSmoothing();
    unsigned int getLloydMaxIterations();
    bool         getODTSmoothing();
    unsigned int getODTMaxIerations();
    bool         getPerturb();
    double       getPerturbTimout();
    bool         getExude();
    double       getExudeTimout();


signals:

public slots:


protected:

    QLineEdit* cellSizeLine;
    QLineEdit* cellRadiusEdgeRatioLine;
    QLineEdit* facetSizeLine;
    QLineEdit* facetAngleLine;
    QLineEdit* facetDistanceLine;

    QCheckBox* lloydSmoothing;
    QLineEdit* lloydMaxIt;

    QCheckBox* odtSmoothing;
    QLineEdit* odtMaxIt;

    QCheckBox* perturb;
    QLineEdit* perturbTimout;

    QCheckBox* exude;
    QLineEdit* exudeTimout;

    QFormLayout* formLayout;

};

#endif // CGALPARAMETERSWIDGET_H
