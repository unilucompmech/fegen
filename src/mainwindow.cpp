#include "mainwindow.h"

#include <iomanip>

#include <QFileDialog>
#include <QSettings>
#include <QCloseEvent>
#include <QMessageBox>
#include <QProgressDialog>
#include <QProgressBar>
#include <QSlider>

//Mesh generation
#include "meshgeneratorfrompolyhedra.h"
#include "cgalmeshgenerator.h"
#include "meshgeneratorfromimage.h"
#include "cylindermeshgenerator.h"
#include "copymeshgenerator.h"

//Export
#include "fileexport.h"
#include "vtuexport.h"
#include "dolfinexporter.h"
#include "objexporter.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    ui.setupUi(this);

    meshParametersWidget = nullptr;
    outputInfoWidget = nullptr;

    inputWidget = nullptr;
    outputWidget= nullptr;

    imageWidget = nullptr;
    imageLayout = nullptr;

    imageXLabel = nullptr;
    imageYLabel = nullptr;
    imageZLabel = nullptr;

    imageCaptionX = nullptr;
    imageCaptionY = nullptr;
    imageCaptionZ = nullptr;

    sliderX = nullptr;
    sliderY = nullptr;
    sliderZ = nullptr;

    addSubdomainButton = nullptr;
    labelList = nullptr;

    meshGenerator = nullptr;
    mesh = nullptr;
    reader = nullptr;

    slice      = nullptr;
//    meshViewer = nullptr;

    meshComputed= false;
    inputLoaded = false;


    ui.methodComboBox->addItem("CGAL - Delauney");
    ui.methodComboBox->addItem("Simple conversion");
    ui.methodComboBox->addItem("Cylinder");
    connect(ui.methodComboBox, SIGNAL(currentIndexChanged(QString)), this, SLOT(setMeshingMethod(QString)));
//    setMeshingMethod("CGAL - Delauney");

    readWindowSettings();

    QString inputFile;
    QString outputFile;
    for (unsigned int i = 0; i < qApp->arguments().count(); ++i)
    {
        if ( qApp->arguments().at(i) == QString("--input") || qApp->arguments().at(i) == QString("-i") )
        {
            if (i+1 < qApp->arguments().count())
                inputFile = qApp->arguments().at(i+1);
            else
                std::cerr << "--input option requires one argument" << std::endl;
        }
        if ( qApp->arguments().at(i) == QString("--output") || qApp->arguments().at(i) == QString("-o") )
        {
            if (i+1 < qApp->arguments().count())
                outputFile = qApp->arguments().at(i+1);
            else
                std::cerr << "--output option requires one argument" << std::endl;
        }
        else if ( qApp->arguments().at(i) == QString("--help") || qApp->arguments().at(i) == QString("-h") )
        {
            std::cout << "Usage: " << qApp->applicationName().toStdString().c_str() << " [OPTION]" << std::endl;

            int reducedWidth = 5;
            int expandedWidth = 10;
            int descriptionWidth = 30;
            const char separator = ' ';

            std::cout << std::left << std::setw(reducedWidth) << std::setfill(separator) << "-i";
            std::cout << std::left << std::setw(expandedWidth) << std::setfill(separator) << "--input";
            std::cout << std::left << std::setw(descriptionWidth) << std::setfill(separator) << "Input file";
            std::cout << std::endl;

            std::cout << std::left << std::setw(reducedWidth) << std::setfill(separator) << "-o";
            std::cout << std::left << std::setw(expandedWidth) << std::setfill(separator) << "--output";
            std::cout << std::left << std::setw(descriptionWidth) << std::setfill(separator) << "Output file";
            std::cout << std::endl;
        }
    }

    if (!inputFile.isNull() && !inputFile.isEmpty())
        openFilename(inputFile);

    if (!outputFile.isNull() && !outputFile.isEmpty())
        setOutputFile(outputFile);

    qDebug() << "Main window created";

}

MainWindow::~MainWindow()
{
    delete meshGenerator;
    delete mesh;
}


void MainWindow::openFilename(QString inputFilename)
{
    if (inputFilename.isEmpty())
        qDebug() << "File not provided";
    else
    {
        qDebug() << "Reading file " << inputFilename << "...";

        QFileInfo infoInputFilename(inputFilename);

        if (!infoInputFilename.isFile())
        {
            std::cerr << inputFilename.toStdString().c_str() << " is not a file" << std::endl;
            return;
        }

        //Qt will remember this directory next time the user load a file
        QDir::setCurrent(infoInputFilename.absoluteDir().absolutePath());

        QString extension = infoInputFilename.completeSuffix();

        if (extension == QString("obj"))
        {
            qDebug() << inputFilename << " is considered as a Wavefront .obj file";
            this->reader = new ObjReader(inputFilename.toStdString());
        }
        else if (extension == QString("vtk"))
        {
            qDebug() << inputFilename << " is considered as a VTK file";
            this->reader = new VTKReader(inputFilename.toStdString());
        }
        else if (extension == QString("nii") || extension == QString("nii.gz"))
        {
            qDebug() << inputFilename << " is considered as a Nifti file";
            this->reader = new NiftiReader(inputFilename.toStdString());
        }
        else if (extension == QString("inr.gz"))
        {
            qDebug() << inputFilename << " is considered as a INR file";
            this->reader = new SimpleImageFileReader(inputFilename.toStdString());
        }
        else
        {
            qDebug() << "Unknown type of file";
            this->reader = NULL;
        }

        //if the extension is known, reader is instantiated
        if (this->reader)
        {
            if (reader->getFileType() == FileType::MESH)
            {
                MeshFileReader* m_reader = dynamic_cast<MeshFileReader*>(this->reader);

                if (!m_reader)
                {
                    std::cerr << "Wrong type of file reader. Should be of type MESH" << std::endl;
                    return;
                }

                //instantiation of the mesh data structure depending on the type of mesh
                MeshType meshType = m_reader->getMeshType();
                if (meshType == MeshType::TRIANGLES || meshType == MeshType::QUADS || meshType == MeshType::QUADS_AND_TRIANGLES)
                {
                    //the input file can be made of triangle and/or quads (quads are converted to triangles)
                    mesh = new PolygonMesh<Triangle3D>();
                }
                if (meshType == MeshType::TETRAHEDRONS)
                {
                    mesh = new PolygonMesh<Tetrahedron3D>();
                }
                else
                    std::cerr << "Cannot handle other types of mesh than: triangles, quads" << std::endl;

                if (mesh)
                {
                    mesh->readFromFile(this->reader);
                    updateUIFromInputFile();
                    setMeshingMethod(ui.methodComboBox->currentText());
                }
            }
            else if (this->reader->getFileType() == FileType::IMAGE)
            {
                int uiindex = ui.methodComboBox->findText("Simple conversion");
                if (uiindex != -1)
                {
                    int currentIndex = ui.methodComboBox->currentIndex();
                    if (uiindex == currentIndex)
                    {
                        if (currentIndex != 0)
                            ui.methodComboBox->setCurrentIndex(0);
                        else if (ui.methodComboBox->count() > 1)
                            ui.methodComboBox->setCurrentIndex(1);
                    }
                    ui.methodComboBox->removeItem(uiindex);
                }
                updateUIFromInputFile();

                meshParameters = std::make_shared<CGALParameters>();

                meshGenerator = new MeshGeneratorFromImage(this->reader);
                meshGenerator->init();

                inputLoaded = true;
                meshComputed = false;
            }
            else
            {
                std::cout << "Type of file unknown" << std::endl;
            }
        }
    }
}

void MainWindow::openInputFile()
{
    qDebug() << "Open Input File";

    const QStringList docLocations = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation);
    QString startFolder = QDir::homePath();
    if (QDir::currentPath().isEmpty() || !(QDir(QDir::currentPath()).exists())) //never empty
    {
        if (!docLocations.empty())
        {
            startFolder = docLocations.first();
        }
    }
    else
    {
        startFolder = QDir::currentPath();
    }

    QString inputFilename = QFileDialog::getOpenFileName(this, tr("Open input file"), startFolder, tr("Wavefront (*.obj);;Nifti (*.nii *.nii.gz);; INR image (*.inr.gz);; VTK (*.vtk *vtu)"));

    openFilename(inputFilename);
}

void MainWindow::setOutputFile()
{
    qDebug() << "Set output filename";

    QString startFolder = QDir::homePath();

    QString outputFilename = QFileDialog::getSaveFileName(this, tr("Save output file"), startFolder, tr("VTU file (*.vtu);;mesh file (*.mesh);;Object File Format (*.off);;Dolfin File Format (*.xml)"));

    setOutputFile(outputFilename);
}

void MainWindow::setOutputFile(QString outputFilename)
{
    if (outputFilename.isEmpty())
        qDebug() << "Output file not provided";
    else
    {
        qDebug() << "Output file is set to " << outputFilename;

        this->outFilename = outputFilename.toStdString();
        ui.outputLineEdit->setText(outputFilename);
    }
}

void MainWindow::computeMesh()
{
    qDebug() << "Compute Mesh";

    if (meshGenerator)
    {
        if (this->getMeshingMethod() == MeshingMethod::CGAL)
        {
            if (!inputLoaded)
            {
                QMessageBox msgBox;
                msgBox.setText("No input file");
                msgBox.setIcon(QMessageBox::Warning);
                msgBox.exec();
                qDebug() << "No input file";
                return;
            }

            if (CGALParametersWidget* cgalLayout = dynamic_cast<CGALParametersWidget*>(meshParametersWidget))
            {
                std::cout << "Computing mesh with method CGAL" << std::endl;
                if (!meshParameters)
                {
                    meshParameters = std::make_shared<CGALParameters>(
                                cgalLayout->getFacetSize()
                                ,cgalLayout->getFacetAngle()
                                ,cgalLayout->getFacetDistance()
                                ,cgalLayout->getCellRadiusEdgeRatio()
                                ,cgalLayout->getCellSize()
                                ,cgalLayout->getLloydSmoothing()
                                ,cgalLayout->getLloydMaxIterations()
                                ,cgalLayout->getODTSmoothing()
                                ,cgalLayout->getODTMaxIerations()
                                ,cgalLayout->getPerturb()
                                ,cgalLayout->getPerturbTimout()
                                ,cgalLayout->getExude()
                                ,cgalLayout->getExudeTimout()
                                );
                    meshParameters->outputScale = 1.0;
                }
                else
                {
                    if (std::shared_ptr<CGALParameters> cgalParam = std::dynamic_pointer_cast<CGALParameters>(meshParameters))
                    {
                        cgalParam->cell_size              = cgalLayout->getCellSize();
                        cgalParam->cell_radius_edge_ratio = cgalLayout->getCellRadiusEdgeRatio();
                        cgalParam->facet_approximation    = cgalLayout->getFacetDistance();
                        cgalParam->facet_angle            = cgalLayout->getFacetAngle();
                        cgalParam->facet_size             = cgalLayout->getFacetSize();
                        cgalParam->lloyd_smoothing        = cgalLayout->getLloydSmoothing();
                        cgalParam->lloyd_max_iteration    = cgalLayout->getLloydMaxIterations();
                        cgalParam->odt_smoothing          = cgalLayout->getODTSmoothing();
                        cgalParam->odt_max_iteration      = cgalLayout->getODTMaxIerations();
                        cgalParam->perturb                = cgalLayout->getPerturb();
                        cgalParam->perturb_max_time       = cgalLayout->getPerturbTimout();
                        cgalParam->exude                  = cgalLayout->getExude();
                        cgalParam->exude_max_time         = cgalLayout->getExudeTimout();

                        cgalParam->outputScale = ui.scaleOutputLineEdit->text().toDouble();
                    }
                    else
                        std::cerr << "Mesh parameters has not the right type (should not happen)" << std::endl;
                }
            }
            else
                std::cerr << "Mesh parameters layout could not be cast to CGALParametersLayout* (should not happen)" << std::endl;
        }
        else if (this->getMeshingMethod() == MeshingMethod::CONVERSION)
        {
            if (!inputLoaded)
            {
                QMessageBox msgBox;
                msgBox.setText("No input file");
                msgBox.setIcon(QMessageBox::Warning);
                msgBox.exec();
                qDebug() << "No input file";
                return;
            }

            meshParameters = std::make_shared<CopyMeshGeneratorParameters>();
        }
        else if (this->getMeshingMethod() == MeshingMethod::CYLINDER)
        {
            std::cout << "Cylinder generation" << std::endl;

            if (CylinderGenerationWidget* cylLayout = dynamic_cast<CylinderGenerationWidget*>(meshParametersWidget))
            {
                meshParameters = std::make_shared<CylinderMeshGeneratorParameters>(cylLayout->getLength(),
                                                                                   cylLayout->getInnerRadius(),
                                                                                   cylLayout->getOuterRadius(),
                                                                                   cylLayout->getLengthSubdiv(),
                                                                                   cylLayout->getRadiusSubdiv(),
                                                                                   cylLayout->getAngleSubdiv(),
                                                                                   cylLayout->getAutomaticLabelingVertex(),
                                                                                   cylLayout->getAutomaticLabelingFacet(),
                                                                                   cylLayout->getAutomaticLabelingCell()
                                                                                   );
            }
        }
        else
        {
            std::cout << "The selected method is not valid" << std::endl;
            meshParameters = nullptr;
        }

//        QProgressDialog progress("Compute mesh...", "Abort computing", 0, INT_MAX);
//        progress.setWindowModality(Qt::WindowModal);
////        progress.setCancelButton(0);
//        progress.setMinimumWidth(350);
//        progress.setMinimumDuration(500);
//        progress.setValue(50);
//        progress.setRange(0,0);

//        progress.show();
//        qApp->processEvents();

        QMessageBox* box = new QMessageBox(this);
        box->setText("Mesh computing...");
        box->setModal(true);
        box->show();
        qApp->processEvents();

        if (meshParameters)
        {
            meshGenerator->computeMesh(meshParameters.get());

//            if (CGALMeshGenerator* cgal = dynamic_cast<CGALMeshGenerator*>(meshGenerator))
//            {
//                PolygonMesh<Tetrahedron3D> outputMesh = cgal->getTetrahedronMesh();
//                for (const auto& p : outputMesh.getPositions())
//                {
//                    std::cout << "Point " << p << std::endl;
//                }
//            }
            //give the labels to the mesh generator and update the output mesh
            meshGenerator->updateLabels(meshParameters.get());

            //get the labels from GUI
            this->setLabelDomainsFromUI();

            //mesh has been computed successfully
            meshComputed = true;
        }

//        progress.close();
//        progress.setValue(INT_MAX);

        delete box;
    }

    if (meshComputed) updateUIFromOutput();
}

void MainWindow::exportMesh()
{
    qDebug() << "Export Mesh";

    if (!meshComputed)
    {
        QMessageBox::StandardButton reply = QMessageBox::question(this, "Compute mesh", "Output mesh has not been computed. Would you like to compute it now?", QMessageBox::Yes|QMessageBox::No);

        if (reply == QMessageBox::Yes)
            this->computeMesh();
    }

    if (meshComputed)
    {
        if (meshGenerator)
        {
            if (outFilename.empty())
            {
                this->setOutputFile();
            }

            this->setLabelDomainsFromUI();

            QFileInfo fileInfo(QString(this->outFilename.c_str()));
            QString extension =fileInfo.completeSuffix();

            std::cout << "About to export file with extension " << extension.toStdString() << std::endl;

            if (extension == QString("off") || extension == QString("mesh"))
            {
                if (CGALMeshGenerator* cgal = dynamic_cast<CGALMeshGenerator*>(meshGenerator))
                {
                    std::cout << "Use CGAL to export the file " << this->outFilename << std::endl;
                    cgal->exportMesh(this->outFilename);
                }
            }
            else if (extension == QString("vtu") || extension == QString("xml") || extension == QString("obj"))
            {
                if (CGALMeshGenerator* cgal = dynamic_cast<CGALMeshGenerator*>(meshGenerator))
                {
                    PolygonMesh<Tetrahedron3D> outputMesh = cgal->getTetrahedronMesh();

                    //Transform the mesh according to the UI scaling component
                    std::cout << "Scale the mesh with factor " << ui.scaleOutputLineEdit->text().toDouble() << " ..." << std::endl;
                    Transform t = Transform();
                    t.setScale(ui.scaleOutputLineEdit->text().toDouble());
                    std::cout << "Applied transformation is " << t << std::endl;

                    std::cout << "Output mesh:  " << outputMesh << std::endl;

                    if (extension == QString("vtu"))
                    {
                        VTUExport fileExport = VTUExport();
                        fileExport.exportToFile( &outputMesh, this->outFilename, t);
                    }
                    else if (extension == QString("xml"))
                    {
                        DolfinExporter fileExport = DolfinExporter();
                        fileExport.exportToFile( &outputMesh, this->outFilename, t);
                    }
                    else if (extension == QString("obj"))
                    {
                        OBJExporter fileExport = OBJExporter();
                        fileExport.exportToFile( &outputMesh, this->outFilename, t);
                    }
                }
                else if (CopyMeshGenerator* copy = dynamic_cast<CopyMeshGenerator*>(meshGenerator))
                {
                    BaseMesh* outputMesh = copy->getMesh();

                    //Transform the mesh according to the UI scaling component
                    std::cout << "Scale the mesh with factor " << ui.scaleOutputLineEdit->text().toDouble() << " ..." << std::endl;
                    Transform t = Transform();
                    t.setScale(ui.scaleOutputLineEdit->text().toDouble());
                    std::cout << "Applied transformation is " << t << std::endl;

                    if (extension == QString("obj"))
                    {
                        OBJExporter fileExport = OBJExporter();
                        fileExport.exportToFile( outputMesh, this->outFilename, t);
                    }
                    else if (extension == QString("xml"))
                    {
                        DolfinExporter fileExport = DolfinExporter();
                        fileExport.exportToFile( outputMesh, this->outFilename, t);
                    }
                    else if (extension == QString("vtu"))
                    {
                        VTUExport fileExport = VTUExport();
                        fileExport.exportToFile( outputMesh, this->outFilename, t);
                    }
                }
                else if (CylinderMeshGenerator* cyl = dynamic_cast<CylinderMeshGenerator*>(meshGenerator))
                {
                    PolygonMesh<Tetrahedron3D> outputMesh = cyl->getOutputMesh();

                    //Transform the mesh according to the UI scaling component
                    std::cout << "Scale the mesh with factor " << ui.scaleOutputLineEdit->text().toDouble() << " ..." << std::endl;
                    Transform t = Transform();
                    t.setScale(ui.scaleOutputLineEdit->text().toDouble());
                    std::cout << "Applied transformation is " << t << std::endl;

                    std::cout << "Output mesh:  " << outputMesh << std::endl;

                    if (extension == QString("vtu"))
                    {
                        VTUExport fileExport = VTUExport();
                        fileExport.exportToFile( &outputMesh, this->outFilename, t);
                    }
                    else if (extension == QString("xml"))
                    {
                        DolfinExporter fileExport = DolfinExporter();
                        fileExport.exportToFile( &outputMesh, this->outFilename, t);
                    }
                    else if (extension == QString("obj"))
                    {
                        OBJExporter fileExport = OBJExporter();
                        fileExport.exportToFile( &outputMesh, this->outFilename, t);
                    }
                }
            }

        }
    }
}

void MainWindow::updateUIFromInputFile()
{
    if (!this->reader)
    {
        std::cerr << "File reader is not correctly set" << std::endl;
        return;
    }

    ui.inputLineEdit->setText(QString(this->reader->getFilename().c_str()));

    if (inputWidget) delete inputWidget;

    inputWidget = new QTreeWidget;
    inputWidget->setColumnCount(2);

    QStringList m_TableHeader;
    m_TableHeader<<"Property"<<"Value";
    inputWidget->setHeaderLabels(m_TableHeader);

    QTreeWidgetItem *treeItem = new QTreeWidgetItem(inputWidget);
    treeItem->setText(0, QString("Input"));

    if (this->reader->getFileType() == FileType::IMAGE)
    {
        QTreeWidgetItem *treeItem_type = new QTreeWidgetItem(treeItem);
        treeItem_type->setText(0, QString("Input type"));
        treeItem_type->setText(1, QString("Image"));

        if (ImageFileReader* img_read = dynamic_cast<ImageFileReader*>(this->reader))
        {

            QString imgTypeString("Unknown");
            ImageType imgType = img_read->getImage()->getDatatype();//plante si l'image est null
            switch(imgType)
            {
            case ImageType::UNSIGNED_CHAR:
                imgTypeString = QString("Unsigned char");
                break;
            case ImageType::SHORT:
                imgTypeString = QString("Short");
                break;
            case ImageType::INT:
                imgTypeString = QString("Int");
                break;
            case ImageType::FLOAT:
                imgTypeString = QString("Float");
                break;
            case ImageType::DOUBLE:
                imgTypeString = QString("Double");
                break;
            }

            QTreeWidgetItem *treeItem_imgType = new QTreeWidgetItem(treeItem);
            treeItem_imgType->setText(0, QString("Image type"));
            treeItem_imgType->setText(1, imgTypeString);

            QTreeWidgetItem *treeItem_imgDim = new QTreeWidgetItem(treeItem);
            treeItem_imgDim->setText(0, QString("Image dimension"));
            treeItem_imgDim->setText(1, QString::number(img_read->getImage()->getDimX()) + "x" +
                                     QString::number(img_read->getImage()->getDimY()) + "x" +
                                     QString::number(img_read->getImage()->getDimZ()));

            std::ostringstream ss;
            ss << img_read->getImage()->getTransform().getScale();
            QTreeWidgetItem *treeItem_imgVoxDim = new QTreeWidgetItem(treeItem);
            treeItem_imgVoxDim->setText(0, QString("Voxel dimension"));
            treeItem_imgVoxDim->setText(1, QString(ss.str().c_str()));

            ss.clear(); ss.str("");
            ss << img_read->getImage()->getTransform().getTranslation();
            QTreeWidgetItem *treeItem_imgTranslation = new QTreeWidgetItem(treeItem);
            treeItem_imgTranslation->setText(0, QString("Image translation"));
            treeItem_imgTranslation->setText(1, QString(ss.str().c_str()));

            ss.clear(); ss.str("");
            ss << img_read->getImage()->getTransform().getRotation();
            QTreeWidgetItem *treeItem_imgRotation = new QTreeWidgetItem(treeItem);
            treeItem_imgRotation->setText(0, QString("Image rotation (quaternion)"));
            treeItem_imgRotation->setText(1, QString(ss.str().c_str()));

            ss.clear(); ss.str("");
            ss << img_read->getImage()->getTransform().getRotation().toEuler();
            QTreeWidgetItem *treeItem_imgRotationEuler = new QTreeWidgetItem(treeItem);
            treeItem_imgRotationEuler->setText(0, QString("Image rotation (Euler)"));
            treeItem_imgRotationEuler->setText(1, QString(ss.str().c_str()));

            QTreeWidgetItem *treeItem_imgTransformation = new QTreeWidgetItem(treeItem);
            treeItem_imgTransformation->setText(0, QString("Image transformation matrix"));

            Matrix<double, 4> m = img_read->getImage()->getTransform().toMatrix();
            for (unsigned int i = 0 ; i < 4; i++)
            {
                ss.clear(); ss.str("");
                ss << m.getRow(i);
                QTreeWidgetItem *treeItem_imgTransformation_0 = new QTreeWidgetItem(treeItem_imgTransformation);
    //            treeItem_imgTransformation_0->setText(0, QString("Row ") + QString::number(i));
                treeItem_imgTransformation_0->setText(1, QString(ss.str().c_str()));
            }

            if (std::shared_ptr<Image<unsigned char> > typed_img = std::dynamic_pointer_cast<Image<unsigned char> >(img_read->getImage()))
            {

                ss.clear(); ss.str("");
                ss << "[" << static_cast<unsigned>(typed_img->getMin()) << ", "<< static_cast<unsigned>(typed_img->getMax()) << "]";
                QTreeWidgetItem *treeItem_imgRange = new QTreeWidgetItem(treeItem);
                treeItem_imgRange->setText(0, QString("Intensity range"));
                treeItem_imgRange->setText(1, QString(ss.str().c_str()));
            }

            slice = new ImageSlice(img_read->getImage());

            if (imageWidget) delete imageWidget;

            imageWidget = new QWidget;
            ui.horizontalLayout->addWidget(imageWidget);

            imageLayout = new QGridLayout;
            imageWidget->setLayout(imageLayout);

            sliderX = new QSlider;
            sliderX->setOrientation(Qt::Horizontal);
            sliderX->setMinimum(0);
            sliderX->setMaximum(img_read->getImage()->getDimX()-1);
            sliderX->setValue(img_read->getImage()->getDimX()*0.5);
            connect(sliderX, SIGNAL(valueChanged(int)), this, SLOT(updateImagesViewX(int)));
            imageLayout->addWidget(sliderX, 0, 0);

            imageXLabel = new QLabel;
            imageLayout->addWidget(imageXLabel, 1, 0);

            imageCaptionX = new QLabel(QString("[X] slice(") + QString::number(img_read->getImage()->getDimX()*0.5) + QString(")") );
            imageLayout->addWidget(imageCaptionX, 2, 0);

            sliderY = new QSlider;
            sliderY->setOrientation(Qt::Horizontal);
            sliderY->setMinimum(0);
            sliderY->setMaximum(img_read->getImage()->getDimY()-1);
            sliderY->setValue(img_read->getImage()->getDimY()*0.5);
            connect(sliderY, SIGNAL(valueChanged(int)), this, SLOT(updateImagesViewY(int)));
            imageLayout->addWidget(sliderY, 0, 1);

            imageYLabel = new QLabel;
            imageLayout->addWidget(imageYLabel, 1, 1);

            imageCaptionY = new QLabel(QString("[Y] slice(") + QString::number(img_read->getImage()->getDimY()*0.5) + QString(")") );
            imageLayout->addWidget(imageCaptionY, 2, 1);

            sliderZ = new QSlider;
            sliderZ->setOrientation(Qt::Horizontal);
            sliderZ->setMinimum(0);
            sliderZ->setMaximum(img_read->getImage()->getDimZ()-1);
            sliderZ->setValue(img_read->getImage()->getDimZ()*0.5);
            connect(sliderZ, SIGNAL(valueChanged(int)), this, SLOT(updateImagesViewZ(int)));
            imageLayout->addWidget(sliderZ, 3, 0);

            imageZLabel = new QLabel;
            imageLayout->addWidget(imageZLabel, 4, 0);

            imageCaptionZ = new QLabel(QString("[Z] slice(") + QString::number(img_read->getImage()->getDimZ()*0.5) + QString(")") );
            imageLayout->addWidget(imageCaptionZ, 5, 0);

            updateImagesViewX(img_read->getImage()->getDimX()/2);
            updateImagesViewY(img_read->getImage()->getDimY()/2);
            updateImagesViewZ(img_read->getImage()->getDimZ()/2);
        }



    }
    else if (this->reader->getFileType() == FileType::MESH)
    {
        MeshFileReader* m_reader = dynamic_cast<MeshFileReader*>(this->reader);

        if (!m_reader)
        {
            std::cerr << "Wrong type of file reader. Should be of type MESH" << std::endl;
        }
        else
        {
            QTreeWidgetItem *treeItem_type = new QTreeWidgetItem(treeItem);
            treeItem_type->setText(0, QString("Input type"));
            treeItem_type->setText(1, QString("Mesh"));

            QString meshTypeString("Unknown");
            MeshType meshType = m_reader->getMeshType();
            if (meshType == MeshType::TRIANGLES)
                meshTypeString =  QString("Triangles");
            else if (meshType == MeshType::TETRAHEDRONS)
                meshTypeString =  QString("Tetrahedrons");
            else if (meshType == MeshType::HEXAHEDRONS)
                meshTypeString =  QString("Triangles");

            QTreeWidgetItem *treeItem_meshType = new QTreeWidgetItem(treeItem);
            treeItem_meshType->setText(0, QString("Mesh type"));
            treeItem_meshType->setText(1, meshTypeString);

            QTreeWidgetItem *treeItem_nbVertices = new QTreeWidgetItem(treeItem);
            treeItem_nbVertices->setText(0, QString("Number of points"));
            treeItem_nbVertices->setText(1, QString::number(m_reader->getNbVertices()));

            QTreeWidgetItem *treeItem_nbCells = new QTreeWidgetItem(treeItem);
            treeItem_nbCells->setText(0, QString("Number of cells"));
            treeItem_nbCells->setText(1, QString::number(m_reader->getNbCells()));

            if (PolygonMesh<Triangle3D>* pmesh = dynamic_cast<PolygonMesh<Triangle3D>* >(mesh))
            {
                BoundingBox<> bb = mesh->getBoundingBox();

                QTreeWidgetItem *treeItem_bb = new QTreeWidgetItem(treeItem);
                treeItem_bb->setText(0, QString("Bounding box"));
                treeItem_bb->setText(1, QString(bb.print().c_str()));

                std::stringstream ss;
                ss << bb.getMin();
                QTreeWidgetItem *treeItem_bb_min = new QTreeWidgetItem(treeItem_bb);
                treeItem_bb_min->setText(0, QString("Min"));
                treeItem_bb_min->setText(1, QString(ss.str().c_str()));

                ss.str("");ss.clear();
                ss << bb.getMax();
                QTreeWidgetItem *treeItem_bb_max = new QTreeWidgetItem(treeItem_bb);
                treeItem_bb_max->setText(0, QString("Max"));
                treeItem_bb_max->setText(1, QString(ss.str().c_str()));

                ss.str("");ss.clear();
                ss << bb.getDiagonal();
                QTreeWidgetItem *treeItem_bb_diag = new QTreeWidgetItem(treeItem_bb);
                treeItem_bb_diag->setText(0, QString("Diagonal"));
                treeItem_bb_diag->setText(1, QString(ss.str().c_str()));

                if (pmesh->getNbPoints() < 2000)
                {
                    QTreeWidgetItem *treeItem_vert = new QTreeWidgetItem(treeItem);
                    treeItem_vert->setText(0, QString("Vertices"));

                    for (unsigned int i = 0 ; i < pmesh->getNbPoints(); i++)
                    {
                        ss.str("");ss.clear();
                        ss << pmesh->getPosition(i);
                        QTreeWidgetItem *treeItem_vert_i = new QTreeWidgetItem(treeItem_vert);
                        treeItem_vert_i->setText(0, QString::number(i));
                        treeItem_vert_i->setText(1, QString(ss.str().c_str()));
                    }
                }

                if (pmesh->getNbCells() < 2000)
                {
                    QTreeWidgetItem *treeItem_cells = new QTreeWidgetItem(treeItem);
                    treeItem_cells->setText(0, QString("Cells"));

                    for (unsigned int i = 0 ; i < pmesh->getNbCells(); i++)
                    {
                        ss.str("");ss.clear();
                        for (unsigned int j = 0 ; j < pmesh->getCell(i).getSize(); j++)
                        {
                            ss << pmesh->getCell(i).getIndex(j);
                            if (j < pmesh->getCell(i).getSize()-1)ss << " ";
                        }
                        QTreeWidgetItem *treeItem_cell_i = new QTreeWidgetItem(treeItem_cells);
                        treeItem_cell_i->setText(0, QString::number(i));
                        treeItem_cell_i->setText(1, QString(ss.str().c_str()));
                    }
                }
            }

        }
    }

    if (this->reader->getFileType() != FileType::NONE)
    {
        inputWidget->expandToDepth(0);
        inputWidget->resizeColumnToContents(0);
        ui.verticalLayout_input->addWidget(inputWidget);

        addSubdomainButton = new QPushButton("Add label");
        connect(addSubdomainButton, SIGNAL(clicked()), this, SLOT(addLabel()));
        ui.verticalLayout_input->addWidget(addSubdomainButton);


//        meshViewer = new MeshViewer;
    }


}

void MainWindow::updateUIFromOutput()
{
    //the output mesh needs to be computed
    if (!meshComputed) return;
    //check also that a mesh generator has been created
    if (!meshGenerator) return;

    //if the mesh has been generated with CGAL
    if (CGALMeshGenerator* cgal = dynamic_cast<CGALMeshGenerator*>(meshGenerator))
    {
        //get the mesh
        PolygonMesh<Tetrahedron3D> outputMesh = cgal->getTetrahedronMesh();

        if (!outputInfoWidget)
        {
            //create the output widget in case it does not exist yet
            outputInfoWidget = new OutputInfoWidget(outputWidget);
            int index = ui.verticalLayout_output->indexOf(ui.buttonsHorizontalWidget);
            ui.verticalLayout_output->insertWidget(index, outputInfoWidget);
        }
        else
            outputInfoWidget->clear();

        if (outputInfoWidget)
        {
            //display the number of vertices
            outputInfoWidget->addNbVertices(outputMesh.getNbPoints());
            //display the number of cells
            outputInfoWidget->addNbCells(outputMesh.getNbCells());
            //display the number of facets
            outputInfoWidget->addNbFacets(outputMesh.getNbFacets());
            //display the bounding box
            outputInfoWidget->addBoundingBox(outputMesh.getBoundingBox());

            //display the different labels
            for (const auto& l : outputMesh.getLabels())
            {
                outputInfoWidget->addLabel(l,
                                           outputMesh.getNbPointsWithLabel(l),
                                           outputMesh.getNbFacetsWithLabel(l),
                                           outputMesh.getNbCellsWithLabel(l));
            }

            //display the details of the vertices (only if nbVertices < NB_MAX)
            outputInfoWidget->addVertices(outputMesh.getPositions());
        }
    }
    //if the mesh has been generated with a simple copy
    else if (CopyMeshGenerator* copy = dynamic_cast<CopyMeshGenerator*>(meshGenerator))
    {
        BaseMesh* outputMesh = copy->getMesh();

        if (!outputInfoWidget)
        {
            outputInfoWidget = new OutputInfoWidget(outputWidget);
            int index = ui.verticalLayout_output->indexOf(ui.buttonsHorizontalWidget);
            ui.verticalLayout_output->insertWidget(index, outputInfoWidget);
        }
        else
            outputInfoWidget->clear();
        if (outputInfoWidget)
        {
            outputInfoWidget->addNbVertices(outputMesh->getNbPoints());
            outputInfoWidget->addNbCells(outputMesh->getNbCells());
            outputInfoWidget->addNbFacets(outputMesh->getNbFacets());
            outputInfoWidget->addBoundingBox(outputMesh->getBoundingBox());

//            for (const auto& l : outputMesh.getLabels())
//            {
//                outputInfoWidget->addLabel(l,
//                                           outputMesh.getNbPointsWithLabel(l),
//                                           outputMesh.getNbFacetsWithLabel(l),
//                                           outputMesh.getNbCellsWithLabel(l));
//            }

            if (PolygonMesh<Tetrahedron3D>* m = dynamic_cast<PolygonMesh<Tetrahedron3D>* >(outputMesh))
            {
                outputInfoWidget->addVertices(m->getPositions());
            }
            else if (PolygonMesh<Triangle3D>* m = dynamic_cast<PolygonMesh<Triangle3D>* >(outputMesh))
            {
                outputInfoWidget->addVertices(m->getPositions());
            }

        }
    }
    else if (CylinderMeshGenerator* cylinderGen = dynamic_cast<CylinderMeshGenerator*>(meshGenerator))
    {
        PolygonMesh<Tetrahedron3D> outputMesh = cylinderGen->getOutputMesh();

        if (!outputInfoWidget)
        {
            outputInfoWidget = new OutputInfoWidget(outputWidget);
            int index = ui.verticalLayout_output->indexOf(ui.buttonsHorizontalWidget);
            ui.verticalLayout_output->insertWidget(index, outputInfoWidget);
        }
        else
            outputInfoWidget->clear();

        if (outputInfoWidget)
        {
            outputInfoWidget->addNbVertices(outputMesh.getNbPoints());
            outputInfoWidget->addNbCells(outputMesh.getNbCells());
            outputInfoWidget->addNbFacets(outputMesh.getNbFacets());
            outputInfoWidget->addBoundingBox(outputMesh.getBoundingBox());
            outputInfoWidget->addVertices(outputMesh.getPositions());
            for (const auto& l : outputMesh.getLabels())
            {
                outputInfoWidget->addLabel(l,
                                           outputMesh.getNbPointsWithLabel(l),
                                           outputMesh.getNbFacetsWithLabel(l),
                                           outputMesh.getNbCellsWithLabel(l));
            }
        }

    }
}

MeshingMethod MainWindow::getMeshingMethod()
{
    QString m = ui.methodComboBox->currentText();

    if (m == QString("CGAL - Delauney"))
        return MeshingMethod::CGAL;
    else if (m == QString("Simple conversion"))
        return MeshingMethod::CONVERSION;
    else if (m == QString("Cylinder"))
        return MeshingMethod::CYLINDER;

    return MeshingMethod::NONE;
}

void MainWindow::setLabelDomainsFromUI()
{
    if (!labelList) return;//the user didn't add a label yet

    if (meshParameters)
    {
        meshParameters->labelDomains.clear();

        for (auto i = 0 ; i < labelList->size(); i++)
        {
            BBoxWidget* bboxw = labelList->getWidget(i);
            BoundingBox<> bbox = bboxw->getBoundingBox();
            if (meshGenerator)
            {
                if (CGALMeshGenerator* cgal = dynamic_cast<CGALMeshGenerator*>(meshGenerator))
                {
                    std::cout << "Bounding box " << bbox.print() << " in image world ";
                    bbox.applyTransform(cgal->getTransform());
                    std::cout << "has been transformed to "<< bbox.print() << " in mesh world" << std::endl;
                }
            }

            meshParameters->labelDomains.push_back( BoundingBoxLabelDomain<>(bbox,
                                                                             bboxw->getValue(),
                                                                             bboxw->getCheckPoints(),
                                                                             bboxw->getCheckFacets(),
                                                                             bboxw->getCheckCells() )
                                                    );
        }

        if (meshGenerator)
        {
            //give the labels to the mesh generator and update the output mesh
            meshGenerator->updateLabels(meshParameters.get());
        }
    }
}

void MainWindow::writeWindowSettings()
{
    QSettings app_settings;

    app_settings.beginGroup("MainWindow");

    app_settings.setValue("size", this->size());
//    qDebug() << "Save window size " << size();

    app_settings.setValue("pos", this->pos());
//    qDebug() << "Save window pos " << pos();

    app_settings.setValue("meshing_method", ui.methodComboBox->currentText());

    app_settings.endGroup();
}

void MainWindow::readWindowSettings()
{
    QSettings app_settings;

    app_settings.beginGroup("MainWindow");

    QSize size = app_settings.value("size", QSize(400, 400)).toSize();
    qDebug() << "Read window size " << size;
    resize(size);

    QPoint point = app_settings.value("pos", QPoint(200, 200)).toPoint();
    qDebug() << "Read window point " << point;
    move(point);

    QString meshing_method = app_settings.value("meshing_method", QString("CGAL - Delauney")).toString();
    ui.methodComboBox->setCurrentText(meshing_method);
    setMeshingMethod(ui.methodComboBox->currentText());

    app_settings.endGroup();
}

void MainWindow::resizeEvent(QResizeEvent* event)
{
    QMainWindow::resizeEvent(event);

    updateImagesView();
}

void MainWindow::updateImagesView()
{
    if (sliderX) updateImagesViewX(sliderX->value());
    if (sliderY) updateImagesViewY(sliderY->value());
    if (sliderZ) updateImagesViewZ(sliderZ->value());
}

void MainWindow::setMeshingMethod(QString text)
{
    std::cerr << text.toStdString().c_str() << " is the current meshing method" << std::endl;

    //if the user selected CGAL meshing method
    if (text == QString("CGAL - Delauney"))
    {
        //the previous widget is deleted
        if (meshParametersWidget) delete meshParametersWidget;

        //... and a new one corresponding to CGAL is created
        meshParametersWidget = new CGALParametersWidget;

        //the widget is inserted at the right position
        int index = ui.verticalLayout_output->indexOf(ui.methodHorizontalWidget);
        ui.verticalLayout_output->insertWidget(index+1, meshParametersWidget);

        // if an input mesh already exists
        if (mesh)
        {
            meshParameters = std::make_shared<CGALParameters>();
            if (PolygonMesh<Triangle3D>* triangleMesh = dynamic_cast<PolygonMesh<Triangle3D>* >(mesh))
            {
                meshGenerator = new MeshGeneratorFromPolyhedra(triangleMesh);
                meshGenerator->init();

                inputLoaded = true;
                meshComputed = false;
            }
        }
    }
    else if (text == QString("Simple conversion"))
    {
        if (meshParametersWidget)
        {
            delete meshParametersWidget;
            meshParametersWidget = nullptr;
        }

        // if an input mesh already exists
        if (mesh)
        {
            meshGenerator = new CopyMeshGenerator(mesh);
            meshGenerator->init();

            inputLoaded = true;
            meshComputed = false;
        }
    }
    else if (text == QString("Cylinder"))
    {
        if (meshParametersWidget) delete meshParametersWidget;
        meshParametersWidget = new CylinderGenerationWidget;

        //the widget is inserted at the right position
        int index = ui.verticalLayout_output->indexOf(ui.methodHorizontalWidget);
        ui.verticalLayout_output->insertWidget(index+1, meshParametersWidget);

        meshParameters = std::make_shared<CylinderMeshGeneratorParameters>();

        meshGenerator = new CylinderMeshGenerator();
        meshGenerator->init();
    }
    else
        std::cerr << "Meshing method " << text.toStdString().c_str() << " not supported" << std::endl;
}

void MainWindow::updateImagesViewX(int newValue)
{
    if (sliderX && slice)
    {
        if (imageXLabel)
        {
            QPixmap* pixmap = new QPixmap(QPixmap::fromImage(slice->getSliceX(newValue)));

            if (labelList)
            {
                std::vector<BoundingBox<> > boxes = labelList->getBoundingBoxes();

                QPainter* pixPaint = new QPainter(pixmap);

                for (unsigned int i = 0 ; i < boxes.size(); i++)
                {
                    if (boxes[i].isPlaneXIn(newValue))
                    {
                        QPen p;
                        p.setColor(labelList->getWidget(i)->getColor());\
                        pixPaint->setPen(p);

                        pixPaint->drawLine(
                                    boxes[i].getMin()[1], boxes[i].getMin()[2],
                                    boxes[i].getMax()[1], boxes[i].getMin()[2]
                                );
                        pixPaint->drawLine(
                                    boxes[i].getMin()[1], boxes[i].getMin()[2],
                                    boxes[i].getMin()[1], boxes[i].getMax()[2]
                                );
                        pixPaint->drawLine(
                                    boxes[i].getMax()[1], boxes[i].getMin()[2],
                                    boxes[i].getMax()[1], boxes[i].getMax()[2]
                                );
                        pixPaint->drawLine(
                                    boxes[i].getMin()[1], boxes[i].getMax()[2],
                                    boxes[i].getMax()[1], boxes[i].getMax()[2]
                                );
                    }
                }

            }


            imageXLabel->setPixmap(pixmap->scaled(imageWidget->height()*IMAGE_VIEW_PROPORTION,
                                                  imageWidget->height()*IMAGE_VIEW_PROPORTION,
                                                  Qt::IgnoreAspectRatio));
        }
        if (imageCaptionX) imageCaptionX->setText(QString("[X] slice(") + QString::number(newValue) + QString(")"));
    }
}
void MainWindow::updateImagesViewY(int newValue)
{
    if (sliderY && slice)
    {
        if (imageYLabel)
        {
            QPixmap* pixmap = new QPixmap(QPixmap::fromImage(slice->getSliceY(newValue)));

            if (labelList)
            {
                std::vector<BoundingBox<> > boxes = labelList->getBoundingBoxes();

                QPainter* pixPaint = new QPainter(pixmap);

                for (unsigned int i = 0 ; i < boxes.size(); i++)
                {
                    if (boxes[i].isPlaneYIn(newValue))
                    {
                        QPen p;
                        p.setColor(labelList->getWidget(i)->getColor());\
                        pixPaint->setPen(p);

                        pixPaint->drawLine(
                                    boxes[i].getMin()[0], boxes[i].getMin()[2],
                                    boxes[i].getMax()[0], boxes[i].getMin()[2]
                                );
                        pixPaint->drawLine(
                                    boxes[i].getMin()[0], boxes[i].getMin()[2],
                                    boxes[i].getMin()[0], boxes[i].getMax()[2]
                                );
                        pixPaint->drawLine(
                                    boxes[i].getMax()[0], boxes[i].getMin()[2],
                                    boxes[i].getMax()[0], boxes[i].getMax()[2]
                                );
                        pixPaint->drawLine(
                                    boxes[i].getMin()[0], boxes[i].getMax()[2],
                                    boxes[i].getMax()[0], boxes[i].getMax()[2]
                                );
                    }
                }

            }

            imageYLabel->setPixmap(pixmap->scaled(imageWidget->height()*IMAGE_VIEW_PROPORTION,
                                                  imageWidget->height()*IMAGE_VIEW_PROPORTION,
                                                  Qt::IgnoreAspectRatio));
        }
        if (imageCaptionY) imageCaptionY->setText(QString("[Y] slice(") + QString::number(newValue) + QString(")"));
    }
}
void MainWindow::updateImagesViewZ(int newValue)
{
    if (sliderZ && slice)
    {
        if (imageZLabel)
        {
            QPixmap* pixmap = new QPixmap(QPixmap::fromImage(slice->getSliceZ(newValue)));

            if (labelList)
            {
                std::vector<BoundingBox<> > boxes = labelList->getBoundingBoxes();

                QPainter* pixPaint = new QPainter(pixmap);

                for (unsigned int i = 0 ; i < boxes.size(); i++)
                {
                    if (boxes[i].isPlaneZIn(newValue))
                    {
                        QPen p;
                        p.setColor(labelList->getWidget(i)->getColor());\
                        pixPaint->setPen(p);

                        pixPaint->drawLine(
                                    boxes[i].getMin()[0], boxes[i].getMin()[1],
                                    boxes[i].getMax()[0], boxes[i].getMin()[1]
                                );
                        pixPaint->drawLine(
                                    boxes[i].getMin()[0], boxes[i].getMin()[1],
                                    boxes[i].getMin()[0], boxes[i].getMax()[1]
                                );
                        pixPaint->drawLine(
                                    boxes[i].getMax()[0], boxes[i].getMin()[1],
                                    boxes[i].getMax()[0], boxes[i].getMax()[1]
                                );
                        pixPaint->drawLine(
                                    boxes[i].getMin()[0], boxes[i].getMax()[1],
                                    boxes[i].getMax()[0], boxes[i].getMax()[1]
                                );
                    }
                }

            }

            imageZLabel->setPixmap(pixmap->scaled(imageWidget->height()*IMAGE_VIEW_PROPORTION,
                                                  imageWidget->height()*IMAGE_VIEW_PROPORTION,
                                                  Qt::IgnoreAspectRatio));
        }
        if (imageCaptionZ)
            imageCaptionZ->setText(QString("[Z] slice(") + QString::number(newValue) + QString(")"));
    }
}

void MainWindow::addLabel()
{
    qDebug() << "Add label";

    if (!labelList)
    {
        labelList = new BBoxWidgetList;

        QWidget* labelListWidget = new QWidget;
        labelListWidget->setLayout(labelList);

        int index = ui.verticalLayout_input->indexOf(addSubdomainButton);
        ui.verticalLayout_input->insertWidget(index, labelListWidget);

        qDebug() << "The space to add the label widgets has been created";
    }

    if (labelList)
    {
        if (labelList->count())
        {
            QFrame* line = new QFrame;
            line->setObjectName(QString::fromUtf8("line"));
            line->setFrameShape(QFrame::HLine);
            line->setFrameShadow(QFrame::Sunken);
            labelList->addWidget(line);
        }

        BBoxWidget* bbox = new BBoxWidget(this);
        if (reader->getFileType() == FileType::IMAGE)
        {
            if (ImageFileReader* img_read = dynamic_cast<ImageFileReader*>(this->reader))
                bbox->provideImageData(img_read->getImage());
        }
        else if (reader->getFileType() == FileType::MESH)
        {

        }
        else
        {
            std::cerr << "Wrong file format" << std::endl;
        }
        labelList->addWidget(bbox);
        labelList->updateLabelNames();

        //a domain has been created, so we need to see it on the images viewer
        this->updateImagesView();

        //get the labels from GUI
        this->setLabelDomainsFromUI();

        //if a mesh has already been computed, we update the label information
        this->updateUIFromOutput();
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    writeWindowSettings();
    event->accept();
}
