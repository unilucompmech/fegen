#ifndef MATRIX_H
#define MATRIX_H

#include <cstddef>
#include <assert.h>
#include <functional>

#include "vector.h"
#include "quaternion.h"

//! Matrix
/*!
  M-by-N matrix with its mathematical treatments (multiplication, etc)
*/
template <class T, std::size_t M, std::size_t N = M>
class Matrix
{
public:
    //! Default constructor
    /*!
     * \brief The matrix is set to 0 (zero)
     */
    Matrix()
    {
        for (unsigned int i = 0 ; i < M; i++)
            for (unsigned int j = 0 ; j < N; j++)
                this->data[i][j] = (T)0;
    }

    //! Access the element of the row row and column col
    /*!
     * \brief operator ()
     * \param row Row row
     * \param col Column row
     * \return Element
     */
    T& operator()(int row, int col)
    {
        assert(row >= 0 && row < M);
        assert(col >= 0 && col < N);
        return data[row][col];
    }

    //! Get the number of rows
    int getNbRow(){return M;}
    //! Get the number of columns
    int getNbCol(){return N;}

    //! Set the matrix to identity
    void identity()
    {
        for (unsigned int i = 0 ; i < M; i++)
            for (unsigned int j = 0 ; j < N; j++)
                if (i != j)
                    this->data[i][j] = (T)0;
                else
                    this->data[i][j] = (T)1;
    }

    //! Get the i-th row
    /*!
     * \brief Return a vector composed of the element of the i-th row
     * \param i i-th row
     * \return Vector
     */
    FixedVector<T, N> getRow(int i)
    {
        FixedVector<T,N> v;
        for (unsigned int c = 0; c < N; c++)
            v[c] = this->operator ()(i,c);
        return v;
    }

    //! Multiplicator operator
    template<size_t P>
    Matrix<T, M, P> operator*(Matrix<T, N, P>& v)
    {
        Matrix<T, M, P> result;
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < P; j++)
            {
                T a = 0;
                for (unsigned int k = 0; k < N; k++)
                    a += this->operator ()(i,k) * v(k,j);
                result(i,j) = a;
            }

        return result;
    }

    //! Multiplicator operator
    FixedVector<T, M> operator*(FixedVector<T, N>& v)
    {
        FixedVector<T,M> result;

        for (unsigned int i = 0; i < M; i++)
        {
            result[i] = 0;
            for (unsigned int j = 0; j < N; j++)
            {
                result[i] += this->operator ()(i,j)*v[j];
            }
        }

        return result;
    }

    friend std::ostream& operator<<(std::ostream &os, const Matrix<T,M,N>& m)
    {
        os << "[";
        for (unsigned int i = 0 ; i < M; i++)
        {
            os << "(";
            for (unsigned int j = 0 ; j < M; j++)
            {
                os << m.data[i][j];
                if (j < N-1) os << ", ";
            }
            os << ")";
            if (i < M-1) os << ", ";
        }
        os << "]";
        return os;
    }

protected:
    T data[N][M];
};


//operators
template <typename _T, std::size_t _M, std::size_t _N, typename BinaryOp>
Matrix<_T, _M, _N> base_op(const Matrix<_T, _M, _N>& a, const Matrix<_T, _M, _N>& b, BinaryOp f)
{
    Matrix<_T, _M, _N> result;
    for (unsigned int i = 0 ; i < _M; i++)
    {
        for (unsigned int j = 0 ; j < _N; j++)
        {
            result(i,j) = f(a(i,j), b(i,j));
        }
    }
    return result;
}

template <typename _T, std::size_t _M, std::size_t _N>
Matrix<_T, _M, _N> operator+(const Matrix<_T, _M, _N>& a, const Matrix<_T, _M, _N>& b)
{
    return base_op(a, b, std::plus<_T>());
}

template <typename _T, std::size_t _M, std::size_t _N>
Matrix<_T, _M, _N> operator-(const Matrix<_T, _M, _N>& a, const Matrix<_T, _M, _N>& b)
{
    return base_op(a, b, std::minus<_T>());
}

template <class T>
class RotationMatrix : public Matrix<T, 3>
{
public:
    RotationMatrix() : Matrix<T, 3>()
    {
        this->identity();
    }

    void fromQuaternion(const Quaternion<T> q)
    {
        Quaternion<T> quat = q;

        T a = quat[0];
        T b = quat[1];
        T c = quat[2];
        T d = quat[3];

        this->operator()(0,0) = 1.0 - 2.0 * c * c - 2.0 * d * d;
        this->operator()(0,1) = 2*(b*c - a*d);
        this->operator()(0,2) = 2*(b*d + a*c);

        this->operator()(1,0) = 2*(b*c + a*d);
        this->operator()(1,1) = 1.0 - 2.0 * b * b - 2.0 * d * d;
        this->operator()(1,2) = 2*(c*d - a*b);

        this->operator()(2,0) = 2*(b*d - a*c);
        this->operator()(2,1) = 2*(c*d + a*b);
        this->operator()(2,2) = 1.0 - 2.0 * b * b - 2.0 * c * c;
    }
};

#endif // MATRIX_H
