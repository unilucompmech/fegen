#ifndef LABELDOMAIN_H
#define LABELDOMAIN_H

#include "boundingbox.h"
#include "point3d.h"
#include "polygon3d.h"

template<class T = double>
class LabelDomain
{
public:
    LabelDomain() {}
    virtual ~LabelDomain() = 0;

    T getLabelValue() const {return labelValue;}

    virtual bool isIn(const Point3D<> p) const = 0;

    bool getApplyToCells() const
    {
        return applyToCells;
    }
    void setApplyToCells(bool value)
    {
        applyToCells = value;
    }

    bool getApplyToFacets() const
    {
        return applyToFacets;
    }
    void setApplyToFacets(bool value)
    {
        applyToFacets = value;
    }

    bool getApplyToPoints() const
    {
        return applyToPoints;
    }
    void setApplyToPoints(bool value)
    {
        applyToPoints = value;
    }

protected:
    T labelValue;

    bool applyToPoints;
    bool applyToFacets;
    bool applyToCells;
};
template <class T>
LabelDomain<T>::~LabelDomain()
{}


template<class T1 = double, class T2 = double>
class BoundingBoxLabelDomain : public LabelDomain<T2>
{
public:
    BoundingBoxLabelDomain() : LabelDomain<T2>(), bbox(BoundingBox<T1>()) {}
    BoundingBoxLabelDomain(BoundingBox<T1> b, T2 value) : bbox(b)
    {
        LabelDomain<T2>::labelValue = value;
    }
    BoundingBoxLabelDomain(BoundingBox<T1> b, T2 value, bool points, bool facets, bool cells)
        : bbox(b)
    {
        LabelDomain<T2>::applyToPoints = points;
        LabelDomain<T2>::applyToFacets = facets;
        LabelDomain<T2>::applyToCells  = cells;
        LabelDomain<T2>::labelValue = value;
    }

    ~BoundingBoxLabelDomain(){}

    BoundingBox<T1> getBoundingBox() const {return bbox;}

    bool isIn(const Point3D<T1> p) const override
    {
        return bbox.isIn(p);
    }


protected:
    BoundingBox<T1> bbox;
};

#endif // LABELDOMAIN_H
