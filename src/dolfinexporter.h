#ifndef DOLFINEXPORTER_H
#define DOLFINEXPORTER_H

#include "fileexport.h"

//! Write a mesh in the dolfin .xml format
class DolfinExporter : public FileExport
{
public:
    DolfinExporter();

    void exportToFile(BaseMesh *mesh,                   std::string filename, Transform t = Transform()) override;
    void exportToFile(PolygonMesh<Tetrahedron3D> *mesh, std::string filename, Transform t = Transform());
    void exportToFile(PolygonMesh<Triangle3D> *mesh,    std::string filename, Transform t = Transform());
};

#endif // DOLFINEXPORTER_H
