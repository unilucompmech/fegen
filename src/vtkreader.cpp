#include "vtkreader.h"

#include <set>


VTKReader::VTKReader()
{

}

VTKReader::VTKReader(std::string filename)
{
    std::cout << "Create the Wavefront .obj file reader" << std::endl;
    this->setFilename(filename);
    this->readMesh();
}

void VTKReader::readMesh()
{
    //read all the data from the file
    vtkSmartPointer<vtkGenericDataObjectReader> reader = vtkSmartPointer<vtkGenericDataObjectReader>::New();
    std::cout << "Reading " << m_filename << "..." << std::endl;
    reader->SetFileName(m_filename.c_str());
    reader->Update();

    if(reader->IsFilePolyData())
    {
        std::cout << "input is a polydata" << std::endl;
        vtkPolyData* input = reader->GetPolyDataOutput();
        std::cout << "> Nb points: " << input->GetNumberOfPoints() << std::endl;
        std::cout << "> Nb cells: " << input->GetNumberOfCells() << std::endl;

        std::set<int> cellTypes;
        std::set<int> cellDimensions;
        for (unsigned int i = 0 ; i < input->GetNumberOfCells(); i++)
        {
            cellTypes.insert(input->GetCellType(i));
            cellDimensions.insert(input->GetCell(i)->GetCellDimension());
        }

        std::cout << "> Different types of cells: " << cellTypes.size() << std::endl;
        for (std::set<int>::iterator it = cellTypes.begin(); it != cellTypes.end(); ++it)
        {
            std::cout << "> > Cell type #" << (std::distance(cellTypes.begin(), it)) << ": " << *it << std::endl;
        }

        std::cout << "> Different dimensions of cells: " << cellDimensions.size() << std::endl;
        for (std::set<int>::iterator it = cellDimensions.begin(); it != cellDimensions.end(); ++it)
        {
            std::cout << "> > Cell dimension type #" << (std::distance(cellDimensions.begin(), it)) << ": " << *it << std::endl;
        }

        for(vtkIdType i = 0; i < input->GetNumberOfPoints(); i++)
        {
            double p[3];
            input->GetPoint(i,p);
//            std::cout << "Get point " << p[0] << ", " << p[1] << ", " << p[2] << std::endl;
            vertices.push_back(Point3D<value_type>(p[0],p[1],p[2]));
        }
        for(vtkIdType i = 0; i < input->GetNumberOfCells(); i++)
        {
            if (input->GetCellType(i) == VTK_TRIANGLE )
            {
                vtkCell* c = input->GetCell(i);
                vtkTriangle* t = dynamic_cast<vtkTriangle*>(c);

                Face f;
                f.push_back(t->GetPointId(0));
                f.push_back(t->GetPointId(1));
                f.push_back(t->GetPointId(2));
                cells.push_back(f);
            }
            else if (input->GetCellType(i) == VTK_TETRA )
            {
                vtkCell* c = input->GetCell(i);
                vtkTetra* t = dynamic_cast<vtkTetra*>(c);

                Face f;
                f.push_back(t->GetPointId(0));
                f.push_back(t->GetPointId(1));
                f.push_back(t->GetPointId(2));
                f.push_back(t->GetPointId(3));
                cells.push_back(f);
            }
        }
    }
    else if (reader->IsFileStructuredGrid())
    {
        std::cerr << "VTK reader: File is a structured grid and is not yet supported" << std::endl;
    }
    else if (reader->IsFileRectilinearGrid())
    {
        std::cerr << "VTK reader: File is a rectilinear grid and is not yet supported" << std::endl;
    }
    else if (reader->IsFileUnstructuredGrid())
    {
        vtkUnstructuredGrid* input = reader->GetUnstructuredGridOutput();

        std::cout << "> Nb points: " << input->GetNumberOfPoints() << std::endl;
        std::cout << "> Nb cells: " << input->GetNumberOfCells() << std::endl;

        std::set<int> cellTypes;
        std::set<int> cellDimensions;
        for (unsigned int i = 0 ; i < input->GetNumberOfCells(); i++)
        {
            cellTypes.insert(input->GetCellType(i));
            cellDimensions.insert(input->GetCell(i)->GetCellDimension());
        }

        std::cout << "> Different types of cells: " << cellTypes.size() << std::endl;
        for (std::set<int>::iterator it = cellTypes.begin(); it != cellTypes.end(); ++it)
        {
            std::cout << "> > Cell type #" << (std::distance(cellTypes.begin(), it)) << ": " << *it << std::endl;
        }

        std::cout << "> Different dimensions of cells: " << cellDimensions.size() << std::endl;
        for (std::set<int>::iterator it = cellDimensions.begin(); it != cellDimensions.end(); ++it)
        {
            std::cout << "> > Cell dimension type #" << (std::distance(cellDimensions.begin(), it)) << ": " << *it << std::endl;
        }

        for(vtkIdType i = 0; i < input->GetNumberOfPoints(); i++)
        {
            double p[3];
            input->GetPoint(i,p);
//            std::cout << "Get point " << p[0] << ", " << p[1] << ", " << p[2] << std::endl;
            vertices.push_back(Point3D<value_type>(p[0],p[1],p[2]));
        }
        for(vtkIdType i = 0; i < input->GetNumberOfCells(); i++)
        {
            if (input->GetCellType(i) == VTK_TRIANGLE )
            {
                vtkCell* c = input->GetCell(i);
                vtkTriangle* t = dynamic_cast<vtkTriangle*>(c);

                Face f;
                f.push_back(t->GetPointId(0));
                f.push_back(t->GetPointId(1));
                f.push_back(t->GetPointId(2));
                cells.push_back(f);
            }
            else if (input->GetCellType(i) == VTK_TETRA )
            {
                vtkCell* c = input->GetCell(i);
                vtkTetra* t = dynamic_cast<vtkTetra*>(c);

                Face f;
                f.push_back(t->GetPointId(0));
                f.push_back(t->GetPointId(1));
                f.push_back(t->GetPointId(2));
                f.push_back(t->GetPointId(3));
                cells.push_back(f);
            }
        }
    }
    else
    {
        std::cerr << "VTK reader: Cannot read file " << m_filename << std::endl;
    }
}

