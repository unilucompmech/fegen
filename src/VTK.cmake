message( "External project - VTK" )

find_package(Git)
if(NOT GIT_FOUND)
  message(ERROR "Cannot find git. git is required for Superbuild")
endif()

option( USE_GIT_PROTOCOL "If behind a firewall turn this off to use http instead." ON)

set(git_protocol "git")
if(NOT USE_GIT_PROTOCOL)
  set(git_protocol "http")
endif()

set( VTK_DEPENDENCIES )

if( ${USE_SYSTEM_HDF5} MATCHES "OFF" )
  set( VTK_DEPENDENCIES HDF5 )
endif()

set( _vtkOptions )
if( APPLE )
  set( _vtkOptions -DVTK_REQUIRED_OBJCXX_FLAGS:STRING="" )
endif()

find_package(OpenGL)

ExternalProject_Add(VTK
    PREFIX ${CMAKE_CURRENT_BINARY_DIR}/vtk
#    GIT_REPOSITORY ${git_protocol}://vtk.org/VTK.git
    URL "http://www.vtk.org/files/release/6.2/VTK-6.2.0.tar.gz"
    DOWNLOAD_DIR ${CMAKE_CURRENT_BINARY_DIR}/vtk/download
    BUILD_IN_SOURCE
    BUILD_COMMAND cmake --build .
    INSTALL_COMMAND make install
    UPDATE_COMMAND ""
    PATCH_COMMAND ""
    CMAKE_GENERATOR ${EP_CMAKE_GENERATOR}
    CMAKE_ARGS
        ${ep_common_args}
        ${_vtkOptions}
        ${cmake_hdf5_libs}
        -DBUILD_EXAMPLES:BOOL=OFF
        -DBUILD_SHARED_LIBS:BOOL=ON
        -DBUILD_TESTING:BOOL=OFF
        -DCMAKE_BUILD_TYPE:STRING=${BUILD_TYPE}
        -DVTK_BUILD_ALL_MODULES:BOOL=OFF
        -DVTK_USE_SYSTEM_HDF5:BOOL=ON
        -DHDF5_DIR:PATH=${HDF5_DIR}
        -DCMAKE_INSTALL_PREFIX:PATH=${INSTALL_DEPENDENCIES_DIR}
        -DCMAKE_C_FLAGS=-DGLX_GLXEXT_LEGACY
        -DCMAKE_CXX_FLAGS=-DGLX_GLXEXT_LEGACY
)

ExternalProject_Get_Property(VTK install_dir binary_dir)

##link_directories(${binary_dir}/lib)

add_library(libvtk STATIC IMPORTED)
add_dependencies(libvtk VTK)
set_property(TARGET libvtk PROPERTY IMPORTED_LOCATION ${binary_dir}/lib/)


set( VTK_DIR ${binary_dir} )
message("VTK directory: " ${VTK_DIR})
#find_package(VTK REQUIRED)
# message("VTK use file: " ${VTK_USE_FILE})
# include(${VTK_USE_FILE})
