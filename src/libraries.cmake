option( USE_SYSTEM_CGAL "Use system libraries for CGAL" ON )
option( USE_SYSTEM_VTK "Use system libraries for VTK" ON )
option( USE_SYSTEM_QGL "Use system libraries for QGLViewer" OFF )

include(ExternalProject)

##################################################################################
# OpenGL
##################################################################################
find_package(OpenGL REQUIRED)
message(OpenGL found:${OPENGL_GLU_FOUND})
message(GLU found:${OpenGL_FOUND})

##################################################################################
# OpenGL Qt
##################################################################################
find_package(Qt5OpenGL REQUIRED)
message(Qt OpenGL found:${Qt5OpenGL_FOUND})

##################################################################################
# CGAL Library
##################################################################################
if( ${USE_SYSTEM_CGAL} MATCHES "OFF" )
    message("CGAL external project is used")
    include(${PROJECT_SOURCE_DIR}/src/CGAL.cmake)
else()
    message("CGAL system library is used")
    find_package( CGAL REQUIRED Core ImageIO CGAL_Qt5)
    include(${CGAL_USE_FILE})
endif()
add_definitions(-DCGAL_MESH_3_VERBOSE)

##################################################################################
# VTK
##################################################################################
if( ${USE_SYSTEM_VTK} MATCHES "OFF" )
    message("VTK external project is used")
    include(${PROJECT_SOURCE_DIR}/src/VTK.cmake)
else()
    set(dashboard_cache "
      VTK_Group_Imaging:BOOL=ON
      VTK_Group_Qt:BOOL=OFF
      VTK_Group_Tk:BOOL=ON
      VTK_Group_Views:BOOL=ON
      VTK_WRAP_JAVA:BOOL=OFF
      VTK_WRAP_PYTHON:BOOL=ON
      VTK_WRAP_TCL:BOOL=ON
      CMAKE_CXX_FLAGS:STRING=-std=c++11
    ")

    message("VTK system library is used")
    find_package( VTK REQUIRED )
    include( ${VTK_USE_FILE} )
#    message(${VTK_LIBRARIES})
endif()
    
##################################################################################
# Nifti
##################################################################################
# Add source directory to CMAKE_MODULE_PATH to find FindNIFTI.cmake
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/src/CMake")
find_package(NIFTI REQUIRED)
if (NIFTI_FOUND)
    include_directories(${NIFTI_INCLUDE_DIR})
endif()

##################################################################################
# QGLViewer
##################################################################################
#if( ${USE_SYSTEM_QGL} MATCHES "OFF" )
#    message("QGLViewer external project is used")
#    include(src/QGLViewer.cmake)
#else()
#    message("QGLViewer system library is used")
#    find_package( QGLViewer REQUIRED )
#    include_directories(${QGLVIEWER_INCLUDE_DIR})
#endif()


##################################################################################
# Zlib
##################################################################################
#find_package(ZLIB)
#if (ZLIB_FOUND)
#    include_directories(${ZLIB_INCLUDE_DIRS})
#    message(STATUS "Zlib is found in ${ZLIB_INCLUDE_DIRS}")
#endif()
