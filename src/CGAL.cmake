message( "External project - CGAL" )


ExternalProject_Add( CGAL
    PREFIX ${CMAKE_CURRENT_BINARY_DIR}/cgal
    GIT_REPOSITORY "https://github.com/CGAL/cgal.git"
    BUILD_IN_SOURCE
    BUILD_COMMAND cmake --build .
    INSTALL_COMMAND make install
    UPDATE_COMMAND ""
)

ExternalProject_Get_Property(CGAL install_dir binary_dir)

add_library(libcgal SHARED IMPORTED)
add_dependencies(libcgal CGAL)
set_property(TARGET libcgal PROPERTY IMPORTED_LOCATION
    ${install_dir}/src/CGAL-build/lib/libCGAL${CMAKE_SHARED_LIBRARY_SUFFIX}
)

add_library(libcgal_core SHARED IMPORTED)
add_dependencies(libcgal_core CGAL)
set_property(TARGET libcgal_core PROPERTY IMPORTED_LOCATION
    ${install_dir}/src/CGAL-build/lib/libCGAL_Core${CMAKE_SHARED_LIBRARY_SUFFIX}
)

add_library(libcgal_qt SHARED IMPORTED)
add_dependencies(libcgal_qt CGAL)
set_property(TARGET libcgal_qt PROPERTY IMPORTED_LOCATION
    ${install_dir}/src/CGAL-build/lib/libCGAL_Qt4${CMAKE_SHARED_LIBRARY_SUFFIX}
)

add_library(libcgal_imageio SHARED IMPORTED)
add_dependencies(libcgal_imageio CGAL)
set_property(TARGET libcgal_imageio PROPERTY IMPORTED_LOCATION
    ${install_dir}/src/CGAL-build/lib/libCGAL_ImageIO${CMAKE_SHARED_LIBRARY_SUFFIX}
)



