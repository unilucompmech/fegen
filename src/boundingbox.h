#ifndef BOUNDINGBOX_H
#define BOUNDINGBOX_H

#include <sstream>

#include "point3d.h"
#include "transform.h"


//! Bounding box of a mesh
template<class T = double>
class BoundingBox
{
    Point3D<T> min;
    Point3D<T> max;

public:

    BoundingBox(Point3D<T> _min, Point3D<T> _max) : min(_min), max(_max)
    {
        checkOrder();
    }
    BoundingBox(const BoundingBox<T>& bbox) : min(bbox.getMin()), max(bbox.getMax()) {}

    Point3D<T> getMin() const {return min;}
    void setMin(const Point3D<T> &value){min = value;checkOrder();}
    Point3D<T> getMax() const {return max;}
    void setMax(const Point3D<T> &value){max = value;checkOrder();}

    T getDiagonal()
    {
        return min.distance(max);
    }

    bool isPlaneXIn(T xValue) const
    {
        return xValue >= min.getX() && xValue <= max.getX();
    }
    bool isPlaneYIn(T yValue) const
    {
        return yValue >= min.getY() && yValue <= max.getY();
    }
    bool isPlaneZIn(T zValue) const
    {
        return zValue >= min.getZ() && zValue <= max.getZ();
    }

    bool isIn(Point3D<T> p) const
    {
        return isPlaneXIn(p.getX()) && isPlaneYIn(p.getY()) && isPlaneZIn(p.getZ());
    }

    BoundingBox<T> transform(Transform t) const
    {
        Point3D<T> _min = t.transform(min);
        Point3D<T> _max = t.transform(max);

        return BoundingBox<T>(_min, _max);

    }

    void applyTransform(Transform t)
    {
        min = t.transform(min);
        max = t.transform(max);

        checkOrder();
    }

    void checkOrder()
    {
        Point3D<T> n_min(min);
        Point3D<T> n_max(max);
        for (unsigned int i = 0 ; i < 3; i++)
        {
            if (n_min[i] > max[i]) n_min[i] = max[i];
            if (n_max[i] < min[i]) n_max[i] = min[i];
        }

        min = n_min;
        max = n_max;
    }


    std::string print()
    {
        std::ostringstream ss;
        ss << "[min(";
        ss << this->getMin().getX();
        ss << ",";
        ss << this->getMin().getY();
        ss << ",";
        ss << this->getMin().getZ();
        ss << "),max(";
        ss << this->getMax().getX();
        ss << ",";
        ss << this->getMax().getY();
        ss << ",";
        ss << this->getMax().getZ();
        ss << ")]";

        return ss.str();
    }
};




#endif // BOUNDINGBOX_H
