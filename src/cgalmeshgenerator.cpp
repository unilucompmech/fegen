#include "cgalmeshgenerator.h"

CGALMeshGenerator::CGALMeshGenerator()
{
}

PolygonMesh<Tetrahedron3D> CGALMeshGenerator::getTetrahedronMesh()
{
    return outputMesh;
}

Transform CGALMeshGenerator::getTransform() const
{
    return transform;
}

void CGALMeshGenerator::convertToInternalMesh()
{
    clock_t t = clock();

    outputMesh.clear();
    
    const Tr& tr = c3t3.triangulation();
    
    std::map<Tr::Vertex_handle, int> Vnbe;

    for( Cell_iterator cit = c3t3.cells_begin() ; cit != c3t3.cells_end() ; ++cit )
    {
        for (int i=0; i<4; i++)
            ++Vnbe[cit->vertex(i)];
    }

    std::map<Tr::Vertex_handle, int> V;
    int notconnected = 0;
    int inum = 0;
    for( Finite_vertices_iterator vit = tr.finite_vertices_begin(); vit != tr.finite_vertices_end(); ++vit)
    {
        Tr::Point pointCgal = vit->point();
        Point3D<> point(CGAL::to_double(pointCgal.x()), CGAL::to_double(pointCgal.y()), CGAL::to_double(pointCgal.z()));

        //if a transformation is defined (for images for example)
        point.transform(this->transform);

        if (Vnbe.find(vit) == Vnbe.end() || Vnbe[vit] <= 0)
        {
            ++notconnected;
        }
        else
        {
            V[vit] = inum++;
            outputMesh.addPoint(point);
        }
    }

    if (notconnected > 0) std::cerr << notconnected << " points are not connected to the mesh." << std::endl;


    clock_t addCellTime = clock();
    int progress = 0;
    bool log = false;
    for( Cell_iterator cit = c3t3.cells_begin() ; cit != c3t3.cells_end() ; ++cit )
    {
        MeshCellType tetra;
        for (int i = 0; i < tetra.getSize(); i++)
            tetra.setPoint(i,V[cit->vertex(i)]);
        outputMesh.addCell(tetra);

        progress++;
        if ( (float)(clock()-addCellTime)/CLOCKS_PER_SEC > 1)
        {
            log = true;
            std::cout
                    << "Computing mesh data structure: "
                    << (int)(100.0*((float)progress/(float)c3t3.number_of_cells()))
                    << "%" <<std::endl;
            addCellTime = clock();
        }
    }
    if (log) std::cout<< "Computing mesh data structure: "<< 100 << "%" <<std::endl;

    t = clock()-t;
    std::cout << "The mesh conversion lasted " << ((float)t)/CLOCKS_PER_SEC << " seconds" << std::endl;
}

void CGALMeshGenerator::updateLabels(MeshGenerationParameters *parameters)
{
    if (!parameters) return;

    //clear the labels in the mesh
    outputMesh.clearLabels();

    std::cout << "Nb domains " << parameters->labelDomains.size() << std::endl;

    //loop over the labels
    for(const auto& d : parameters->labelDomains)
    {
        if (!d.getApplyToPoints() &&  !d.getApplyToFacets() && !d.getApplyToCells()) continue;

        std::vector<bool> isInVector;

        std::cout << "Domain apply to points " << d.getApplyToPoints() << std::endl;
        //loop over the mesh vertices
        for (auto& v : outputMesh.getPositions())
        {
//            v.clearLabels();
            bool isIn = d.isIn(v);//is the current vertex in the domain defined by the label?
            isInVector.push_back(isIn);//used after for determining if a cell is in the domain
            if (isIn && d.getApplyToPoints())
            {
                outputMesh.addLabel(v, d.getLabelValue());//add this label to the vertex
            }
        }

        std::cout << "Domain apply to facets " << d.getApplyToFacets() << std::endl;
        //loop over the mesh facets
        if (d.getApplyToFacets())
        {
            for (auto& c : outputMesh.getFacets())
            {
//                c.clearLabels();

                bool isIn = true; //is the cell in the domain?
                for (const auto& p : c.indices)
                {
                    if (isInVector[p] == false)
                    {
                        isIn = false;
                        break;
                    }
                }
                if (isIn)
                    outputMesh.addLabel(c, d.getLabelValue());//add this label to the facet

            }
        }

        std::cout << "Domain apply to cells " << d.getApplyToCells() << std::endl;
        if (d.getApplyToCells())
        {
            //loop over the mesh cells
            for (auto& c : outputMesh.getCells())
            {
//                c.clearLabels();

                bool isIn = true;
                for (const auto& p : c.indices)
                {
                    if (isInVector[p] == false)
                    {
                        isIn = false;
                        break;
                    }
                }
                if (isIn)
                    outputMesh.addLabel(c, d.getLabelValue());//add this label to the cell

            }
        }
    }

    std::cout << "Now there are " << outputMesh.getNbLabels() << " labels in the mesh" << std::endl;
    std::cout << outputMesh.getLabelsInfo() << std::endl;
}
