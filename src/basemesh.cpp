#include "basemesh.h"

template<class T>
void PolygonMesh<T>::readFromFile(FileReader *reader)
{
    MeshFileReader* m_reader = dynamic_cast<MeshFileReader*>(reader);

    if (!m_reader)
    {
        std::cerr << "Wrong type of file reader" << std::endl;
        std::cerr << "Cannot read file" << std::endl;
        return;
    }

//    positions = m_reader->getVertices();
    for (const auto& v : m_reader->getVertices()) this->positions.push_back(v);
    cells.clear();

    std::vector<MeshFileReader::Face > faces = m_reader->getCells();
    for (unsigned int i = 0; i < faces.size(); i++)
    {
        CellType p = CellType();
        for (unsigned int j = 0 ; j < std::min(p.getSize(), (unsigned int)faces[i].size()); j++)
        {
            p.setPoint(j, faces[i][j]);
        }
        cells.push_back(p);
    }

    std::cout << "A mesh has been loaded from " << m_reader->getFilename() << std::endl;
    std::cout << print() << std::endl;


}

template<class T>
BoundingBox<> PolygonMesh<T>::getBoundingBox() const
{
    double xMin = std::numeric_limits<double>::max();
    double yMin = std::numeric_limits<double>::max();
    double zMin = std::numeric_limits<double>::max();

    double xMax = std::numeric_limits<double>::min();
    double yMax = std::numeric_limits<double>::min();
    double zMax = std::numeric_limits<double>::min();

    for (const auto& p : positions)
    {
        if (p.getX() < xMin) xMin = p.getX();
        if (p.getY() < yMin) yMin = p.getY();
        if (p.getZ() < zMin) zMin = p.getZ();

        if (p.getX() > xMax) xMax = p.getX();
        if (p.getY() > yMax) yMax = p.getY();
        if (p.getZ() > zMax) zMax = p.getZ();
    }

    return BoundingBox<>(
                Point3D<>(xMin, yMin, zMin),
                Point3D<>(xMax, yMax, zMax));
}

template<>
void PolygonMesh<Triangle3D>::readFromFile(FileReader *reader)
{
    MeshFileReader* m_reader = dynamic_cast<MeshFileReader*>(reader);

    if (!m_reader)
    {
        std::cerr << "Wrong type of file reader" << std::endl;
        std::cerr << "Cannot read file" << std::endl;
        return;
    }

//    this->positions = m_reader->getVertices();
    for (const auto& v : m_reader->getVertices()) this->positions.push_back(v);

    MeshType meshType = m_reader->getMeshType();

    if (meshType == MeshType::QUADS || meshType == MeshType::QUADS_AND_TRIANGLES)
        std::cout << "Quads are converted to triangles for the computation" << std::endl;
    else if (meshType != MeshType::TRIANGLES)
        std::cerr << "Cannot handle primitive different from triangles and quads" << std::endl;

    this->cells.clear();

    for (const auto& f : m_reader->getCells())
    {
        if (f.size() == 3)
        {
            Triangle3D p = Triangle3D();
            for (unsigned int j = 0 ; j < 3; j++)
            {
                p.setPoint(j, f[j]);
            }
            this->cells.push_back(p);
        }
        else if (f.size() == 4)
        {
            Triangle3D p1 = Triangle3D();
            Triangle3D p2 = Triangle3D();

            p1.setPoint(0, f[0]);
            p1.setPoint(0, f[1]);
            p1.setPoint(0, f[2]);

            p2.setPoint(1, f[0]);
            p2.setPoint(1, f[2]);
            p2.setPoint(1, f[3]);

            this->cells.push_back(p1);
            this->cells.push_back(p2);
        }
        else
        {
            std::cerr << "Something's wrong" << std::endl;
        }
    }

    std::cout << "A mesh has been loaded from " << m_reader->getFilename() << std::endl;
    std::cout << this->print() << std::endl;
}



template class PolygonMesh<Triangle3D>;
template class PolygonMesh<Tetrahedron3D>;


