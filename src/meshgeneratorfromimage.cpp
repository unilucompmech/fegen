#include "meshgeneratorfromimage.h"



MeshGeneratorFromImage::MeshGeneratorFromImage()
{
}


MeshGeneratorFromImage::MeshGeneratorFromImage(FileReader *reader) : reader(reader)
{
    if (reader->getFileType() != FileType::IMAGE)
    {
        std::cerr << "This mesh generation method requires an image file." << std::endl;
    }

    if (ImageFileReader* img_read = dynamic_cast<ImageFileReader*>(reader))
    {
        transform = img_read->getImage()->getTransform();
    }
}

void MeshGeneratorFromImage::computeMesh(MeshGenerationParameters *parameters)
{
    if (SimpleImageFileReader* simg_read = dynamic_cast<SimpleImageFileReader*>(reader))
    {
        CGAL::Image_3 image3;

        if (ImageFileReader* img_read = dynamic_cast<ImageFileReader*>(reader))
        {
            #ifdef IMAGE_FILE_READER_USE_SMART_POINTER
            std::shared_ptr<BaseImage>
            #else
            BaseImage*
            #endif
            b_img = img_read->getImage();

            if (!b_img)
            {
                std::cerr << "No image data" << std::endl;
                return;
            }

            uint8_t *cgal_image_data = new uint8_t[b_img->voxelCount()];
            uint8_t *vptr= cgal_image_data;
            std::cout << "Fill data (" << b_img->getDimX() << "x" << b_img->getDimY() << "x" << b_img->getDimZ() << ")..." << std::endl;

            for(unsigned int k = 0; k < b_img->getDimZ(); k++)
            {
                for(unsigned int j = 0; j < b_img->getDimY(); j++)
                {
                    for(unsigned int i = 0; i < b_img->getDimX(); i++)
                    {
    //                    std::cout << "Trying to access voxel (" << i << ", " << j << ", " << k << ")" << std::endl;
                        switch(b_img->getDatatype())
                        {
                        case ImageType::UNSIGNED_CHAR:
                            #ifdef IMAGE_FILE_READER_USE_SMART_POINTER
                            if (std::shared_ptr<Image<unsigned char> > img = std::dynamic_pointer_cast<Image<unsigned char> >(b_img))
                            #else
                            if (Image<unsigned char>* img = dynamic_cast<Image<unsigned char>*>(b_img) )
                            #endif
                            {
                                unsigned char vox = img->getVoxel(i,j,k);
                                unsigned int index = k*b_img->getDimX()*b_img->getDimY()
                                        +j*b_img->getDimX()
                                        +i;
                                if (index >= b_img->voxelCount()) std::cerr << "Trying to access voxel (" << i << ", " << j << ", " << k << ")/(index = " << index << ") while data size is " << b_img->voxelCount() << std::endl;
                                vptr[index] = vox;
                            }
                            break;
                        }

                    }
                }
            }

            std::cout << "Init image..." << std::endl;
            _image* vrnimage = _initImage();
            vrnimage->vectMode = VM_SCALAR;
            vrnimage->xdim = b_img->getDimX();
            vrnimage->ydim = b_img->getDimY();
            vrnimage->zdim = b_img->getDimZ();
            vrnimage->vdim = 1; //vectorial dimension
//            //voxel size
//            vrnimage->vx = b_img->getVoxelSizeX();
//            vrnimage->vy = b_img->getVoxelSizeY();
//            vrnimage->vz = b_img->getVoxelSizeZ();
//            //image translation
//            vrnimage->tx = b_img->getTransform().getTranslation()[0];
//            vrnimage->ty = b_img->getTransform().getTranslation()[1];
//            vrnimage->tz = b_img->getTransform().getTranslation()[2];
//            //image rotation
//            vrnimage->rx = b_img->getTransform().getRotation().toEuler()[0];
//            vrnimage->ry = b_img->getTransform().getRotation().toEuler()[1];
//            vrnimage->rz = b_img->getTransform().getRotation().toEuler()[2];
            vrnimage->endianness = END_LITTLE;
            vrnimage->wdim = 1;
            vrnimage->wordKind = WK_FIXED;
            vrnimage->sign = SGN_UNSIGNED;
            vrnimage->data = ImageIO_alloc(b_img->voxelCount());
            std::cout << "Copy data..." << std::endl;
            memcpy(vrnimage->data,(void*)cgal_image_data,b_img->voxelCount());

            image3 = CGAL::Image_3(vrnimage);

        }
        else
        {
            image3.read(simg_read->getFilename().c_str());
        }

        // Create domain
        std::cout << "Create domain..." << std::endl;
        Mesh_domain_from_image domain(image3);

        double facet_angle = 25;
        double facet_size = 0.15;
        double facet_distance = 0.008;
        double cell_radius_edge_ratio = 3;
        double cell_size = 0.15;
        bool lloyd_smoothing = false;
        unsigned int lloyd_max_iteration = 200;
        bool odt_smoothing = false;
        unsigned int odt_max_iteration = 200;
        bool perturb = false;
        double perturb_max_time = 30.0;
        bool exude = false;
        double exude_max_time = 30.0;

        if (CGALParameters* param = dynamic_cast<CGALParameters*>(parameters))
        {
            facet_angle             = param->facet_angle;
            facet_size              = param->facet_size;
            facet_distance          = param->facet_approximation;
            cell_radius_edge_ratio  = param->cell_radius_edge_ratio;
            cell_size               = param->cell_size;
            lloyd_smoothing         = param->lloyd_smoothing;
            lloyd_max_iteration     = param->lloyd_max_iteration;
            odt_smoothing           = param->odt_smoothing;
            odt_max_iteration       = param->odt_max_iteration;
            perturb                 = param->perturb;
            perturb_max_time        = param->perturb_max_time;
            exude                   = param->exude;
            exude_max_time          = param->exude_max_time;

        }
        else
        {
            std::cout << "Wrong parameters type" << std::endl;
        }

        // Mesh criteria (no cell_size set)
        Mesh_criteria criteria(CGAL::parameters::facet_angle=facet_angle
                , CGAL::parameters::facet_size=facet_size
                , CGAL::parameters::facet_distance=facet_distance
                , CGAL::parameters::cell_radius_edge_ratio=cell_radius_edge_ratio
                , CGAL::parameters::cell_size=cell_size);

        // Mesh generation
        std::cout << "Make mesh..." << std::endl;
        c3t3 = CGAL::make_mesh_3<C3t3>(domain, criteria, CGAL::parameters::no_perturb(), CGAL::parameters::no_exude());

        if (lloyd_smoothing)
        {
            std::cout << "Lloyd smoothing..." << std::endl;
            CGAL::lloyd_optimize_mesh_3(c3t3, domain, CGAL::parameters::max_iteration_number=lloyd_max_iteration);
        }
        if (odt_smoothing)
        {
            std::cout << "ODT smoothing..." << std::endl;
            CGAL::odt_optimize_mesh_3(c3t3, domain, CGAL::parameters::max_iteration_number=odt_max_iteration);
        }
        if (perturb)
        {
            std::cout << "Perturb..." << std::endl;
            CGAL::perturb_mesh_3(c3t3, domain, CGAL::parameters::time_limit=perturb_max_time);
        }

        if (exude)
        {
            CGAL::exude_mesh_3(c3t3, CGAL::parameters::time_limit=exude_max_time);
            std::cout << "Exude..." << std::endl;
        }


        std::cout << "Mesh conversion..." << std::endl;
        this->convertToInternalMesh();

        std::cout << "Computed mesh:  " << outputMesh << std::endl;
    }
}

void MeshGeneratorFromImage::init()
{

}


