#ifndef MESHGENERATORFROMPOLYHEDRA_H
#define MESHGENERATORFROMPOLYHEDRA_H

#include "cgalmeshgenerator.h"
#include "polygon3d.h"

//! Generate a mesh from a surface mesh
class MeshGeneratorFromPolyhedra : public CGALMeshGenerator
{
public:
    MeshGeneratorFromPolyhedra(PolygonMesh<Triangle3D> *mesh);

    void computeMesh(MeshGenerationParameters* parameters) override;
    void init() override;

protected:
    PolygonMesh<Triangle3D>* inputMesh;
};

// A modifier creating a triangle with the incremental builder.
template<class HDS>
class polyhedron_builder : public CGAL::Modifier_base<HDS> {
public:
    PolygonMesh<Triangle3D>* mesh;

    polyhedron_builder( PolygonMesh<Triangle3D>* mesh ) : mesh(mesh) {}

    void operator()( HDS& hds)
    {
        typedef typename HDS::Vertex   Vertex;
        typedef typename Vertex::Point Point;

        std::cout << *mesh << " will be converted to CGAL data structure" << std::endl;

        if (mesh->getNbPoints()==0 || mesh->getNbCells()==0)
        {
            if (mesh->getNbPoints()==0)
            {
                std::cout << *mesh << " has 0 nodes" << std::endl;
            }
            if (mesh->getNbCells()==0)
            {
                std::cout << *mesh << " has 0 polygons" << std::endl;
            }
            return;
        }

        // create a cgal incremental builder
        CGAL::Polyhedron_incremental_builder_3<HDS> B( hds, true);
        B.begin_surface( mesh->getNbPoints(), mesh->getNbCells() );

        // add the polyhedron vertices
        for( int i=0; i<(int)mesh->getNbPoints(); i++ ){
//            std::cout << "Get point "<< i << std::endl;
            B.add_vertex(
                        Point( mesh->getPosition(i).getX(),
                             mesh->getPosition(i).getY(),
                             mesh->getPosition(i).getZ()
                             ) );
        }

        // add the polyhedron triangles
        for( int i=0; i<(int)mesh->getNbCells(); i++ ){
            B.begin_facet();

//            std::cout << "Get polygon "<< i << std::endl;
            B.add_vertex_to_facet( mesh->getCell(i).getIndex(0) );
            B.add_vertex_to_facet( mesh->getCell(i).getIndex(1) );
            B.add_vertex_to_facet( mesh->getCell(i).getIndex(2) );
            B.end_facet();
        }

        // finish up the surface
        B.end_surface();

        std::cout << "Finish compute surface" << std::endl;
    }
};

#endif // MESHGENERATORFROMPOLYHEDRA_H
