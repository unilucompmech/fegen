TEMPLATE = app

QT += qml quick widgets

# Default rules for deployment.
include(deployment.pri)

RESOURCES += qml.qrc

SOURCES += main.cpp \
    mainwindow.cpp \
    meshgenerator.cpp \
    filereader.cpp \
    objreader.cpp \
    basemesh.cpp \
    point3d.cpp \
    polygon3d.cpp \
    meshgeneratorfrompolyhedra.cpp


FORMS += \
    mainwindow.ui

HEADERS += \
    mainwindow.h \
    meshgenerator.h \
    filereader.h \
    objreader.h \
    basemesh.h \
    point3d.h \
    polygon3d.h \
    meshgeneratorfrompolyhedra.h
