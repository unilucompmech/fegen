#include "dolfinexporter.h"

#include <fstream>

DolfinExporter::DolfinExporter()
{
}

void DolfinExporter::exportToFile(BaseMesh *mesh, std::string filename, Transform t)
{
    if (PolygonMesh<Triangle3D>* m = dynamic_cast<PolygonMesh<Triangle3D>* >(mesh))
    {
        this->exportToFile(m, filename, t);
    }
    else if (PolygonMesh<Tetrahedron3D>* m = dynamic_cast<PolygonMesh<Tetrahedron3D>* >(mesh))
    {
        this->exportToFile(m, filename, t);
    }
    else
    {
        std::cout << "This type of mesh cannot be exported in XML format" << std::endl;
    }
}

void DolfinExporter::exportToFile(PolygonMesh<Tetrahedron3D>* mesh, std::string filename, Transform t)
{
    std::cout << "About to write " << *mesh << " to " << filename << std::endl;

    std::ofstream* outfile = new std::ofstream(filename.c_str());

    if( !outfile->is_open() )
    {
        std::cerr << "Error creating file "<< filename << std::endl;
        delete outfile;
        outfile = NULL;
        return;
    }

    //Write header
    *outfile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << std::endl;

    //write dolfin node
    *outfile << "<dolfin xmlns:dolfin=\"http://www.fenicsproject.org\">" << std::endl;

    if (mesh->getNbPoints() && mesh->getNbCells())
    {
        *outfile << "\t<mesh celltype=\"tetrahedron\" dim=\"3\">" << std::endl;

        //vertices
        *outfile << "\t\t<vertices size=\""<< mesh->getNbPoints() << "\">" << std::endl;
        for (int i=0 ; i < mesh->getNbPoints(); i++)
        {
            Point3D<> p = mesh->getPosition(i);
            p.transform(t);
            *outfile << "\t\t\t<vertex index=\"" << i << "\" x=\"" << p.getX() << "\" y=\"" << p.getY() << "\" z=\"" << p.getZ() << "\"/>" << std::endl;
        }
        *outfile << "\t\t</vertices>" << std::endl;

        //tetra
        *outfile << "\t\t<cells size=\"" << mesh->getNbCells() << "\">"  <<std::endl;
        for (int i = 0 ; i < mesh->getNbCells() ; i++)
        {
            PolygonMesh<Tetrahedron3D>::CellType t = mesh->getCell(i);
            *outfile << "\t\t\t<tetrahedron index=\"" << i << "\" ";
            for (unsigned int j = 0 ; j < t.getSize(); j++)
                *outfile << "v" << j << "=\"" << t.getIndex(j) << "\" ";
            *outfile << "/>" << std::endl;
        }
        *outfile << "\t\t</cells>" << std::endl;

        *outfile << std::endl;
    }

    if (mesh->getNbLabels())
    {
        for (const auto& l : mesh->getLabels())
        {
            if (mesh->getNbFacetsWithLabel(l))
            {
                *outfile << "\t\t<domains>" << std::endl;
                *outfile << "\t\t\t<mesh_value_collection type=\"uint\" dim=\"2\" size=\"" << mesh->getNbCells()*4 << "\">" << std::endl;

                unsigned int i = 0;
                for (const auto& tet : mesh->getCells())
                {
                    unsigned int j = 0;
                    for (const auto& tri : tet.indicesOtherCells)
                    {
                         *outfile << "\t\t\t\t<value cell_index=\"" << i << "\" " << "local_entity=\"" << j << "\" " << "value=\"" << mesh->getFacet(tri).getLabel(0) << "\"/>" << std::endl;
                        j++;
                    }
                    i++;
                }

                *outfile << "\t\t\t</mesh_value_collection>" << std::endl;
                *outfile << "\t\t</domains>" << std::endl;
            }
        }
    }

    *outfile << "\t</mesh>" << std::endl;
    *outfile << "</dolfin>" << std::endl;


    outfile->close();
    std::cerr << "Successfully write "<< *mesh << " to " << filename << std::endl;

}

void DolfinExporter::exportToFile(PolygonMesh<Triangle3D> *mesh, std::string filename, Transform t)
{
    std::cout << "About to write " << *mesh << " to " << filename << std::endl;

    std::ofstream* outfile = new std::ofstream(filename.c_str());

    if( !outfile->is_open() )
    {
        std::cerr << "Error creating file "<< filename << std::endl;
        delete outfile;
        outfile = NULL;
        return;
    }

    //Write header
    *outfile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << std::endl;

    //write dolfin node
    *outfile << "<dolfin xmlns:dolfin=\"http://www.fenicsproject.org\">" << std::endl;

    if (mesh->getNbPoints() && mesh->getNbCells())
    {
        *outfile << "\t<mesh celltype=\"triangle\" dim=\"3\">" << std::endl;

        //vertices
        *outfile << "\t\t<vertices size=\""<< mesh->getNbPoints() << "\">" << std::endl;
        for (int i=0 ; i < mesh->getNbPoints(); i++)
        {
            Point3D<> p = mesh->getPosition(i);
            p.transform(t);
            *outfile << "\t\t\t<vertex index=\"" << i << "\" x=\"" << p.getX() << "\" y=\"" << p.getY() << "\" z=\"" << p.getZ() << "\"/>" << std::endl;
        }
        *outfile << "\t\t</vertices>" << std::endl;

        //tetra
        *outfile << "\t\t<cells size=\"" << mesh->getNbCells() << "\">"  <<std::endl;
        for (int i = 0 ; i < mesh->getNbCells() ; i++)
        {
            PolygonMesh<Triangle3D>::CellType t = mesh->getCell(i);
            *outfile << "\t\t\t<triangle index=\"" << i << "\" ";
            for (unsigned int j = 0 ; j < t.getSize(); j++)
                *outfile << "v" << j << "=\"" << t.getIndex(j) << "\" ";
            *outfile << "/>" << std::endl;
        }
        *outfile << "\t\t</cells>" << std::endl;

        *outfile << std::endl;
    }

    if (mesh->getNbLabels())
    {
        for (const auto& l : mesh->getLabels())
        {
            if (mesh->getNbFacetsWithLabel(l))
            {
                *outfile << "\t\t<domains>" << std::endl;
                *outfile << "\t\t\t<mesh_value_collection type=\"uint\" dim=\"2\" size=\"" << mesh->getNbCells()*4 << "\">" << std::endl;

                unsigned int i = 0;
                for (const auto& tet : mesh->getCells())
                {
                    unsigned int j = 0;
                    for (const auto& tri : tet.indicesOtherCells)
                    {
                         *outfile << "\t\t\t\t<value cell_index=\"" << i << "\" " << "local_entity=\"" << j << "\" " << "value=\"" << mesh->getFacet(tri).getLabel(0) << "\"/>" << std::endl;
                        j++;
                    }
                    i++;
                }

                *outfile << "\t\t\t</mesh_value_collection>" << std::endl;
                *outfile << "\t\t</domains>" << std::endl;
            }
        }
    }

    *outfile << "\t</mesh>" << std::endl;
    *outfile << "</dolfin>" << std::endl;


    outfile->close();
    std::cerr << "Successfully write "<< *mesh << " to " << filename << std::endl;
}
