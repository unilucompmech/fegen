#include "filereader.h"


FileReader::FileReader(std::string filename) : m_filename(filename)
{
}

void FileReader::setFilename(std::string filename)
{
    m_filename = filename;
}

std::string FileReader::getFilename()
{
    return m_filename;
}


