#ifndef COPYMESHGENERATOR_H
#define COPYMESHGENERATOR_H

#include "meshgenerator.h"
#include "basemesh.h"

class CopyMeshGenerator : public MeshGenerator
{
public:
    CopyMeshGenerator();
    CopyMeshGenerator(BaseMesh *mesh);

    void computeMesh(MeshGenerationParameters* parameters) override;
    void init() override;
    void updateLabels(MeshGenerationParameters *parameters) override;

    BaseMesh* getMesh(){return m_mesh;}
private:
    BaseMesh* m_mesh;
};


class CopyMeshGeneratorParameters : public MeshGenerationParameters
{

};

#endif // COPYMESHGENERATOR_H
