message( "External project - GMP" )

ExternalProject_Add(GMP
    PREFIX ${CMAKE_CURRENT_BINARY_DIR}/gmp
    URL "ftp://ftp.gnu.org/gnu/gmp/gmp-5.1.3.tar.gz"
    DOWNLOAD_DIR ${CMAKE_CURRENT_BINARY_DIR}/gmp/download
    SOURCE_DIR ${CMAKE_CURRENT_BINARY_DIR}/gmp/build
    BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR}/gmp/build
    INSTALL_DIR ${CMAKE_CURRENT_BINARY_DIR}/gmp/install
    CONFIGURE_COMMAND sh -c "CC=g++ CXX=g++ ./configure --prefix=<INSTALL_DIR>"
    BUILD_COMMAND make
    INSTALL_COMMAND make install
)

ExternalProject_Get_Property(GMP install_dir binary_dir)
message("GMP bin directory: " ${binary_dir})
message("GMP install directory: " ${install_dir})
