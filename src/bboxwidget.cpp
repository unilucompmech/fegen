#include "bboxwidget.h"

BBoxWidget::BBoxWidget(QWidget *parent) :
    QWidget(parent)
{
    labelLayout = new QVBoxLayout;
    this->setLayout(labelLayout);

    name = new QLabel("Label #");
    labelLayout->addWidget(name);


    labelFormLayout = new QFormLayout;

    QWidget* formWidget = new QWidget;
    formWidget->setLayout(labelFormLayout);
    labelLayout->addWidget(formWidget);

    //MIN

    slider_min_x = new QSlider(Qt::Horizontal);
    labelFormLayout->addRow(tr("&Min [X]"), slider_min_x);

    slider_min_y = new QSlider(Qt::Horizontal);
    labelFormLayout->addRow(tr("&Min [Y]"), slider_min_y);

    slider_min_z = new QSlider(Qt::Horizontal);
    labelFormLayout->addRow(tr("&Min [Z]"), slider_min_z);


    //MAX

    slider_max_x = new QSlider(Qt::Horizontal);
    labelFormLayout->addRow(tr("&Max [X]"), slider_max_x);

    slider_max_y = new QSlider(Qt::Horizontal);
    labelFormLayout->addRow(tr("&Max [Y]"), slider_max_y);

    slider_max_z = new QSlider(Qt::Horizontal);
    labelFormLayout->addRow(tr("&Max [Z]"), slider_max_z);


    labelValue = new QLineEdit;
    labelValue->setText(QString::number(1));
    labelFormLayout->addRow(tr("&Value"), labelValue);
    connect(labelValue, SIGNAL(textChanged(QString)), this, SLOT(updateMainWindowOutput()));


    group = new QButtonGroup(this);
    checkPoint = new QCheckBox(QString("Point"));
    checkFacet = new QCheckBox(QString("Facet"));
    checkCell  = new QCheckBox(QString("Cell") );

    QHBoxLayout* checkLayout = new QHBoxLayout;

    checkLayout->addWidget(checkPoint);
    checkLayout->addWidget(checkFacet);
    checkLayout->addWidget(checkCell);

    group->addButton(checkPoint);
    group->addButton(checkFacet);
    group->addButton(checkCell);

    group->setExclusive(false);
    checkPoint->setChecked(true);
    checkFacet->setChecked(true);
    checkCell->setChecked(true);

    connect(checkPoint, SIGNAL(stateChanged(int)), this, SLOT(updateMainWindowOutput()));
    connect(checkFacet, SIGNAL(stateChanged(int)), this, SLOT(updateMainWindowOutput()));
    connect(checkCell, SIGNAL(stateChanged(int)), this, SLOT(updateMainWindowOutput()));

    QWidget* checkWidget = new QWidget;
    checkWidget->setLayout(checkLayout);
    labelFormLayout->addRow(tr("&Type"), checkWidget);



    colorPickerButton = new QPushButton;
    colorPickerButton->setText("Select color");
    connect(colorPickerButton, SIGNAL(clicked()), this, SLOT(setColor()));
    labelFormLayout->addRow(tr("&Color"), colorPickerButton);

    bboxColor = QColor(255,170,0);


    if (MainWindow* m = dynamic_cast<MainWindow*>(parent))
    {
        mainWindow = m;
    }
}

void BBoxWidget::setName(QString n)
{
    name->setText(n);
}

void BBoxWidget::provideImageData(std::shared_ptr<BaseImage> image)
{
    if (image)
    {
        int dimX = image->getDimX();
        int dimY = image->getDimY();
        int dimZ = image->getDimZ();

        slider_min_x->setMaximum(0);
        slider_min_y->setMaximum(0);
        slider_min_z->setMaximum(0);

        slider_min_x->setMaximum(dimX);
        slider_min_y->setMaximum(dimY);
        slider_min_z->setMaximum(dimZ);

        slider_min_x->setValue(dimX/3);
        slider_min_y->setValue(dimY/3);
        slider_min_z->setValue(dimZ/3);


        updateSliderValueMinX(dimX/3);
        connect(slider_min_x, SIGNAL(valueChanged(int)), this, SLOT(updateSliderValueMinX(int)));
        connect(slider_min_x, SIGNAL(sliderReleased()), this, SLOT(updateMainWindowOutput()));

        updateSliderValueMinY(dimY/3);
        connect(slider_min_y, SIGNAL(valueChanged(int)), this, SLOT(updateSliderValueMinY(int)));
        connect(slider_min_y, SIGNAL(sliderReleased()), this, SLOT(updateMainWindowOutput()));

        updateSliderValueMinZ(dimZ/3);
        connect(slider_min_z, SIGNAL(valueChanged(int)), this, SLOT(updateSliderValueMinZ(int)));
        connect(slider_min_z, SIGNAL(sliderReleased()), this, SLOT(updateMainWindowOutput()));



        slider_max_x->setMaximum(0);
        slider_max_y->setMaximum(0);
        slider_max_z->setMaximum(0);

        slider_max_x->setMaximum(dimX);
        slider_max_y->setMaximum(dimY);
        slider_max_z->setMaximum(dimZ);

        slider_max_x->setValue(2*dimX/3);
        slider_max_y->setValue(2*dimY/3);
        slider_max_z->setValue(2*dimZ/3);

        updateSliderValueMaxX(2*dimX/3);
        connect(slider_max_x, SIGNAL(valueChanged(int)), this, SLOT(updateSliderValueMaxX(int)));
        connect(slider_max_x, SIGNAL(sliderReleased()), this, SLOT(updateMainWindowOutput()));

        updateSliderValueMaxY(2*dimY/3);
        connect(slider_max_y, SIGNAL(valueChanged(int)), this, SLOT(updateSliderValueMaxY(int)));
        connect(slider_max_y, SIGNAL(sliderReleased()), this, SLOT(updateMainWindowOutput()));

        updateSliderValueMaxZ(2*dimZ/3);
        connect(slider_max_z, SIGNAL(valueChanged(int)), this, SLOT(updateSliderValueMaxZ(int)));
        connect(slider_max_z, SIGNAL(sliderReleased()), this, SLOT(updateMainWindowOutput()));
    }
}

Point3D<> BBoxWidget::getMin()
{
    return Point3D<>(
                slider_min_x->value(),
                slider_min_y->value(),
                slider_min_z->value()
                );
}

Point3D<> BBoxWidget::getMax()
{
    return Point3D<>(
                slider_max_x->value(),
                slider_max_y->value(),
                slider_max_z->value()
                );
}

BoundingBox<> BBoxWidget::getBoundingBox()
{
    return BoundingBox<>(getMin(), getMax());
}

double BBoxWidget::getValue()
{
    QString txt = labelValue->text();
    return txt.toDouble();
}

QColor BBoxWidget::getColor()
{
    return bboxColor;
}

bool BBoxWidget::getCheckPoints() const
{
    if (checkPoint)
        return checkPoint->checkState() == Qt::Checked;
    return false;
}

bool BBoxWidget::getCheckFacets() const
{
    if (checkFacet)
        return checkFacet->checkState() == Qt::Checked;
    return false;
}

bool BBoxWidget::getCheckCells() const
{
    if (checkCell)
        return checkCell->checkState() == Qt::Checked;
    return false;
}

void BBoxWidget::updateSliderValueMinX(int value)
{
    QLabel* l = (QLabel*)labelFormLayout->labelForField(slider_min_x);
    l->setText(QString("Min [X] ") + QString::number(value));
    if (mainWindow) mainWindow->updateImagesView();
}

void BBoxWidget::updateSliderValueMinY(int value)
{
    QLabel* l = (QLabel*)labelFormLayout->labelForField(slider_min_y);
    l->setText(QString("Min [Y] ") + QString::number(value));
    if (mainWindow) mainWindow->updateImagesView();
}

void BBoxWidget::updateSliderValueMinZ(int value)
{
    QLabel* l = (QLabel*)labelFormLayout->labelForField(slider_min_z);
    l->setText(QString("Min [Z] ") + QString::number(value));
    if (mainWindow) mainWindow->updateImagesView();
}

void BBoxWidget::updateSliderValueMaxX(int value)
{
    QLabel* l = (QLabel*)labelFormLayout->labelForField(slider_max_x);
    l->setText(QString("Max [X] ") + QString::number(value));
    if (mainWindow) mainWindow->updateImagesView();
}

void BBoxWidget::updateSliderValueMaxY(int value)
{
    QLabel* l = (QLabel*)labelFormLayout->labelForField(slider_max_y);
    l->setText(QString("Max [Y] ") + QString::number(value));
    if (mainWindow) mainWindow->updateImagesView();
}

void BBoxWidget::updateSliderValueMaxZ(int value)
{
    QLabel* l = (QLabel*)labelFormLayout->labelForField(slider_max_z);
    l->setText(QString("Max [Z] ") + QString::number(value));
    if (mainWindow) mainWindow->updateImagesView();
}




void BBoxWidget::updateMainWindowOutput()
{
    if (mainWindow)
    {
        mainWindow->setLabelDomainsFromUI();
        mainWindow->updateUIFromOutput();
    }
}


void BBoxWidget::setColor()
{
    QColor color = QColorDialog::getColor(bboxColor, this);

    if (color.isValid())
    {
        colorPickerButton->setText(color.name());
        colorPickerButton->setPalette(QPalette(color));
        colorPickerButton->setAutoFillBackground(true);

        bboxColor = color;

        if (mainWindow) mainWindow->updateImagesView();
    }
}



void BBoxWidgetList::updateLabelNames()
{
    for (unsigned int i = 0; i < list.size(); i++)
        list[i]->setName(QString("Label #") + QString::number(i) + QString(" (bounding box)"));
}

void BBoxWidgetList::addWidget(QWidget *widget, int stretch, Qt::Alignment alignment)
{
    QVBoxLayout::addWidget(widget, stretch, alignment);
    if (BBoxWidget* bbox = dynamic_cast<BBoxWidget*>(widget))
        list.push_back(bbox);
}

std::vector<BoundingBox<> > BBoxWidgetList::getBoundingBoxes()
{
    std::vector<BoundingBox<> > boxes;

    for (const auto& l : list)
        boxes.push_back(l->getBoundingBox());

    return boxes;

}

BBoxWidget* BBoxWidgetList::getWidget(int i)
{
    assert(i < list.size());
    return list[i];
}
