#ifndef BASEIMAGE_H
#define BASEIMAGE_H

#include <cstddef>
#include <iostream>
#include <memory>

#include "transform.h"

#define BASE_IMAGE_USE_SMART_POINTER

enum class ImageType
{
    NONE,
    UNSIGNED_CHAR,
    SHORT,
    INT,
    FLOAT,
    DOUBLE
};

//!  Base class for 3D Image
class BaseImage
{
public:
    BaseImage();
    BaseImage(int dimX, int dimY, int dimZ);
    virtual ~BaseImage() = 0;

    void setDimX(int dimX){dim[0]=dimX;}
    void setDimY(int dimY){dim[1]=dimY;}
    void setDimZ(int dimZ){dim[2]=dimZ;}

    int getDimX(){return dim[0];}
    int getDimY(){return dim[1];}
    int getDimZ(){return dim[2];}

    void setVoxelSizeX(int voxSizeX){transform.getScale()[0] = voxSizeX;}
    void setVoxelSizeY(int voxSizeY){transform.getScale()[1] = voxSizeY;}
    void setVoxelSizeZ(int voxSizeZ){transform.getScale()[2] = voxSizeZ;}

    int getVoxelSizeX(){return transform.getScale()[0];}
    int getVoxelSizeY(){return transform.getScale()[1];}
    int getVoxelSizeZ(){return transform.getScale()[2];}

    int voxelCount(){return dim[0]*dim[1]*dim[2];}

    virtual ImageType getDatatype();

    Transform& getTransform();
    void setTransform(const Transform &value);

protected:
    int dim[3];

    ImageType datatype;

    Transform transform;

};

//!  Templated class for 3D Image
/*!
The template corresponds to the type of the data stored for each voxel
*/
template <class T>
class Image : public BaseImage
{
public:
    Image();
    Image(int dimX, int dimY, int dimZ);

    ImageType getDatatype() override;

    T  getVoxel(int x, int y, int z);
    T  getIntensityAtPoint(double x, double y, double z);

    void setVoxel(int x, int y, int z, T value);

    T getMin() const;
    T getMax() const;

protected:
#ifdef BASE_IMAGE_USE_SMART_POINTER
    std::shared_ptr<T> data;
#else
    T* data;
#endif
    void createData(int dimX, int dimY, int dimZ);


    T min;
    T max;
};



#endif // BASEIMAGE_H
