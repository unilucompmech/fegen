#include "objreader.h"


void ObjReader::readMesh()
{
    std::cout << "[ObjReader] Construction of the mesh from " << this->m_filename << std::endl;

    std::ifstream file(this->m_filename.c_str(), std::ifstream::in);
    std::string line;
    while( std::getline(file,line) )
    {
        if (line.empty()) continue;

        std::istringstream values(line);
        std::string token;

        values >> token;

        if (token == "#")
        {
            /* comment */
        }
        else if (token == "v")
        {
            double a,b,c;
            values >> a >> b >> c;
            Point3D<> point(a,b,c);
            vertices.push_back(point);
//            std::cout << "Add point " << point << std::endl;
        }
        else if (token == "l" || token == "f")
        {
            std::vector<unsigned int> f;
            while (!values.eof())
            {
                std::string face;
                values >> face;
//                std::cout << face << std::endl;

                if (face.empty()) continue;

                std::string::size_type pos = 0;
//                while (pos != std::string::npos)
                {
                     pos = face.find('/');
                     std::string id = face.substr(0, pos);
//                     std::cout << id << std::endl;
                     if (!id.empty()) f.push_back(atoi(id.c_str()) -1);
//                     face = face.substr(pos + 1);
                }

            }
            cells.push_back(f);
        }
    }

    std::cout << "[ObjReader] File " << this->m_filename << " has been loaded and contains " << this->getNbVertices() << " vertices and " << this->getNbCells() << " cells" << std::endl;

//    std::cout << faces << std::endl;

//    for (unsigned int i = 0; i < mesh.getNbPoints(); i++)
//    {
//        std::cout << "vertex " << i << ": " << mesh.getPosition(i) << std::endl;
//    }

//    for (unsigned int i = 0; i < faces.size(); i++)
//    {
//        std::cout << "polygon " << i << ": ";
//        for (unsigned int j = 0 ; j < faces[i].size(); j++)
//            std::cout << faces[i][j] << ", ";
//        std::cout << std::endl;
//    }

//    std::cout << mesh << " has been created from " << this->m_filename << std::endl;

}

