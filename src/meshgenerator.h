#ifndef MESHGENERATOR_H
#define MESHGENERATOR_H

#include <QtCore>

#include "basemesh.h"
#include "meshgenerationparameters.h"

//! Base class for the mesh generators
class MeshGenerator
{

public:
    MeshGenerator();

    virtual void computeMesh(MeshGenerationParameters* parameters) = 0;
    virtual void init() = 0;
    virtual void updateLabels(MeshGenerationParameters *parameters) = 0;
};

#endif // MESHGENERATOR_H
