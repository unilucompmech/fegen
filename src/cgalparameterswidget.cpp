#include "cgalparameterswidget.h"

CGALParametersWidget::CGALParametersWidget(QWidget *parent) :
    QWidget(parent)
{
    formLayout = new QFormLayout;
    this->setLayout(formLayout);

    cellSizeLine = new QLineEdit;
    cellSizeLine->setToolTip("This parameter controls the size of mesh tetrahedra. It is either a scalar or a spatially variable scalar field. It provides an upper bound on the circumradii of the mesh tetrahedra.");
    formLayout->addRow(tr("&Cell size"), cellSizeLine);

    cellRadiusEdgeRatioLine = new QLineEdit;
    cellRadiusEdgeRatioLine->setToolTip("This parameter controls the shape of mesh cells (but can't filter slivers, as we discussed earlier). Actually, it is an upper bound for the ratio between the circumradius of a mesh tetrahedron and its shortest edge. There is a theoretical bound for this parameter: the Delaunay refinement process is guaranteed to terminate for values of cell_radius_edge_ratio bigger than 2.");
    formLayout->addRow(tr("&Cell radius edge ratio"), cellRadiusEdgeRatioLine);

    facetSizeLine = new QLineEdit;
    facetSizeLine->setToolTip("This parameter controls the size of surface facets. Actually, each surface facet has a surface Delaunay ball which is a ball circumscribing the surface facet and centered on the surface patch. The parameter facet_size is either a constant or a spatially variable scalar field, providing an upper bound for the radii of surface Delaunay balls.");
    formLayout->addRow(tr("&Facet size"), facetSizeLine);

    facetAngleLine = new QLineEdit;
    facetAngleLine->setToolTip("This parameter controls the shape of surface facets. Actually, it is a lower bound for the angle (in degree) of surface facets. When boundary surfaces are smooth, the termination of the meshing process is guaranteed if the angular bound is at most 30 degrees.");
    formLayout->addRow(tr("&Facet angle"), facetAngleLine);

    facetDistanceLine = new QLineEdit;
    facetDistanceLine->setToolTip("This parameter controls the approximation error of boundary and subdivision surfaces. Actually, it is either a constant or a spatially variable scalar field. It provides an upper bound for the distance between the circumcenter of a surface facet and the center of a surface Delaunay ball of this facet.");
    formLayout->addRow(tr("&Facet distance"), facetDistanceLine);

    lloydSmoothing = new QCheckBox;
    formLayout->addRow(tr("&Lloyd smoothing"), lloydSmoothing);

    lloydMaxIt = new QLineEdit;
    formLayout->addRow(tr("&Lloyd max iterations"), lloydMaxIt);

    odtSmoothing = new QCheckBox;
    formLayout->addRow(tr("&ODT smoothing"), odtSmoothing);

    odtMaxIt = new QLineEdit;
    formLayout->addRow(tr("&ODT max iterations"), odtMaxIt);

    perturb = new QCheckBox;
    formLayout->addRow(tr("&Perturb"), perturb);

    perturbTimout = new QLineEdit;
    formLayout->addRow(tr("&Perturb timeout"), perturbTimout);

    exude = new QCheckBox;
    formLayout->addRow(tr("&Exude"), exude);

    exudeTimout = new QLineEdit;
    formLayout->addRow(tr("&Exude timeout"), exudeTimout);



    cellSizeLine           ->setText(QString::number(0.1  ));
    cellRadiusEdgeRatioLine->setText(QString::number(3.0  ));
    facetSizeLine          ->setText(QString::number(0.15 ));
    facetAngleLine         ->setText(QString::number(0.25 ));
    facetDistanceLine      ->setText(QString::number(0.008));
    lloydMaxIt             ->setText(QString::number(200));
    odtMaxIt               ->setText(QString::number(200));
    perturbTimout          ->setText(QString::number(30));
    exudeTimout            ->setText(QString::number(30));

    lloydSmoothing->setChecked(false);
    odtSmoothing  ->setChecked(false);
    perturb       ->setChecked(false);
    exude         ->setChecked(false);


}

double CGALParametersWidget::getCellSize()
{
    assert(cellSizeLine != nullptr);
    return cellSizeLine->text().toDouble();
}

double CGALParametersWidget::getCellRadiusEdgeRatio()
{
    assert(cellRadiusEdgeRatioLine != nullptr);
    return cellRadiusEdgeRatioLine->text().toDouble();
}

double CGALParametersWidget::getFacetSize()
{
    assert(facetSizeLine != nullptr);
    return facetSizeLine->text().toDouble();
}

double CGALParametersWidget::getFacetAngle()
{
    assert(facetAngleLine != nullptr);
    return facetAngleLine->text().toDouble();
}

double CGALParametersWidget::getFacetDistance()
{
    assert(facetDistanceLine != nullptr);
    return facetDistanceLine->text().toDouble();
}

bool CGALParametersWidget::getLloydSmoothing()
{
    assert(lloydSmoothing != nullptr);
    return lloydSmoothing->checkState()==Qt::Checked;
}

unsigned int CGALParametersWidget::getLloydMaxIterations()
{
    assert(lloydMaxIt != nullptr);
    return lloydMaxIt->text().toInt();
}

bool CGALParametersWidget::getODTSmoothing()
{
    assert(odtSmoothing != nullptr);
    return odtSmoothing->checkState()==Qt::Checked;
}

unsigned int CGALParametersWidget::getODTMaxIerations()
{
    assert(odtMaxIt != nullptr);
    return odtMaxIt->text().toInt();
}

bool CGALParametersWidget::getPerturb()
{
    assert(perturb != nullptr);
    return perturb->checkState()==Qt::Checked;
}

double CGALParametersWidget::getPerturbTimout()
{
    assert(perturbTimout != nullptr);
    return perturbTimout->text().toDouble();
}

bool CGALParametersWidget::getExude()
{
    assert(exude != nullptr);
    return exude->checkState()==Qt::Checked;
}

double CGALParametersWidget::getExudeTimout()
{
    assert(exudeTimout != nullptr);
    return exudeTimout->text().toDouble();
}
