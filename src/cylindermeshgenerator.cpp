#include "cylindermeshgenerator.h"

#include <math.h>
#define PI 3.14159265

CylinderMeshGenerator::CylinderMeshGenerator()
{

}

void CylinderMeshGenerator::computeMesh(MeshGenerationParameters *parameters)
{
    std::cout << "Generate cylinder with: " << std::endl;


    if (CylinderMeshGeneratorParameters* param = dynamic_cast<CylinderMeshGeneratorParameters*>(parameters))
    {
        std::cout << "Length: " << param->length << std::endl;
        std::cout << "Inner radius: " << param->innerRadius << std::endl;
        std::cout << "Outer radius: " << param->outerRadius << std::endl;
        std::cout << "Length subdivision: " << param->lengthSubdiv << std::endl;
        std::cout << "Radius subdivision: " << param->radiusSubdiv << std::endl;
        std::cout << "Angle subdivision: " << param->angleSubdiv << std::endl;

        insideIndices.clear();
        outsideIndices.clear();
        topIndices.clear();
        bottomIndices.clear();

        outputMesh.clear();

        for (unsigned int i = 0 ; i < param->lengthSubdiv+1; i++)
        {
            for (unsigned int j = 0; j < param->radiusSubdiv+1; j++)
            {
                for (unsigned int k = 0 ; k < param->angleSubdiv; k++)
                {
                    double angle = 2.0 * PI * (double)k / param->angleSubdiv;
                    double x = cos(angle) * (param->outerRadius - param->innerRadius) * (double)j / (double)param->radiusSubdiv + cos(angle) * param->innerRadius;
                    double y = sin(angle) * (param->outerRadius - param->innerRadius) * (double)j / (double)param->radiusSubdiv + sin(angle) * param->innerRadius;
                    double z = param->length * (double)i / (double)param->lengthSubdiv;
                    Point3D<> point(x,y,z);
                    outputMesh.addPoint(point);

                    if (i ==0)
                    {
                        bottomIndices.push_back(outputMesh.getNbPoints()-1);
                    }

                    if (i == param->lengthSubdiv)
                    {
                        topIndices.push_back(outputMesh.getNbPoints()-1);
                    }

                    if (j == 0)
                    {
                        insideIndices.push_back(outputMesh.getNbPoints()-1);
                    }

                    if (j == param->radiusSubdiv)
                    {
                        outsideIndices.push_back(outputMesh.getNbPoints()-1);
                    }
                }
            }

//            if (i == 0)
//            {

//            }
//            else
//            {

//            }
        }

        for (unsigned int i = 0 ; i < param->lengthSubdiv+1; i++)
        {
            for (unsigned int j = 0; j < param->radiusSubdiv+1; j++)
            {
                for (unsigned int k = 0 ; k < param->angleSubdiv; k++)
                {

                    if (j > 0 && i > 0)
                    {
                        unsigned int a = (k)%param->angleSubdiv                      + j     * param->angleSubdiv + i     * (param->radiusSubdiv+1) * param->angleSubdiv;
                        unsigned int b = (k-1+param->angleSubdiv)%param->angleSubdiv + j     * param->angleSubdiv + i     * (param->radiusSubdiv+1) * param->angleSubdiv;
                        unsigned int c = (k-1+param->angleSubdiv)%param->angleSubdiv + (j-1) * param->angleSubdiv + i     * (param->radiusSubdiv+1) * param->angleSubdiv;
                        unsigned int d = (k)%param->angleSubdiv                      + (j-1) * param->angleSubdiv + i     * (param->radiusSubdiv+1) * param->angleSubdiv;

                        unsigned int e = (k)%param->angleSubdiv                      + j     * param->angleSubdiv + (i-1) * (param->radiusSubdiv+1) * param->angleSubdiv;
                        unsigned int f = (k-1+param->angleSubdiv)%param->angleSubdiv + j     * param->angleSubdiv + (i-1) * (param->radiusSubdiv+1) * param->angleSubdiv;
                        unsigned int g = (k-1+param->angleSubdiv)%param->angleSubdiv + (j-1) * param->angleSubdiv + (i-1) * (param->radiusSubdiv+1) * param->angleSubdiv;
                        unsigned int h = (k)%param->angleSubdiv                      + (j-1) * param->angleSubdiv + (i-1) * (param->radiusSubdiv+1) * param->angleSubdiv;

                        Tetrahedron3D tetra;
                        tetra.setPoint(0, h);
                        tetra.setPoint(1, f);
                        tetra.setPoint(2, e);
                        tetra.setPoint(3, a);

                        outputMesh.addCell(tetra);

                        tetra.setPoint(0, h);
                        tetra.setPoint(1, f);
                        tetra.setPoint(2, a);
                        tetra.setPoint(3, b);

                        outputMesh.addCell(tetra);

                        tetra.setPoint(0, h);
                        tetra.setPoint(1, f);
                        tetra.setPoint(2, b);
                        tetra.setPoint(3, g);

                        outputMesh.addCell(tetra);

                        tetra.setPoint(0, d);
                        tetra.setPoint(1, h);
                        tetra.setPoint(2, a);
                        tetra.setPoint(3, b);

                        outputMesh.addCell(tetra);

                        tetra.setPoint(0, d);
                        tetra.setPoint(1, h);
                        tetra.setPoint(2, g);
                        tetra.setPoint(3, b);

                        outputMesh.addCell(tetra);

                        tetra.setPoint(0, d);
                        tetra.setPoint(1, g);
                        tetra.setPoint(2, b);
                        tetra.setPoint(3, c);

                        outputMesh.addCell(tetra);
                    }
                }
            }
        }

    }

}

void CylinderMeshGenerator::init()
{

}

void CylinderMeshGenerator::updateLabels(MeshGenerationParameters *parameters)
{
    if (!parameters) return;

    if (CylinderMeshGeneratorParameters* cylParams = dynamic_cast<CylinderMeshGeneratorParameters*>(parameters))
    {
        bool vertices = cylParams->automaticLabelingVertices;
        bool facets = cylParams->automaticLabelingFacets;
        bool cells = cylParams->automaticLabelingCells;

        if (vertices)
        {
            for (auto& i : insideIndices)
            {
                outputMesh.addPointLabel(i, 1.0);
            }
            for (auto& i : outsideIndices)
            {
                outputMesh.addPointLabel(i, 2.0);
            }
            for (auto& i : topIndices)
            {
                outputMesh.addPointLabel(i, 3.0);
            }
            for (auto& i : bottomIndices)
            {
                outputMesh.addPointLabel(i, 4.0);
            }
        }

        if (facets)
        {
            for (unsigned int f = 0 ; f < outputMesh.getNbFacets(); f++)
            {
                bool inside = true;
                bool outside = true;
                bool top = true;
                bool bottom = true;

                for (auto& i : outputMesh.getFacet(f).indices)
                {
                    bool inVecInside = false;
                    for (auto& in : this->insideIndices)
                    {
                        if (i == in)
                        {
                            inVecInside = true;
                            break;
                        }
                    }
                    if (!inVecInside) inside = false;

                    bool inVecOutside = false;
                    for (auto& in : this->outsideIndices)
                    {
                        if (i == in)
                        {
                            inVecOutside = true;
                            break;
                        }
                    }
                    if (!inVecOutside) outside = false;

                    bool inVecTop = false;
                    for (auto& in : this->topIndices)
                    {
                        if (i == in)
                        {
                            inVecTop = true;
                            break;
                        }
                    }
                    if (!inVecTop) top = false;

                    bool inVecBottom = false;
                    for (auto& in : this->bottomIndices)
                    {
                        if (i == in)
                        {
                            inVecBottom = true;
                            break;
                        }
                    }
                    if (!inVecBottom) bottom = false;


                }
                if (inside)
                    outputMesh.addFacetLabel(f, 1.0);
                if (outside)
                    outputMesh.addFacetLabel(f, 2.0);
                if (top)
                    outputMesh.addFacetLabel(f, 3.0);
                if (bottom)
                    outputMesh.addFacetLabel(f, 4.0);
            }

        }

        if (cells)
        {
            for (unsigned int f = 0 ; f < outputMesh.getNbCells(); f++)
            {
                bool inside = true;
                bool outside = true;
                bool top = true;
                bool bottom = true;

                for (auto& i : outputMesh.getCell(f).indices)
                {
                    bool inVecInside = false;
                    for (auto& in : this->insideIndices)
                    {
                        if (i == in)
                        {
                            inVecInside = true;
                            break;
                        }
                    }
                    if (!inVecInside) inside = false;

                    bool inVecOutside = false;
                    for (auto& in : this->outsideIndices)
                    {
                        if (i == in)
                        {
                            inVecOutside = true;
                            break;
                        }
                    }
                    if (!inVecOutside) outside = false;

                    bool inVecTop = false;
                    for (auto& in : this->topIndices)
                    {
                        if (i == in)
                        {
                            inVecTop = true;
                            break;
                        }
                    }
                    if (!inVecTop) top = false;

                    bool inVecBottom = false;
                    for (auto& in : this->bottomIndices)
                    {
                        if (i == in)
                        {
                            inVecBottom = true;
                            break;
                        }
                    }
                    if (!inVecBottom) bottom = false;


                }
                if (inside)
                    outputMesh.addCellLabel(f, 1.0);
                if (outside)
                    outputMesh.addCellLabel(f, 2.0);
                if (top)
                    outputMesh.addCellLabel(f, 3.0);
                if (bottom)
                    outputMesh.addCellLabel(f, 4.0);
            }
        }
    }
}

PolygonMesh<Tetrahedron3D> CylinderMeshGenerator::getOutputMesh() const
{
    return outputMesh;
}


