##################################################################################
# CMakeLists.txt for the project FEgen
##################################################################################



# Specifies the minimum version of CMake that can be used with CMakeLists.txt file.
cmake_minimum_required(VERSION 3.3 FATAL_ERROR)

# gets rid of the warning if using Cygwin. Tells CMake not to define WIN32 when building with Cygwin.
set(CMAKE_LEGACY_CYGWIN_WIN32 0)

#Name of the project
project (FEgen)

# enable the add_test() command
enable_testing()

# C++11
include(src/enableC++11.cmake)

# Tell CMake to run moc when necessary:
set(CMAKE_AUTOMOC ON)
# As moc files are generated in the binary dir, tell CMake
# to always look for includes there:
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(AUTORCC ON)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/src/CMake/")

# Widgets finds its own dependencies.
find_package(Qt5Core REQUIRED)
find_package(Qt5Widgets REQUIRED)
find_package(Qt5Gui REQUIRED)
find_package(Qt5Xml REQUIRED)

if (NOT Qt5Xml_FOUND )
  message(FATAL_ERROR "Component QtXml required, but not found!")
endif()

# Copy FEgenConfig.h.in to src/FEgenConfig.h and modify the cmake variables
configure_file(
    "${PROJECT_SOURCE_DIR}/src/FEgenConfig.h.in"
    "${PROJECT_BINARY_DIR}/src/FEgenConfig.h"
    )
include_directories("${PROJECT_BINARY_DIR}/src")

include(${PROJECT_SOURCE_DIR}/src/libraries.cmake)

##################################################################################
# Source files
##################################################################################
set(SOURCE_FILES
    src/main.cpp
    src/mainwindow.cpp
    src/meshgenerator.cpp
    src/copymeshgenerator.cpp
    src/filereader.cpp
    src/meshfilereader.cpp
    src/fileexport.cpp
    src/objreader.cpp
    src/vtkreader.cpp
    src/imagefilereader.cpp
    src/niftireader.cpp
    src/vtuexport.cpp
    src/objexporter.cpp
    src/dolfinexporter.cpp
    src/basemesh.cpp
    src/baseimage.cpp
    src/boundingbox.cpp
    src/point3d.cpp
    src/polygon3d.cpp
    src/cgalmeshgenerator.cpp
    src/cylindergenerationwidget.cpp
    src/cylindermeshgenerator.cpp
    src/meshgenerationparameters.cpp
    src/meshgeneratorfrompolyhedra.cpp
    src/meshgeneratorfromimage.cpp
#    src/meshviewer.cpp
    src/imageslice.cpp
    src/vector.cpp
    src/matrix.cpp
    src/transform.cpp
    src/quaternion.cpp
    src/labeldomain.cpp
    src/bboxwidget.cpp
    src/cgalparameterswidget.cpp
    src/outputinfowidget.cpp
)

##################################################################################
# Header files
##################################################################################
set(HEADER_FILES
    src/mainwindow.h
    src/meshgenerator.h
    src/copymeshgenerator.h
    src/filereader.h
    src/meshfilereader.h
    src/fileexport.h
    src/objreader.h
    src/vtkreader.h
    src/imagefilereader.h
    src/niftireader.h
    src/vtuexport.h
    src/objexporter.h
    src/dolfinexporter.h
    src/basemesh.h
    src/baseimage.h
    src/boundingbox.h
    src/point3d.h
    src/polygon3d.h
    src/meshgenerationparameters.h
    src/cgalmeshgenerator.h
    src/cylindergenerationwidget.h
    src/cylindermeshgenerator.h
    src/meshgeneratorfrompolyhedra.h
    src/meshgeneratorfromimage.h
#    src/meshviewer.h
    src/imageslice.h
    src/vector.h
    src/matrix.h
    src/transform.h
    src/quaternion.h
    src/labeldomain.h
    src/bboxwidget.h
    src/cgalparameterswidget.h
    src/outputinfowidget.h
)

set(UIS
    src/mainwindow.ui
)

set(QRCS
    src/qml.qrc
)

qt5_wrap_ui(UIS_HEADERS ${UIS})
qt5_add_resources(QRC_RCC ${QRCS})

add_executable(FEgen ${HEADER_FILES} ${SOURCE_FILES} ${UIS_HEADERS} ${QRC_RCC})

target_link_libraries(FEgen boost_thread boost_system z)
target_link_libraries(FEgen ${CGAL_LIBRARIES} ${CGAL_3RD_PARTY_LIBRARIES} gmp mpfr)
target_link_libraries(FEgen ${VTK_LIBRARIES})
target_link_libraries(FEgen ${NIFTI_LIBRARIES} z)
#target_link_libraries(FEgen boost_thread boost_system libcgal_core libcgal z libcgal_imageio gmp mpfr GL GLU libqglviewer)

qt5_use_modules(FEgen Core Core)
qt5_use_modules(FEgen Core Qml)
qt5_use_modules(FEgen Widgets Xml OpenGL)

# Installation
install(TARGETS FEgen DESTINATION bin)

