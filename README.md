Created by [Alexandre Bilger](http:://alexbilger.com>)

# Overview
FEgen generates 3D tetrahedral meshes from a surface mesh or a segmented medical image.

## Features

* Graphical interface
* Read medical images file
        - NIFTI (*.nii, *.nii.gz)
        - INR (*.inr, *.inr.gz)
* 3 views image viewer
* Read surface mesh files
        - Wavefront (*.obj)
* Mesh generation methods:
        - CGAL
* Label definition:
        - Bounding box
* Export tetrahedral meshes:
	- VTK (*.vtu)
	- Dolfin (*.xml)


## Dependencies
* **CMake** >=3.3
* **Qt**
* **CGAL**
    - Download and install CGAL library:
    ```
    git clone  https://github.com/CGAL/cgal.git
    mkdir cgal-build
    cd cgal-build
    cmake ../cgal
    make
    sudo make install
    ```
        - CGAL needs dependancies:
                * GMP
                * MPFR
                * zlib
* **VTK** >=6.2
    ```
     wget "http://www.vtk.org/files/release/6.2/VTK-6.2.0.tar.gz"
     tar xvfz VTK-6.2.0.tar
     cd VTK-6.2.0
     mkdir build 
     cd build 
     cmake ../  \
     -DCMAKE_INSTALL_PREFIX=/usr/local
     -DBUILD_EXAMPLES:BOOL=OFF
     -DBUILD_SHARED_LIBS:BOOL=ON
     -DBUILD_TESTING:BOOL=OFF
     -DCMAKE_BUILD_TYPE:STRING="Release"
     -DVTK_BUILD_ALL_MODULES:BOOL=OFF
     -DVTK_USE_SYSTEM_HDF5:BOOL=ON
     -DCMAKE_C_FLAGS=-DGLX_GLXEXT_LEGACY
     -DCMAKE_CXX_FLAGS=-DGLX_GLXEXT_LEGACY
     make -j
     make install
    ` ``
    ```
* **NIFI**
    - Download and install the NIfTI C library
    ```
    wget "http://sourceforge.net/projects/niftilib/files/latest/download" -O nifticlib.tar.gz
    tar -zxvf nifticlib.tar.gz
    cd nifticlib-2.0.0
    cmake .
    make
    sudo make install
    ```


## Installation

Coming soon...

=======


## Screenshots

* FEgen interface:
![screenshot](https://bitbucket.org/unilucompmech/fegen/raw/master/Resources/Screenshots/screenshot_FEgen.png)

* Result in paraview:
![screenshot](https://bitbucket.org/unilucompmech/fegen/raw/master/Resources/Screenshots/screenshot_paraview.png)

* Cylinder visualized in paraview:
![screenshot](https://bitbucket.org/unilucompmech/fegen/raw/master/Resources/Screenshots/screenshot_cylinder_paraview.png)